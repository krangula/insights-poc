export const prefix = 'automation-dashboard/login/'
export const LOGIN_INVALIDATE = `${prefix}LOGIN_INVALIDATE`
export const LOGIN_RESET = `${prefix}LOGIN_RESET`
export const SESSION_LOST = `${prefix}SESSION_LOST`
export const LOGIN_SET_KEYCHAIN = `${prefix}LOGIN_SET_KEYCHAIN`
export const LOGIN_REMOVE_KEYCHAIN = `${prefix}LOGIN_REMOVE_KEYCHAIN`
export const SET_BIOMETRICS_SUPPORT = `${prefix}SET_BIOMETRICS_SUPPORT`
export const ACTIVATE_REMEMBER_ME = `${prefix}ACTIVATE_REMEMBER_ME`
export const ACTIVATE_BIOMETRICS = `${prefix}ACTIVATE_BIOMETRICS`

export const FETCH_LOGIN_REQUEST = `${prefix}FETCH_LOGIN_REQUEST`
export const FETCH_LOGIN_RECEIVED = `${prefix}FETCH_LOGIN_RECEIVED`
export const FETCH_LOGIN_FAIL = `${prefix}FETCH_LOGIN_FAIL`

export const UPDATE_AUTH_DATA = `${prefix}UPDATE_AUTH_DATA`

export const SET_DEMO_MODE = `${prefix}SET_DEMO_MODE`

const INITIAL_STATE = {
  loginProgress: false,
  loginFail: false,
  loginValid: false,
  hasKeychain: false,
  keyChainList: [],
  authData: {},
  sessionArgs: {},
  biometricsSupported: false,
  biometrics: false,
  remember: false
}

const login = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_BIOMETRICS_SUPPORT:
      return {
        ...state,
        biometricsSupported: true
      }
    case ACTIVATE_REMEMBER_ME:
      return {
        ...state,
        remember: action.payload
      }

    case ACTIVATE_BIOMETRICS:
      return {
        ...state,
        biometrics: action.payload
      }

    case FETCH_LOGIN_REQUEST:
      return {
        ...state,
        loginProgress: true
      }

    case FETCH_LOGIN_FAIL:
      return {
        ...state,
        loginProgress: INITIAL_STATE.loginProgress,
        loginFail: true
      }

    case FETCH_LOGIN_RECEIVED:
      return {
        ...state,
        loginValid: true,
        loginProgress: INITIAL_STATE.loginProgress,
        authData: action.payload.authData,
        username: action.payload.user
      }

    case UPDATE_AUTH_DATA:
      return {
        ...state,
        loginValid: true,
        authData: action.payload.authData
      }

    case SESSION_LOST:
      return {
        ...state,
        loginValid: INITIAL_STATE.loginValid,
        sessionArgs: action.payload
      }

    case LOGIN_SET_KEYCHAIN: {
      const updated = state.keyChainList
      updated.push(action.payload)
      return {
        ...state,
        hasKeychain: updated.length > 0,
        keyChainList: updated
      }
    }

    case LOGIN_REMOVE_KEYCHAIN: {
      const index = state.keyChainList.findIndex(
        item => item === action.payload
      )
      const reduced = state.keyChainList
      if (index !== -1) reduced.splice(index, 1)
      return {
        ...state,
        hasKeychain: reduced.length > 0,
        keyChainList: reduced
      }
    }

    case SET_DEMO_MODE:
      return {
        ...INITIAL_STATE,
        loginValid: action.payload
      }

    case LOGIN_INVALIDATE:
    case LOGIN_RESET:
      return INITIAL_STATE

    default:
      return state
  }
}

export const doLogin = () => ({
  type: FETCH_LOGIN_REQUEST
})

export const doLogout = () => ({
  type: LOGIN_INVALIDATE
})

export const doLoginSuccess = (authData, user) => ({
  type: FETCH_LOGIN_RECEIVED,
  payload: {
    authData,
    user
  }
})

export const updateAuthData = authData => ({
  type: UPDATE_AUTH_DATA,
  payload: {
    authData
  }
})

export const doLoginFail = () => ({
  type: FETCH_LOGIN_FAIL
})

export const doResetLogin = () => ({
  type: LOGIN_RESET
})

export const sessionLost = (args = {}) => ({
  type: SESSION_LOST,
  payload: args
})

export const setControlRoomKeychain = args => ({
  type: LOGIN_SET_KEYCHAIN,
  payload: args
})

export const removeControlRoomKeychain = key => ({
  type: LOGIN_REMOVE_KEYCHAIN,
  payload: key
})

export const hasBiometricSupport = () => ({
  type: SET_BIOMETRICS_SUPPORT
})

export const activateBiometric = (activate = false) => ({
  type: ACTIVATE_BIOMETRICS,
  payload: activate
})

export const activateRememberBe = (activate = false) => ({
  type: ACTIVATE_REMEMBER_ME,
  payload: activate
})

export const setDemoLogin = status => ({
  type: SET_DEMO_MODE,
  payload: status
})

export default login
