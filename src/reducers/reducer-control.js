export const prefix = 'automation-control-room/dashboard/'
export const SET_SELECTED_CONTROL_ROOM = `${prefix}SET_SELECTED_CONTROL_ROOM`
export const CLEAR_SELECTED_CONTROL_ROOM = `${prefix}CLEAR_SELECTED_CONTROL_ROOM`
export const SET_CONTROL_ROOM_LIST = `${prefix}MERGE_TO_CONTROL_ROOM_LIST`
export const REMOVE_CONTROL_ROOM = `${prefix}REMOVE_CONTROL_ROOM`
export const RESET_ALL_CONTROL_ROOM = `${prefix}RESET_ALL_CONTROL_ROOM`
export const SET_DEFAULT_CONTROL_ROOM = `${prefix}SET_DEFAULT_CONTROL_ROOM`
export const REMOVE_DEFAULT_CONTROL_ROOM = `${prefix}REMOVE_DEFAULT_CONTROL_ROOM`
export const SET_LOADER_COLOR = `${prefix}SET_LOADER_COLOR`

const INITIAL_STATE = {
  loaderColor: '',
  defaultRoom: {},
  controlRoomList: [],
  selectedControlRoom: {
    value: 'http://ec2-52-36-164-130.us-west-2.compute.amazonaws.com'
  }
}

const control = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_SELECTED_CONTROL_ROOM:
      return {
        ...state,
        selectedControlRoom: state.controlRoomList.find(
          list => list.value === action.payload.value
        )
      }

    case SET_CONTROL_ROOM_LIST:
      return {
        ...state,
        controlRoomList: action.payload
      }

    case SET_DEFAULT_CONTROL_ROOM:
      return {
        ...state,
        defaultRoom: action.payload
      }

    case REMOVE_DEFAULT_CONTROL_ROOM:
      return {
        ...state,
        defaultRoom: INITIAL_STATE.defaultRoom
      }

    case CLEAR_SELECTED_CONTROL_ROOM:
      return {
        ...state,
        selectedControlRoom: INITIAL_STATE.selectedControlRoom
      }

    case REMOVE_CONTROL_ROOM: {
      const rooms = state.controlRoomList
      const idx = rooms.findIndex(room => room.name === action.payload)
      rooms.splice(idx, 1)
      return {
        ...state,
        controlRoomList: rooms,
        selectedControlRoom:
          rooms.length === 0 ||
          state.selectedControlRoom.name === action.payload
            ? INITIAL_STATE.selectedControlRoom
            : state.selectedControlRoom
      }
    }

    case SET_LOADER_COLOR:
      return {
        ...state,
        loaderColor: action.payload
      }

    case RESET_ALL_CONTROL_ROOM:
      return INITIAL_STATE

    default:
      return state
  }
}

export const setControlRoom = val => ({
  type: SET_SELECTED_CONTROL_ROOM,
  payload: val
})

export const clearSelectedControlRoom = () => ({
  type: CLEAR_SELECTED_CONTROL_ROOM
})

export const removeControlRoom = val => ({
  type: REMOVE_CONTROL_ROOM,
  payload: val
})

export const setControlRoomList = (rooms = []) => ({
  type: SET_CONTROL_ROOM_LIST,
  payload: rooms
})

export const resetControlRooms = () => ({
  type: RESET_ALL_CONTROL_ROOM
})

export const setDefaultRoom = (room = {}) => ({
  type: SET_DEFAULT_CONTROL_ROOM,
  payload: room
})

export const removeDefaultRoom = () => ({
  type: REMOVE_DEFAULT_CONTROL_ROOM
})

export const setLoaderColor = color => ({
  type: SET_LOADER_COLOR,
  payload: color
})

export default control
