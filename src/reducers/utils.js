import moment from 'moment'

const colorSet = require('../mock/colors-bar.json')
const pieColorSet = require('../mock/colors-pie.json')

const chartColors = colorSet.colors
const pieColors = pieColorSet.colors

const sumReducer = (accumulator = 0, currentValue) => ({
  value: accumulator.value + currentValue.value
})

const maxReducer = (accumulator = 0, currentValue) => ({
  value: Math.max(accumulator.value, currentValue.value)
})

const minReducer = (accumulator = 0, currentValue) => ({
  value: Math.min(accumulator.value, currentValue.value)
})

/**
 * Parse data response containing bot data and zoom data visulizations
 * @param {Object} args - bot + visulizations data
 * @param {Array} args.data - bot data
 * @param {string} args.dashboard - zoom data visualization information
 * @return {Object} - Parsed and reconstructed data to render charts and kpi data
 */
export const visualizeThis = ({ data, dashboard }) => {
  const chartSet = []
  const filtered = []

  const { bookmarks, sources: _sources } = dashboard

  const viz = bookmarks[0].visualizations
  const sources = _sources.sources[0].objectFields

  const allowed = [
    'DONUT',
    'PIE',
    'LINE_AND_BARS',
    'UBER_BARS',
    'BUBBLES',
    'KPI',
    'HEAT_MAP'
  ]

  const result = viz.filter(visual => allowed.includes(visual.type))
  const chartList = []

  // LOOP 1
  for (let c = 0, len = result.length - 1; c <= len; c++) {
    const { type, source } = result[c]
    const { variables } = source
    let _chart = {
      chart: type,
      chartTitle: variables['Chart Name']
    }

    chartList.push(type)

    switch (type) {
      case 'DONUT':
      case 'PIE':
        _chart = {
          ..._chart,
          name: variables['Group By'].name,
          size: variables.Size[0].name
        }
        break
      case 'LINE_AND_BARS':
      case 'UBER_BARS':
        _chart = {
          ..._chart,
          name:
            type === 'LINE_AND_BARS'
              ? variables['Y1 Axis'][0].name
              : variables['Bar Color'][0].name,
          colors:
            type === 'UBER_BARS' &&
            variables['Bar Color'][0].colorConfig.colors,
          groups: type === 'UBER_BARS' && variables['Multi Group By']
        }

        if (variables['Trend Attribute']) {
          const trend = variables['Trend Attribute']
          _chart = {
            ..._chart,
            trend
          }
        }
        break
      case 'BUBBLES':
        _chart = {
          ..._chart,
          name: variables['Bubble Size'][0].name,
          groups: variables['Group By'] && [variables['Group By']]
        }
        break
      case 'HEAT_MAP':
        _chart = {
          ..._chart,
          name: variables['Color Metric'][0].name,
          groups: variables['Multi Group By'],
          heatX: [],
          heatY: [],
          heat: [],
          dataSet: [],
          heatTotal: 0
        }

        break
      case 'KPI':
        {
          const name = (variables.Metric && variables.Metric[0].name) || ''
          _chart = {
            ..._chart,
            name,
            label:
              variables['Metrics Labels'] &&
              variables['Metrics Labels'].primary[name],
            func: variables.Metric && variables.Metric[0].func
          }
        }
        break
      default:
        break
    }
    if (
      chartSet.findIndex(
        (chart = {}) =>
          chart.name &&
          (chart.name === _chart.name && chart.chart === _chart.chart)
      ) === -1
    ) {
      chartSet.push(_chart)
      filtered.push(_chart.name)
    }
  }

  // console.log(chartSet)
  // TEST LOOP TO CHECK MATH
  // let testamount = 0
  // data.map((items, _idx) => {
  //   const source = items.findIndex(
  //     item => item.name === 'process' && item.value === 'Leaves'
  //   )
  //   console.log(source)
  //   if (source !== -1) {
  //     console.log(data[_idx])
  //     const money = data[_idx].find(item => item.name === 'money_saved')
  //     if (money) testamount += parseInt(money.value, 10)
  //   }
  // })
  // console.log('THE LEAVES', testamount)

  let barCount = 0
  let pieCount = 0

  // LOOP 2
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ]

  // console.log('The total data', chartSet, data.length)
  // const start = new Date().getTime()
  for (let j = 0, len = data.length - 1; j <= len; j++) {
    const index = j
    const items = data[j]
    // items.map(item => {
    let botName
    let botData
    let botProp
    let point
    let _trend

    for (let z = 0, chartLen = chartSet.length - 1; z <= chartLen; z++) {
      const idx = z
      const { name, chart, size, groups, func, trend = {} } = chartSet[z]
      const { sort = {} } = trend
      const { name: sortName, type = '' } = sort
      let monthIdx = -1

      if (pieCount >= pieColors.length) pieCount = 0
      if (barCount >= chartColors.length) barCount = 0

      const item = items.find(prop => prop.name === name)
      if (item) {
        const { name: _name, value } = item
        if (name === _name) {
          const meta = sources.find(source => source.name === name)
          switch (chart) {
            case 'DONUT':
            case 'PIE':
              // _pieValue = botData ? botData.value : 1
              botName = items.find(_item => _item.name === _name)
              botData = items.find(_item => _item.name === size)

              point = {
                name: botName.value,
                value: parseInt(botData.value, 10),
                color: pieColors[pieCount]
              }

              pieCount += 1

              if (name === _name) {
                if (chartSet[idx].plot) {
                  const isPresent = chartSet[idx].plot.findIndex(
                    set => set.name === point.name
                  )
                  if (isPresent !== -1) {
                    chartSet[idx].plot[isPresent].value += parseInt(
                      botData.value,
                      10
                    )
                  } else {
                    chartSet[idx].plot.push(point)
                  }
                } else {
                  chartSet[idx].plot = [point]
                }
              }
              break
            case 'LINE_AND_BARS':
            case 'UBER_BARS':
            case 'BUBBLES':
              if (!chartSet[idx].label) chartSet[idx].label = meta.label
              if (groups && groups.length > 0) {
                botName = items.find(_item => _item.name === groups[0].name)
              } else {
                botName = items.find(_item => _item.name === 'botname') || {}
              }
              botData = items.find(_item => _item.name === name)

              if (name === _name) {
                if (sort && sortName) {
                  _trend = items.find(_item => _item.name === sortName)

                  let _month
                  if (type === 'TIME') {
                    // _month = moment(_trend.value).format('MMMM-YYYY')
                    // const _format = _trend.value
                    // const date = new Date(_format.toString())
                    const _format = _trend.value
                    const date = new Date(_format)

                    _month = `${monthNames[
                      date.getMonth()
                    ].toLowerCase()}-${date.getFullYear()}`

                    // _trend = new Date(
                    //   date.getFullYear(),
                    //   date.getMonth(),
                    //   date.getDate()
                    // )

                    _trend = moment(_trend.value).unix()

                    // Build months
                    if (chartSet[idx].months) {
                      monthIdx = chartSet[idx].months.findIndex(
                        month => month.x === _month
                      )
                      if (monthIdx !== -1) {
                        chartSet[idx].months[monthIdx].value += parseInt(
                          botData.value,
                          10
                        )
                      } else {
                        chartSet[idx].months.push({
                          x: _month,
                          // x: new Date(date.getFullYear(), date.getMonth(), 1),
                          value: parseInt(botData.value, 10),
                          color: chartColors[barCount],
                          plot: []
                        })
                      }
                    } else {
                      chartSet[idx].months = [
                        {
                          x: _month,
                          // x: new Date(date.getFullYear(), date.getMonth(), 1),
                          value: parseInt(botData.value, 10),
                          color: chartColors[barCount],
                          plot: []
                        }
                      ]
                    }
                  }

                  point = {
                    x: type === 'TIME' ? _trend : botName.value,
                    value: parseInt(botData.value, 10),
                    color: chartColors[barCount],
                    process: [
                      {
                        x: botName.value,
                        value: parseInt(botData.value, 10)
                      }
                    ]
                  }

                  if (type === 'TIME' && !chartSet[idx].scale)
                    chartSet[idx].scale = type
                } else {
                  point = {
                    x: botName.value,
                    value: parseInt(botData.value, 10),
                    color: chartColors[barCount],
                    process: [
                      {
                        x: botName.value,
                        value: parseInt(botData.value, 10)
                      }
                    ]
                  }
                }

                if (chartSet[idx].plot) {
                  const isPresent = chartSet[idx].plot.findIndex(set => {
                    if (sort && type === 'TIME') {
                      return set.x.toString() === point.x.toString()
                    }
                    return set.x === point.x
                  })

                  if (isPresent !== -1) {
                    chartSet[idx].plot[isPresent].value += point.value

                    if (chartSet[idx].plot[isPresent].process) {
                      const currentProcesses =
                        chartSet[idx].plot[isPresent].process
                      const processIdx = currentProcesses.findIndex(
                        process => process.x === point.process[0].x
                      )

                      let sortProcess = []

                      if (processIdx !== -1) {
                        currentProcesses[processIdx].value +=
                          point.process[0].value
                        sortProcess = sortProcess
                          .concat(currentProcesses)
                          .sort((a, b) => a.value < b.value)
                        chartSet[idx].plot[isPresent].process = sortProcess
                      } else {
                        const processes = chartSet[idx].plot[
                          isPresent
                        ].process.concat(point.process)
                        sortProcess = sortProcess
                          .concat(processes)
                          .sort((a, b) => a.value < b.value)
                        chartSet[idx].plot[isPresent].process = sortProcess
                      }
                    }
                  } else {
                    barCount += 1
                    chartSet[idx].plot.push(point)
                    if (chartSet[idx].months) {
                      const pointMonth = moment
                        .unix(point.x)
                        .format('MMMM-YYYY')
                      const monthIndex = chartSet[idx].months.findIndex(
                        month => month.x === pointMonth.toLowerCase()
                      )
                      chartSet[idx].months[monthIndex].plot.push(point)
                    }
                  }
                } else {
                  barCount += 1
                  chartSet[idx].plot = [point]
                  if (chartSet[idx].months) {
                    const pointMonth = moment.unix(point.x).format('MMMM-YYYY')
                    const monthIndex = chartSet[idx].months.findIndex(
                      month => month.x === pointMonth.toLowerCase()
                    )
                    chartSet[idx].months[monthIndex].plot = [point]
                  }
                }

                if (index === data.length - 1) {
                  if (trend && trend.type === 'TIME') {
                    // chartSet[idx].plot.sort((a, b) => a.x - b.x)
                    const reversed = chartSet[idx].months.reverse()
                    // chartSet[idx].plot = reversed
                    chartSet[idx].months = reversed
                  } else {
                    chartSet[idx].plot.sort((a, b) => a.value < b.value)
                  }
                }
              }
              break

            case 'KPI':
              if (name === _name) {
                botName = items.find(_item => _item.name === 'botname') || {}
                botData = items.find(_item => _item.name === _name)
                if (!chartSet[idx].label) chartSet[idx].label = meta.label
                const kpi = parseInt(value, 10)
                if (chartSet[idx].value) {
                  chartSet[idx].value.push({
                    value: kpi,
                    type: botData.value,
                    process: botName.value
                  })
                  chartSet[idx].count += 1
                } else {
                  chartSet[idx].value = [
                    {
                      value: kpi,
                      type: botData.value,
                      process: botName.value
                    }
                  ]
                  chartSet[idx].count = 1
                  chartSet[idx].func = func
                }
              }
              break
            case 'HEAT_MAP':
              {
                // if (name === _name && groups.length > 0) {
                const _groupX = groups[0].name
                const _groupY = groups[1].name
                const botY = items.find(_item => _item.name === _groupY)
                botName = items.find(_item => _item.name === 'botname') || {}
                botProp = items.find(_item => _item.name === _name)
                if (_groupX) {
                  botData = items.find(_item => _item.name === _groupX)

                  const _idx = chartSet[idx].heatX.findIndex(
                    group => group === botData.value
                  )

                  if (_idx === -1) {
                    chartSet[idx].heat.push(0)
                    chartSet[idx].heatX.push(botData.value)
                    if (!chartSet[idx].heatY.includes(botY.value)) {
                      chartSet[idx].heatY.push(botY.value)
                    }
                  }

                  const dataSetIdx = chartSet[idx].dataSet.findIndex(
                    set => set.label === botY.value
                  )

                  // const labelIdx = chartSet[idx].heatX.findIndex(
                  //   label => label === botData.value
                  // )

                  if (dataSetIdx === -1) {
                    chartSet[idx].heatTotal += parseInt(botProp.value, 10)
                    chartSet[idx].dataSet.push({
                      label: botY.value,
                      data: {
                        [botData.value]: parseInt(botProp.value, 10)
                      },
                      group: botData.value,
                      bots: [botName.value]
                    })
                  } else {
                    const currentHeat = chartSet[idx].dataSet[dataSetIdx]
                    if (!currentHeat.bots.find(bot => bot === botName.value)) {
                      currentHeat.bots.push(botName.value)
                    }
                    if (currentHeat.data[botData.value]) {
                      currentHeat.data[botData.value] += parseInt(
                        botProp.value,
                        10
                      )
                    } else {
                      currentHeat.data[botData.value] = parseInt(
                        botProp.value,
                        10
                      )
                    }
                    chartSet[idx].dataSet[dataSetIdx] = currentHeat
                  }
                }
              }
              break
            default:
              break
          }
        }
      }
    }
    // }
  }

  // const end = new Date().getTime()
  // console.log('TOTAL TIME RUN', end - start, data.length)

  // reconstruct to summarize the KPIs
  const visuals = []
  const kpis = {
    chart: 'KPI',
    plot: []
  }

  // LOOP 3
  chartSet.forEach(item => {
    const { chart, func, count, plot, scale = '' } = item
    let sort = []

    if (chart === 'KPI') {
      let res
      if (func === 'last_value') {
        res = item.value[item.value.length - 1].value
      } else {
        switch (func) {
          case 'sum':
            res = item.value.reduce(sumReducer).value
            break
          case 'min':
            res = item.value.reduce(minReducer).value
            break
          case 'max':
            res = item.value.reduce(maxReducer).value
            break
          default:
            res = (item.value.reduce(sumReducer).value / count).toFixed(2)
            break
        }
      }
      kpis.plot.push({
        ...item,
        value: res
      })
    } else if (chart === 'PIE' || chart === 'DONUT') {
      sort = sort.concat(plot).sort((a, b) => a.y < b.y)
      item.plot = sort
      visuals.push(item)
    } else {
      if (
        (chart === 'LINE_AND_BARS' || chart === 'UBER_BARS') &&
        scale === 'TIME'
      ) {
        if (item.plot.length === 1) {
          const date = new Date(moment(item.plot[0].x).add(5, 'days'))
          const _trend = new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDay()
          )

          const point = {
            x: _trend,
            value: 0,
            color: '#d2d2d2',
            process: []
          }

          item.plot.push(point)
        }
      }

      visuals.push(item)
    }
  })

  if (kpis.plot.length > 0) visuals.unshift(kpis)

  return {
    visuals,
    chartList,
    colorScale: chartColors
  }
}

export const awaitResult = async ({ data, dashboard }) => {
  const result = await visualizeThis({ data, dashboard })
  return result
}

export const removeDashboardWidget = (dashboard, widgetId) => {
  const { visuals, queries } = dashboard
  const queryIdx = queries.findIndex(({ id }) => id === widgetId)
  const visualIdx = visuals.findIndex(({ id }) => id === widgetId)

  queries.splice(queryIdx, 1)
  visuals.splice(visualIdx, 1)

  return {
    visuals,
    queries
  }
}

export const prepareKibana = (
  {
    type,
    query = {},
    meta = {},
    dashboard = {},
    title,
    formValues,
    id,
    tempId
  },
  currentVisuals = [],
  currentQueries = [],
  selectedDashboard = {}
) => {
  const { aggs } = query

  if (!aggs) {
    return {
      visuals: [],
      queries: []
    }
  }

  const { visualization } = aggs
  const { type: dashboardType } = selectedDashboard

  let queryItems = {}
  let queryTerms = {}
  let autoFormValues = {}

  if (visualization) {
    queryItems = visualization.aggs || visualization
    queryTerms = visualization.terms
  } else {
    queryItems = aggs
  }

  const keys = Object.keys(queryItems)
  const { aggregations } = dashboard
  let _props = {}

  const values = []
  const visuals = currentVisuals
  const queries = currentQueries
  let plot = []

  // access meta props
  const { title: metaTitle, formValues: metaFormValues } = meta

  for (let i = 0; i < keys.length; i++) {
    const func = Object.keys(queryItems[keys[i]])[0]
    _props = {
      ..._props,
      [keys[i]]: func
    }
    values.push(func)

    if (dashboardType === 'AUTO') {
      let metricField = []
      let _metric

      if (type === 'KPI') {
        metricField = queryItems[keys[i]][func].split('.')
        _metric = metricField[metricField.length - 1]
        autoFormValues = { metric: _metric, func: keys[0] }
      } else {
        metricField = queryItems[keys[i]][func].field.split('.')
        const termField = queryTerms.field.split('.')
        const filteredTerm = termField.filter(term => term !== 'keyword')

        _metric = metricField[metricField.length - 1]
        const _group = filteredTerm[filteredTerm.length - 1]
        autoFormValues = { metric: _metric, group: _group, func }
      }
    }
  }

  switch (type) {
    case 'PIE':
    case 'BAR':
    case 'LINE':
    case 'SCATTER':
    case 'BUBBLE':
      {
        const { visualization: item } = aggregations
        const { buckets } = item
        for (let j = 0; j < buckets.length; j++) {
          const bucket = buckets[j]
          for (let k = 0; k < keys.length; k++) {
            const key = keys[k]
            if (bucket[key]) {
              buckets[j] = {
                ...bucket,
                [_props[key]]: bucket[key].value
              }
            }
          }

          plot = buckets
        }
      }
      break
    case 'KPI':
    case 'GAUGE':
      {
        const { visualization: item } = aggregations
        for (let k = 0; k < keys.length; k++) {
          plot.push({
            ...item,
            func: keys[k],
            chartTitle: title || metaTitle
          })
        }
      }
      break

    default:
      break
  }

  let queryProp = {
    id,
    meta: {
      type,
      title: title || metaTitle,
      formValues: formValues || metaFormValues
    },
    query
  }

  let visualProp = {
    id,
    chartTitle: title || metaTitle,
    keys: values,
    type,
    plot,
    formValues: formValues || metaFormValues || autoFormValues
  }

  if (tempId) {
    queryProp = { ...queryProp, tempId }
    visualProp = { ...visualProp, tempId }
  }

  const queryIdx = queries.findIndex(
    ({ id: widgetId, tempId: currentTempId }) =>
      (widgetId && id === widgetId) ||
      (currentTempId && currentTempId === tempId)
  )

  const visualIdx = visuals.findIndex(
    ({ id: widgetId, tempId: currentTempId }) =>
      (widgetId && id === widgetId) ||
      (currentTempId && currentTempId === tempId)
  )

  if (queryIdx !== -1) {
    queries[queryIdx] = queryProp
  } else {
    queries.push(queryProp)
  }

  if (visualIdx !== -1) {
    visuals[visualIdx] = visualProp
  } else {
    visuals.push(visualProp)
  }

  return {
    visuals,
    queries
  }
}
