import { awaitResult, prepareKibana, removeDashboardWidget } from './utils'

export const prefix = 'rpa-benchmarking/dashboard/'

export const REQUEST_ERROR_RESET = 'REQUEST_ERROR_RESET'

export const REQUEST_DASHBOARDS = `${prefix}REQUEST_DASHBOARDS`
export const RECEIVE_DASHBOARDS = `${prefix}RECEIVED_DASHBOARDS`

export const REQUEST_DASHBOARD = `${prefix}REQUEST_DASHBOARD`
export const RECEIVE_DASHBOARD = `${prefix}RECEIVED_DASHBOARD`
export const REQUEST_COMPARE_DASHBOARD = `${prefix}REQUEST_COMPARE_DASHBOARD`
export const RECEIVE_COMPARE_DASHBOARD = `${prefix}RECEIVE_COMPARE_DASHBOARD`

export const SET_PRIMARY_DASHBOARD = `${prefix}SET_PRIMARY_DASHBOARD`
export const SET_COMPARE_DASHBOARD = `${prefix}SET_COMPARE_DASHBOARD`
export const SET_DASHBOARD_LIST = `${prefix}SET_DASHBOARD_LIST`
export const SET_DASHBOARD_DATA = `${prefix}SET_DASHBOARD_DATA`

export const PREPARE_DASHBOARD_DATA = `${prefix}PREPARE_DASHBOARD_DATA`
export const SET_NO_DASHBOARD_DATA = `${prefix}SET_NO_DASHBOARD_DATA`
export const TOGGLE_DASHBOARD_FULLSCREEN = `${prefix}TOGGLE_DASHBOARD_FULLSCREEN`

export const SET_KIBANA_DASHBOARD = `${prefix}SET_KIBANA_DASHBOARD`
export const SET_COMPARE_TO_DASHBOARD = `${prefix}SET_COMPARE_TO_DASHBOARD`
export const SET_SMART_SEARCH_PRIMARY = `${prefix}SET_SMART_SEARCH_PRIMARY`

export const SET_DASHBOARD_TASKS = `${prefix}SET_DASHBOARD_TASKS`
export const SET_TASK_MAPPINGS = `${prefix}SET_TASK_MAPPINGS`

export const SET_CURRENT_DASHBOARD_INFO = `${prefix}SET_CURRENT_DASHBOARD_INFO`
export const SET_COMPARED_DASHBOARD_INFO = `${prefix}SET_COMPARED_DASHBOARD_INFO`

export const RESET_DASHBOARD_DATA = `${prefix}RESET_DASHBOARD_DATA`
export const REMOVE_DASHBOARD_WIDGET = `${prefix}REMOVE_DASHBOARD_WIDGET`

export const UPDATE_DASHBOARD_START = `${prefix}UPDATE_DASHBOARD_START`
export const UPDATE_DASHBOARD_FINISH = `${prefix}UPDATE_DASHBOARD_FINISH`

export const REMOVE_ITEM_DASHBOARD_LIST = `${prefix}REMOVE_ITEM_DASHBOARD_LIST`
export const ADD_ITEM_DASHBOARD_LIST = `${prefix}ADD_ITEM_DASHBOARD_LIST`

export const TOGGLE_COMPARE_TO_DASHBOARD = `${prefix}TOGGLE_COMPARE_TO_DASHBOARD`

export const SET_DATE_RANGE_COMPARE = `${prefix}SET_DATE_RANGE_COMPARE`
export const SET_DATE_RANGE = `${prefix}SET_DATE_RANGE`

export const SET_GENERATED_DASHBOARD = `${prefix}SET_GENERATED_DASHBOARD`

export const REORDER_DASHBOARD_WIDGETS = `${prefix}REORDER_DASHBOARD_WIDGETS`

export const TOGGLE_WIDGET_SWITCH = `${prefix}TOGGLE_WIDGET_SWITCH`

const INITIAL_STATE = {
  requestingDashboard: false,
  requestingCompare: false,
  requestingDashboards: false,
  preparingDashboards: false,
  preparingCompare: false,
  updatingDashboard: false,
  noDashboardData: false,
  widgetSwitch: false,
  compare: false,
  fullScreen: false,
  selected: '',
  compareSelected: '',
  tasks: [],
  mappings: [],
  metrics: [],
  dashboards: [],
  activeDashboard: {},
  compareToDashboard: {},
  selectedDashboard: {},
  comparedDashboard: {},
  demoMode: false,
  generatedDashboard: '',
  smartSearchPrimary: ''
}

const dashboard = (state = INITIAL_STATE, action) => {
  const { payload } = action

  switch (action.type) {
    case REQUEST_ERROR_RESET:
      return {
        ...state,
        requestingDashboard: false,
        requestingDashboards: false,
        preparingDashboards: false
      }

    case TOGGLE_DASHBOARD_FULLSCREEN:
      return {
        ...state,
        fullScreen: !state.fullScreen
      }

    case REQUEST_DASHBOARDS:
      return {
        ...state,
        requestingDashboards: true
      }

    case RECEIVE_DASHBOARDS:
      return {
        ...state,
        requestingDashboards: INITIAL_STATE.requestingDashboard
      }

    case REQUEST_DASHBOARD:
      return {
        ...state,
        requestingDashboard: true,
        noDashboardData: INITIAL_STATE.noDashboardData,
        activeDashboard: INITIAL_STATE.activeDashboard
      }

    case RECEIVE_DASHBOARD:
      return {
        ...state,
        requestingDashboard: INITIAL_STATE.requestingDashboard,
        preparingDashboards: true
      }

    case REQUEST_COMPARE_DASHBOARD:
      return {
        ...state,
        requestingCompare: true,
        noDashboardData: INITIAL_STATE.noDashboardData,
        compareToDashboard: INITIAL_STATE.compareToDashboard,
        comparedDashboard: INITIAL_STATE.comparedDashboard
      }

    case RECEIVE_COMPARE_DASHBOARD:
      return {
        ...state,
        requestingCompare: false,
        preparingCompare: true
      }

    case PREPARE_DASHBOARD_DATA:
      return {
        ...state,
        preparingDashboards: true
      }

    case SET_PRIMARY_DASHBOARD: {
      return {
        ...state,
        selected: payload
      }
    }

    case SET_COMPARE_DASHBOARD: {
      return {
        ...state,
        compareSelected: payload
      }
    }

    case SET_DASHBOARD_DATA: {
      const activeDashboard = payload
      const { visuals } = activeDashboard
      return {
        ...state,
        activeDashboard,
        preparingDashboards: visuals.length === 0
      }
    }

    case SET_NO_DASHBOARD_DATA:
      return {
        ...state,
        noDashboardData: true,
        preparingDashboards: INITIAL_STATE.preparingDashboards
      }

    case SET_DASHBOARD_LIST: {
      const list = []
      if (payload.length > 0) {
        payload.map(({ id, title, type }) =>
          list.push({ value: id, label: title, type })
        )
      }

      return {
        ...state,
        dashboards: list
      }
    }

    case SET_DASHBOARD_TASKS: {
      const tasks = payload
      const _massagedTasks = []
      tasks.map(task =>
        _massagedTasks.push({ label: task.name, value: task.id })
      )
      return {
        ...state,
        tasks: _massagedTasks
      }
    }

    case SET_TASK_MAPPINGS: {
      const _mappings = []
      const _metrics = []
      const keys = Object.keys(payload)
      for (let i = 0; i < keys.length; i++) {
        const key = keys[i]
        if (payload[key] === 'text' || payload[key] === 'date') {
          _mappings.push({
            label: key,
            value: key,
            type: payload[key]
          })
        } else {
          _metrics.push({
            label: key,
            value: key,
            type: payload[key]
          })
        }
      }
      return {
        ...state,
        metrics: _metrics,
        mappings: _mappings
      }
    }

    case SET_KIBANA_DASHBOARD: {
      const { activeDashboard, selectedDashboard } = state
      const { visuals, queries } = activeDashboard
      return {
        ...state,
        activeDashboard:
          Object.keys(payload).length !== 0
            ? prepareKibana(payload, visuals, queries, selectedDashboard)
            : {},
        preparingDashboards: false
      }
    }

    case SET_COMPARE_TO_DASHBOARD: {
      const { compareToDashboard } = state
      const { visuals, queries } = compareToDashboard
      return {
        ...state,
        compareToDashboard:
          Object.keys(payload).length !== 0
            ? prepareKibana(payload, visuals, queries)
            : {},
        preparingCompare: false
      }
    }

    case SET_CURRENT_DASHBOARD_INFO:
      return {
        ...state,
        selectedDashboard: {
          ...payload.data,
          ...payload.range
        },
        selected: payload.selected
      }

    case SET_COMPARED_DASHBOARD_INFO: {
      return {
        ...state,
        comparedDashboard: {
          ...payload.data,
          ...payload.range
        },
        compareSelected: payload.selected
      }
    }

    case RESET_DASHBOARD_DATA:
      return {
        ...INITIAL_STATE,
        dashboards: state.dashboards
      }

    case REMOVE_DASHBOARD_WIDGET:
      return {
        ...state,
        activeDashboard: removeDashboardWidget(state.activeDashboard, payload)
      }

    case UPDATE_DASHBOARD_START:
      return {
        ...state,
        updatingDashboard: true
      }

    case UPDATE_DASHBOARD_FINISH:
      return {
        ...state,
        updatingDashboard: INITIAL_STATE.updatingDashboard
      }

    case ADD_ITEM_DASHBOARD_LIST: {
      const { dashboards } = state
      const { id, title } = payload
      dashboards.push({
        value: id,
        label: title
      })
      return {
        ...state,
        dashboards
      }
    }

    case REMOVE_ITEM_DASHBOARD_LIST:
      return {
        ...state,
        dashboards: state.dashboards.filter(({ value }) => value !== payload)
      }

    case SET_GENERATED_DASHBOARD: {
      if (payload) {
        const { dashboards } = state
        dashboards.push(payload)
        return {
          ...state,
          generatedDashboard: payload,
          dashboards
        }
      }

      return {
        ...state,
        generatedDashboard: INITIAL_STATE.generatedDashboard
      }
    }

    case REORDER_DASHBOARD_WIDGETS: {
      return {
        ...state,
        activeDashboard: {
          ...state.activeDashboard,
          visuals: payload.visuals,
          queries: payload.queries
        }
      }
    }

    case TOGGLE_COMPARE_TO_DASHBOARD:
      return {
        ...state,
        compare: !state.compare,
        compareSelected: state.compare
          ? INITIAL_STATE.compareSelected
          : state.compareSelected,
        compareToDashboard: state.compare
          ? INITIAL_STATE.compareToDashboard
          : state.compareToDashboard
      }

    case SET_DATE_RANGE: {
      return {
        ...state,
        selectedDashboard: {
          ...state.selectedDashboard,
          dateFrom: payload.from,
          dateTo: payload.to
        }
      }
    }

    case SET_DATE_RANGE_COMPARE: {
      return {
        ...state,
        comparedDashboard: {
          ...state.comparedDashboard,
          dateFrom: payload.from,
          dateTo: payload.to
        }
      }
    }

    case SET_SMART_SEARCH_PRIMARY:
      return {
        ...state,
        smartSearchPrimary: payload
      }

    case TOGGLE_WIDGET_SWITCH:
      return {
        ...state,
        widgetSwitch: payload
      }

    default:
      return state
  }
}

export const requestDashboards = () => ({
  type: REQUEST_DASHBOARDS
})

export const receiveDashboards = () => ({
  type: RECEIVE_DASHBOARDS
})

export const requestDashboard = () => ({
  type: REQUEST_DASHBOARD
})

export const receiveDashboard = () => ({
  type: RECEIVE_DASHBOARD
})

export const requestCompareDashboard = () => ({
  type: REQUEST_COMPARE_DASHBOARD
})

export const receiveCompareDashboard = () => ({
  type: RECEIVE_COMPARE_DASHBOARD
})

export const setDashboard = (selected = {}) => ({
  type: SET_PRIMARY_DASHBOARD,
  payload: selected
})

export const setCompareDashboard = (selected = {}) => ({
  type: SET_COMPARE_DASHBOARD,
  payload: selected
})

export const setDashboardList = (data = []) => ({
  type: SET_DASHBOARD_LIST,
  payload: data
})

export const setDashboardData = (dashboardData = {}) => dispatch => {
  const { data = [] } = dashboardData
  if (!data || data.length === 0) {
    dispatch({
      type: SET_NO_DASHBOARD_DATA
    })
    return
  }
  awaitResult(dashboardData)
    .then(res => {
      dispatch({
        type: SET_DASHBOARD_DATA,
        payload: res
      })
    })
    .catch(() =>
      dispatch({
        type: SET_NO_DASHBOARD_DATA
      })
    )
}

export const toggleFullScreen = () => ({
  type: TOGGLE_DASHBOARD_FULLSCREEN
})

export const setKibanaDashboard = (args = {}) => ({
  type: SET_KIBANA_DASHBOARD,
  payload: args
})

export const setCompareToDashboard = (args = {}) => ({
  type: SET_COMPARE_TO_DASHBOARD,
  payload: args
})

export const setDashboardTasks = ({ tasks = [] }) => ({
  type: SET_DASHBOARD_TASKS,
  payload: tasks
})

export const setTaskMappings = ({ mappings = {} }) => ({
  type: SET_TASK_MAPPINGS,
  payload: mappings
})

export const setSelectedDashboard = (data = {}, selected = {}, range = {}) => ({
  type: SET_CURRENT_DASHBOARD_INFO,
  payload: { data, selected, range }
})

export const setComparedDashboard = (data = {}, selected = {}, range = {}) => ({
  type: SET_COMPARED_DASHBOARD_INFO,
  payload: { data, selected, range }
})

export const resetDashboard = () => ({
  type: RESET_DASHBOARD_DATA
})

export const removeWidget = id => ({
  type: REMOVE_DASHBOARD_WIDGET,
  payload: id
})

export const updateDashboardStart = () => ({
  type: UPDATE_DASHBOARD_START
})

export const updateDashboardFinish = () => ({
  type: UPDATE_DASHBOARD_FINISH
})

export const addDashboard = (args = {}) => ({
  type: ADD_ITEM_DASHBOARD_LIST,
  payload: args
})

export const removeDashboard = value => ({
  type: REMOVE_ITEM_DASHBOARD_LIST,
  payload: value
})

export const setGeneratedDashboard = id => ({
  type: SET_GENERATED_DASHBOARD,
  payload: id
})

export const reorderWidgets = value => ({
  type: REORDER_DASHBOARD_WIDGETS,
  payload: value
})

export const toggleCompare = () => ({
  type: TOGGLE_COMPARE_TO_DASHBOARD
})

// Date Pickers
export const setDateRangeCompare = range => ({
  type: SET_DATE_RANGE_COMPARE,
  payload: range
})

export const setDateRange = range => ({
  type: SET_DATE_RANGE,
  payload: range
})

export const setSmartSearchPrimary = search => ({
  type: SET_SMART_SEARCH_PRIMARY,
  payload: search
})

export const toggleWigetSwitch = (toggle = false) => ({
  type: TOGGLE_WIDGET_SWITCH,
  payload: toggle
})

export default dashboard
