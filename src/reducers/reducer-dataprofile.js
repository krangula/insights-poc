export const prefix = 'rpa-benchmarking/dataProfile/'

export const SET_DATAPROFILE = `${prefix}SET_DATAPROFILE`

export const SET_PROFILE_VARIABLES = `${prefix}SET_PROFILE_VARIABLES`
export const SET_PROFILE_VARIABLES_DATATYPE = `${prefix}SET_PROFILE_VARIABLES_DATATYPE`
export const SET_PROFILE_VARIABLES_ENABLED = `${prefix}SET_PROFILE_VARIABLES_ENABLED`

export const SET_DISTINCT_VALUES = `${prefix}SET_DISTINCT_VALUES`

export const INITIAL_DATAPROFILE_DATA = `${prefix}INITIAL_DATAPROFILE_DATA`

export const SET_PREVIEW_DATA = `${prefix}SET_PREVIEW_DATA`

const INITIAL_STATE = {
  dataProfileData: [],
  distinctValues: [],
  initialDataProfile: [],
  previewData: []
}

const dataProfile = (state = INITIAL_STATE, action) => {
  const { payload } = action

  switch (action.type) {
    case SET_DATAPROFILE: {
      return {
        ...state,
        dataProfileData: payload
      }
    }
    case SET_PROFILE_VARIABLES: {
      return {
        ...state,
        dataProfileData: [...state.dataProfileData].map(profileObject => ({
          profileVariables: profileObject.profileVariables.map(val => {
            if (val.variableName === payload.row.variableName) {
              return { ...val, displayName: payload.displayName }
            }
            return val
          })
        }))
      }
    }

    case SET_PROFILE_VARIABLES_DATATYPE: {
      return {
        ...state,
        dataProfileData: [...state.dataProfileData].map(profileObject => ({
          profileVariables: profileObject.profileVariables.map(val => {
            if (val.variableName === payload.row.variableName) {
              return !payload.row.defaultDatatype
                ? {
                    ...val,
                    attributeType: payload.selected,
                    defaultDatatype: payload.row.attributeType
                  }
                : { ...val, attributeType: payload.selected }
            }
            return val
          })
        }))
      }
    }

    case SET_PROFILE_VARIABLES_ENABLED: {
      return {
        ...state,
        dataProfileData: [...state.dataProfileData].map(profileObject => ({
          profileVariables: profileObject.profileVariables.map(val => {
            if (val.variableName === payload.row.variableName) {
              return { ...val, enabled: payload.selected }
            }
            return val
          })
        }))
      }
    }
    case SET_DISTINCT_VALUES: {
      return {
        ...state,
        distinctValues: payload
      }
    }
    case INITIAL_DATAPROFILE_DATA:
      return {
        ...state,
        initialDataProfile: payload
      }
    case SET_PREVIEW_DATA:
      return {
        ...state,
        previewData: payload
      }
    default:
      return state
  }
}

export const setDataprofile = data => ({
  type: SET_DATAPROFILE,
  payload: data
})

export const setProfileVariables = data => ({
  type: SET_PROFILE_VARIABLES,
  payload: data
})

export const setProfileVariablesDataType = data => ({
  type: SET_PROFILE_VARIABLES_DATATYPE,
  payload: data
})

export const setProfileVariablesEnabled = data => ({
  type: SET_PROFILE_VARIABLES_ENABLED,
  payload: data
})

export const setDistinctValues = data => ({
  type: SET_DISTINCT_VALUES,
  payload: data
})

export const setInitialDataProfile = initialDataProfile => ({
  type: INITIAL_DATAPROFILE_DATA,
  payload: initialDataProfile
})

export const setPreviewData = data => ({
  type: SET_PREVIEW_DATA,
  payload: data
})

export default dataProfile
