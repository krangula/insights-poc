export const prefix = 'rpa-benchmarking/global/'

export const REQUEST_ERROR_RESET = 'REQUEST_ERROR_RESET'
export const REQUEST_ERROR_SET = `${prefix}REQUEST_ERROR_SET`

const INITIAL_STATE = {
  requestError: false,
  errorPayload: {}
}

const main = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case REQUEST_ERROR_SET:
      return {
        requestError: true,
        errorPayload: action.payload
      }

    case REQUEST_ERROR_RESET:
      return INITIAL_STATE

    default:
      return state
  }
}

export const requestError = (args = {}) => ({
  type: REQUEST_ERROR_SET,
  payload: args
})

export const requestErrorReset = () => ({
  type: REQUEST_ERROR_RESET
})

export default main
