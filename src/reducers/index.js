import { combineReducers } from 'redux'
import { reducer as reduxFormReducer } from 'redux-form'
import main from './reducer-global'
import bots from './reducer-bots'
import dashboard from './reducer-dashboard'
import login from './reducer-login'
import control from './reducer-control'
import dataProfile from './reducer-dataprofile'

export default combineReducers({
  main,
  bots,
  dashboard,
  dataProfile,
  login,
  control,
  form: reduxFormReducer
})
