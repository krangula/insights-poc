import React, { PureComponent, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { Dialog, DialogContent, DialogActions, Button } from '@material-ui/core'
import html2canvas from 'html2canvas'
import { withSnackbar } from 'notistack'
import styles from '../../../styles'
import ButtonComponent from './button-component'
import { strings } from '../../../../locales/i18n'

class ScreenshotButton extends PureComponent {
  constructor(props) {
    super(props)
    this._takeScreenShot = this._takeScreenShot.bind(this)
    this._handleClose = this._handleClose.bind(this)

    this.state = {
      showScreenshot: false,
      component: null
    }
  }

  _handleClose() {
    this.setState({
      showScreenshot: false,
      component: null
    })
  }

  _takeScreenShot() {
    const element = document.getElementById('dashboard')

    html2canvas(element, {
      logging: true,
      background: '#FFFFFF',
      width: element.scrollWidth,
      height: element.scrollHeight
    }).then(canvas => {
      const data = canvas.toDataURL('image/jpeg', 1.0)
      const src = encodeURI(data)
      this.setState({
        showScreenshot: true,
        component: (
          <img alt="html2canvas" src={src} style={{ maxWidth: '100%' }} />
        )
      })
    })
  }

  render() {
    const { showScreenshot, component } = this.state

    return (
      <Fragment>
        <Dialog maxWidth="xl" open={showScreenshot} onClose={this._handleClose}>
          <DialogContent>{component}</DialogContent>
          <DialogActions>
            <Button color="secondary" onClick={this._handleClose}>
              {strings('buttons.label-close')}
            </Button>
          </DialogActions>
        </Dialog>
        <ButtonComponent
          fullWidth
          icon="camera"
          handler={this._takeScreenShot}
          label="Screenshot"
        />
      </Fragment>
    )
  }
}

export default withStyles(styles, { withTheme: true })(
  withSnackbar(ScreenshotButton)
)
