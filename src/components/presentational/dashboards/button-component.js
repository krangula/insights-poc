import React from 'react'
import { Button, Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Icon from '../icon'

const styles = () => ({
  label: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    '& i': {
      marginRight: 2
    }
  }
})

const ButtonComponent = ({
  classes,
  label,
  icon,
  handler,
  disabled,
  style,
  variant,
  fullWidth = false
}) => (
  <Button
    className={classes.label}
    fullWidth={fullWidth}
    disabled={disabled}
    variant={variant || 'outlined'}
    color="secondary"
    style={{ marginRight: 10, ...style }}
    onClick={handler}>
    {icon && <Icon name={icon} />}
    <Typography variant="button">{label}</Typography>
  </Button>
)

export default withStyles(styles, { withTheme: true })(ButtonComponent)
