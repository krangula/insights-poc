import React, { PureComponent, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import {
  MenuList,
  MenuItem,
  Dialog,
  DialogTitle,
  DialogContent,
  ClickAwayListener,
  DialogContentText,
  DialogActions,
  TextField,
  Grow,
  Popper,
  Paper
} from '@material-ui/core'
import html2canvas from 'html2canvas'

import ButtonComponent from './button-component'
import PdfButton from './pdf-button'
import { strings } from '../../../../locales/i18n'
import styles from '../../../styles'

class DashboardButtons extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      save: false,
      saveAs: false,
      doDelete: false,
      showScreenshot: false,
      anchorEl: null
    }
  }

  delete = () => {
    this.setState({
      open: true,
      doDelete: true
    })
  }

  save = () => {
    this.setState({
      open: true,
      save: true
    })
  }

  saveAs = () => {
    this.setState({
      open: true,
      saveAs: true
    })
  }

  handleClose = () => {
    this.setState(
      {
        open: false
      },
      () =>
        setTimeout(
          () =>
            this.setState({
              save: false,
              saveAs: false,
              doDelete: false,
              showScreenshot: false
            }),
          250
        )
    )
  }

  _handleSaveAs = () => {
    const { dashboard, description } = this.state
    const { saveDashboardAs } = this.props
    this.setState(
      {
        open: false,
        saveAs: false
      },
      () => saveDashboardAs(dashboard, description)
    )
  }

  _handleSave = () => {
    const { saveDashboard } = this.props
    this.setState(
      {
        open: false,
        save: false
      },
      () => saveDashboard()
    )
  }

  _takeScreenShot = () => {
    const element = document.getElementById('dashboard')

    html2canvas(element, {
      logging: true,
      background: '#FFFFFF',
      width: element.scrollWidth,
      height: element.scrollHeight
    }).then(canvas => {
      const data = canvas.toDataURL('image/jpeg', 1.0)
      const src = encodeURI(data)
      this.setState({
        showScreenshot: true,
        component: (
          <img alt="html2canvas" src={src} style={{ maxWidth: '100%' }} />
        )
      })
    })
  }

  _handleDelete = () => {
    const { deleteDashboard } = this.props
    this.setState(
      {
        open: false,
        doDelete: false
      },
      () => deleteDashboard()
    )
  }

  _handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
  }

  _handleClick = e => {
    this.setState({
      anchorEl: e.currentTarget
    })
  }

  _handleClose = () => {
    this.setState({
      anchorEl: null
    })
  }

  render() {
    const {
      open,
      save,
      saveAs,
      doDelete,
      dashboard,
      component,
      description,
      showScreenshot,
      anchorEl
    } = this.state
    const {
      theme,
      disabled,
      icon,
      handler,
      dashboardType,
      toggleCompare
    } = this.props

    let title
    let content

    if (showScreenshot) {
      content = component
    }

    if (saveAs) {
      title = strings('dialog.title-save-dashboard')
      content = (
        <Fragment>
          <DialogContentText>
            {strings('dialog.content-save-dashboard-as')}
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            onChange={this._handleChange('dashboard')}
            label={strings('dashboard.dashboard-name')}
            fullWidth
            autoComplete="off"
          />
          <TextField
            margin="dense"
            id="description"
            onChange={this._handleChange('description')}
            label={strings('dashboard.dashboard-description')}
            fullWidth
            autoComplete="off"
          />
        </Fragment>
      )
    }

    if (save) {
      title = strings('dialog.title-save-dashboard')
      content = (
        <DialogContentText id="alert-dialog-description">
          {strings('dialog.content-save-dashboard')}
        </DialogContentText>
      )
    }

    if (doDelete) {
      title = strings('dialog.are-you-sure')
      content = (
        <DialogContentText id="alert-dialog-description">
          {strings('dialog.confirm-no-undone')}
        </DialogContentText>
      )
    }

    // const menuOpts = {
    //   onClick: this._handleClose
    // }

    return (
      <div>
        <ButtonComponent
          label="Actions"
          icon="menu"
          handler={this._handleClick}
          variant={Boolean(anchorEl) && 'contained'}
        />
        <Popper
          id="actions-popper"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          placement="bottom-end"
          transition
          disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === 'bottom' ? 'center top' : 'left bottom'
              }}>
              <Paper>
                <ClickAwayListener onClickAway={this._handleClose}>
                  <MenuList>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-save-as')}
                        icon="save"
                        disabled={disabled}
                        handler={this.saveAs}
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-save')}
                        icon="save"
                        disabled={disabled || dashboardType === 'AUTO'}
                        handler={this.save}
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-compare')}
                        icon="compare"
                        handler={toggleCompare}
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-share')}
                        icon="mail_outline"
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-bookmark')}
                        icon="bookmark_border"
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-publish')}
                        icon="publish"
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label={strings('buttons.label-delete')}
                        icon="delete_outline"
                        handler={this.delete}
                        disabled={dashboardType === 'AUTO'}
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <ButtonComponent
                        label="Screenshot"
                        icon="camera"
                        handler={this._takeScreenShot}
                        fullWidth
                      />
                    </MenuItem>
                    <MenuItem>
                      <PdfButton icon={icon} handler={handler} />
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
        <Dialog
          maxWidth={showScreenshot && 'xl'}
          open={open || showScreenshot}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>{content}</DialogContent>
          <DialogActions>
            <ButtonComponent
              variant="contained"
              label={strings('buttons.label-cancel')}
              handler={this.handleClose}
            />
            {doDelete ? (
              <ButtonComponent
                variant="contained"
                label={strings('buttons.label-delete')}
                style={{ color: theme.palette.error.main }}
                handler={this._handleDelete}
              />
            ) : (
              !showScreenshot && (
                <ButtonComponent
                  variant="contained"
                  label={strings('buttons.label-save')}
                  disabled={saveAs && (!dashboard || !description)}
                  handler={saveAs ? this._handleSaveAs : this._handleSave}
                />
              )
            )}
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(DashboardButtons)
