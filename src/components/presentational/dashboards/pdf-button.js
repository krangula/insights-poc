import React, { PureComponent } from 'react'
import { withStyles } from '@material-ui/core/styles'
import html2canvas from 'html2canvas'
import JSPDF from 'jspdf'
import { withSnackbar } from 'notistack'
import styles from '../../../styles'
import ButtonComponent from './button-component'
import { strings } from '../../../../locales/i18n'

class PdfButton extends PureComponent {
  constructor(props) {
    super(props)
    this._generatePDF = this._generatePDF.bind(this)
  }

  _generatePDF() {
    const { enqueueSnackbar } = this.props
    const element = document.getElementById('dashboard')
    html2canvas(element, {
      logging: true,
      background: '#FFFFFF',
      width: element.scrollWidth,
      height: element.scrollHeight
    }).then(canvas => {
      enqueueSnackbar(strings('snackbar.message-download-pdf'), {
        variant: strings('snackbar.message-variant-info')
      })

      const imgData = canvas.toDataURL('image/jpeg', 1.0)
      const doc = new JSPDF('p', 'pt', 'a4', true)
      const width = doc.internal.pageSize.getWidth()
      const height = doc.internal.pageSize.getHeight()
      doc.addImage(imgData, 'JPEG', 0, 0, width, height)
      doc.save('sample-file.pdf')
    })
  }

  render() {
    return (
      <ButtonComponent
        fullWidth
        icon="picture_as_pdf"
        handler={this._generatePDF}
        label="Download PDF"
      />
    )
  }
}

export default withStyles(styles, { withTheme: true })(withSnackbar(PdfButton))
