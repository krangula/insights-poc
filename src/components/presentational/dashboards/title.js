import React from 'react'
import { Typography } from '@material-ui/core'

const Title = ({ title, variant }) => (
  <Typography variant={variant} style={{ marginLeft: 10 }}>
    {title}
  </Typography>
)

export default Title
