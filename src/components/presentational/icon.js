import React from 'react'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import styles from '../../styles'

const Icon = ({ name, classes, classProp }) => (
  <i
    className={classNames(
      'material-icons',
      classes.iconSpacing,
      classProp && classes[classProp]
    )}>
    {name}
  </i>
)

export default withStyles(styles, { withTheme: true })(Icon)
