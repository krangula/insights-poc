import React from 'react'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  Button
} from '@material-ui/core'
import { strings } from '../../../../locales/i18n'

const ApplicationError = React.memo(({ error, status, requestErrorReset }) => (
  <Dialog open={error}>
    <DialogTitle id="alert-dialog-title">
      {`Application Error - ${status}`}
    </DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        {strings('dialog.content-incomplete-request')}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button color="secondary" onClick={requestErrorReset}>
        {strings('buttons.label-ok')}
      </Button>
    </DialogActions>
  </Dialog>
))

export default ApplicationError
