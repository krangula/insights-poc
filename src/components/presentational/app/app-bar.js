import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { AppBar, Toolbar, Typography, Button } from '@material-ui/core'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import AccountPopover from '../accounts/popover'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const Header = props => {
  const { classes, dashboard, toggleDark, themeType } = props
  const { fullScreen } = dashboard
  return (
    <AppBar
      position="absolute"
      className={classNames(classes.appBar)}
      style={{ display: fullScreen ? 'none' : 'flex' }}>
      <Toolbar disableGutters>
        <div className="app-icon" />
        <Typography variant="h6" color="inherit" noWrap>
          {strings('title-product')}
        </Typography>
        <Button
          style={{
            position: 'absolute',
            right: 65
          }}
          variant={themeType === 'dark' ? 'contained' : 'outlined'}
          color="secondary"
          onClick={toggleDark}>
          {themeType === 'dark'
            ? strings('buttons.label-toggle-light')
            : strings('buttons.label-toggle-dark')}
        </Button>
        <AccountPopover />
      </Toolbar>
    </AppBar>
  )
}

const mapStateToProps = ({ dashboard }) => ({ dashboard })
const mapDispatchToProps = {}
const MemoizedHeader = React.memo(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Header)
)
export default withStyles(styles, { withTheme: true })(
  withRouter(React.memo(MemoizedHeader))
)
