import React from 'react'
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import styles from '../../styles'

const SimpleAlert = ({
  title,
  message,
  acceptHandler,
  cancelHandler,
  isOpen = false
}) => (
  <Dialog
    open={isOpen}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description">
    <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        {message}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button variant="contained" color="secondary" onClick={cancelHandler}>
        No
      </Button>
      <Button
        variant="contained"
        color="secondary"
        onClick={acceptHandler}
        autoFocus>
        Yes
      </Button>
    </DialogActions>
  </Dialog>
)

export default withStyles(styles, { withTheme: true })(SimpleAlert)
