import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Select from 'react-select'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import NoSsr from '@material-ui/core/NoSsr'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import Chip from '@material-ui/core/Chip'
import MenuItem from '@material-ui/core/MenuItem'
import CancelIcon from '@material-ui/icons/Cancel'
import { emphasize } from '@material-ui/core/styles/colorManipulator'

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: 250
  },
  input: {
    display: 'flex',
    padding: 0
  },
  valueContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    overflow: 'hidden'
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === 'light'
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
      0.08
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  }
})

function NoOptionsMessage(props) {
  const { selectProps, innerProps, children } = props
  const { classes } = selectProps
  return (
    <Typography
      color="textSecondary"
      className={classes.noOptionsMessage}
      {...innerProps}>
      {children}
    </Typography>
  )
}

function inputComponent({ inputRef, ...props }) {
  return <div className="input-component" ref={inputRef} {...props} />
}

function Control(props) {
  const { selectProps, innerProps, innerRef, children } = props
  const { classes } = selectProps
  const { textFieldProps } = selectProps
  return (
    <TextField
      fullWidth
      InputProps={{
        inputComponent,
        inputProps: {
          className: classes.input,
          inputRef: innerRef,
          children,
          ...innerProps
        }
      }}
      {...textFieldProps}
    />
  )
}

function Option(props) {
  const { innerProps, children, innerRef, isFocused, isSelected } = props
  return (
    <MenuItem
      buttonRef={innerRef}
      selected={isFocused}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
      {...innerProps}>
      {children}
    </MenuItem>
  )
}

function Placeholder(props) {
  const { selectProps, innerProps, children } = props
  const { classes } = selectProps
  return (
    <Typography
      color="textSecondary"
      className={classes.placeholder}
      {...innerProps}>
      {children}
    </Typography>
  )
}

function SingleValue(props) {
  const { selectProps, innerProps, children } = props
  const { classes } = selectProps
  return (
    <Typography className={classes.singleValue} {...innerProps}>
      {children}
    </Typography>
  )
}

function ValueContainer(props) {
  const { selectProps, children } = props
  const { classes } = selectProps
  return <div className={classes.valueContainer}>{children}</div>
}

function MultiValue(props) {
  const { children, selectProps, removeProps, isFocused } = props
  const { classes } = selectProps
  return (
    <Chip
      tabIndex={-1}
      label={children}
      className={classNames(classes.chip, {
        [classes.chipFocused]: isFocused
      })}
      onDelete={removeProps.onClick}
      deleteIcon={<CancelIcon {...removeProps} />}
    />
  )
}

function Menu(props) {
  const { selectProps, innerProps, children } = props
  const { classes } = selectProps
  const { paper } = classes
  return (
    <Paper square className={paper} {...innerProps}>
      {children}
    </Paper>
  )
}

const components = {
  Control,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
}

class IntegrationReactSelect extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      single: null,
      multi: null
    }
  }

  handleChange = name => args => {
    const { handleSelect, input, reduxForm } = this.props
    this.setState(
      {
        [name]: args
      },
      () => {
        if (handleSelect) {
          handleSelect(name, {
            [name]: args
          })
        }

        if (reduxForm) {
          const { onChange } = input
          const { value = '' } = args || {}
          onChange(value)
        }
      }
    )
  }

  render() {
    const { state } = this
    const {
      name,
      classes,
      theme,
      dataSource = [],
      meta = {},
      input = {},
      placeholder,
      disabled,
      error = false
    } = this.props
    const { initial } = meta
    const { name: inputName } = input

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        '& input': {
          font: 'inherit'
        }
      })
    }

    return (
      <NoSsr>
        <Select
          className={classNames('custom-react-select', {
            'select-error': error
          })}
          classes={classes}
          styles={selectStyles}
          options={dataSource}
          components={components}
          defaultValue={
            dataSource[dataSource.findIndex(({ value }) => value === initial)]
          }
          value={state[name || inputName]}
          onChange={this.handleChange(name || inputName)}
          placeholder={placeholder}
          isClearable
          isDisabled={disabled}
        />
      </NoSsr>
    )
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.objectOf(PropTypes.object).isRequired,
  theme: PropTypes.objectOf(PropTypes.object).isRequired
}

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect)
