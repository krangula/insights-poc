import React from 'react'
import moment from 'moment'
import { Card, CardContent, Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import styles from '../../styles'
import { strings } from '../../../locales/i18n'

const TooltipLabel = ({ payload = [], scale, classes }) => {
  if (payload && payload.length > 0) {
    const { payload: bar } = payload[0]
    const { time, x, process = [] } = bar
    let label = time || x

    if (scale === 'TIME') {
      const timestamp = moment.unix(label).format('lll')
      if (timestamp !== 'Invalid date') label = timestamp
    }

    // <Typography variant="h6" className={classes.primaryText1}>
    //   {label}
    // </Typography>

    // <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    //   <Typography
    //     variant="h6"
    //     className={classes.primaryText2}
    //     style={{ marginRight: 10 }}>
    //     Total {kpiType}:
    //   </Typography>
    //   <Typography variant="h6">{value}</Typography>
    // </div>
    return (
      <Card>
        <CardContent>
          {process.map(item => (
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Typography
                variant="h6"
                className={classes.primaryText3}
                style={{ marginRight: 10 }}>
                {item.time || item.x}:
              </Typography>
              <Typography variant="h6">{item.value}</Typography>
            </div>
          ))}
        </CardContent>
      </Card>
    )
  }

  return <p>{strings('messages.load-message')}</p>
}

export default withStyles(styles, { withTheme: true })(TooltipLabel)
