import React, { Component, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
  Card
} from '@material-ui/core'
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  Legend,
  Brush,
  Cell
} from 'recharts'
import classNames from 'classnames'
import humanizeNum from 'humanize-num'

import { FullScreenToggle, FullScreenExit } from './fullscreen'
import {
  chartHeight,
  chartWidth,
  xAxisLabel,
  yAxisLabel,
  CustomTick,
  ActiveItem
} from './chart-utils'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const COLORS = require('../../../mock/colors-random.json')

const ScatterComponent = ({ opts, stroke, textFill, bubble }) => {
  const {
    handleOpen,
    scale,
    zoom,
    animate = true,
    fill,
    activeLabel,
    itemOnClick,
    demo,
    metric,
    group,
    data = [],
    keys = [],
    height,
    width,
    fullScreen
  } = opts

  let scatterOpts = {
    dataKey: keys.length > 0 ? keys[0] : 'value',
    nameKey: keys.length > 0 && 'key',
    fill,
    isAnimationActive: false
  }

  if (scale === 'TIME') {
    scatterOpts = {
      ...scatterOpts,
      onClick: item => {
        if (handleOpen) handleOpen(item)
      }
    }
  }

  return (
    <ResponsiveContainer
      className={classNames({
        'recharts-bubble': bubble
      })}
      width={demo || fullScreen ? width : '99%'}
      height={height}>
      <ScatterChart {...opts}>
        {!demo && (
          <YAxis
            dataKey={keys.length > 0 ? keys[0] : 'value'}
            nameKey={keys.length > 0 && 'key'}
            type="number"
            tickFormatter={tick => humanizeNum(tick)}
            label={{
              ...yAxisLabel({ keys, metric, textFill }),
              angle: !bubble && 270
            }}
          />
        )}
        <XAxis
          height={50}
          dataKey={keys.length > 0 ? 'key' : 'name'}
          tick={<CustomTick keys={keys} textFill={textFill} />}
          interval={0}
          tickLine={{ transform: 'translate(0, -6)' }}
          label={xAxisLabel({ group, textFill })}
        />
        {bubble && (
          <ZAxis
            dataKey={keys.length > 0 ? keys[0] : 'value'}
            range={[64, 144]}
          />
        )}
        <Tooltip />
        {!demo && <Legend />}
        {!bubble && <CartesianGrid />}
        {zoom && (
          <Brush
            style={{ top: 50 }}
            dataKey="time"
            height={30}
            stroke={fill}
            startIndex={0}
            endIndex={Math.round(data.length / 8)}
            isAnimationActive={animate}
          />
        )}
        <Scatter {...scatterOpts} stroke={stroke} onClick={itemOnClick}>
          {data.map((entry, index) => (
            <Cell
              key={entry.key || entry.x}
              fill={
                activeLabel === (entry.key || entry.x)
                  ? '#1a75cf'
                  : bubble
                  ? COLORS.colors[index % COLORS.colors.length]
                  : fill
              }
            />
          ))}
        </Scatter>
      </ScatterChart>
    </ResponsiveContainer>
  )
}

class ScatterReChart extends Component {
  constructor(props) {
    super(props)
    this.state = {
      zoom: [],
      zoomTitle: '',
      showZoom: false,
      fullScreen: false,
      activeLabel: undefined,
      activeItem: {}
    }
  }

  _toggleFullscreen = () => {
    const { fullScreen } = this.state
    this.setState({
      fullScreen: !fullScreen
    })
  }

  render() {
    const { fullScreen, activeLabel } = this.state
    const {
      keys = [],
      plot = [],
      months = [],
      scale,
      kpiType,
      classes,
      dimensions = {},
      animate = true,
      chartTitle,
      demo = false,
      fullScreenApp = false,
      theme,
      metric,
      group,
      bubble
    } = this.props

    const { palette } = theme
    const { type } = palette

    const { height = chartHeight, width = chartWidth } = dimensions

    if (plot.length === 0) {
      return (
        <Card
          className={classNames(
            !fullScreen && classes.gridEmpty,
            fullScreenApp && classes.fullScreenApp
          )}>
          <Typography variant="h3">
            {strings('messages.no-data-found-message')}
          </Typography>
        </Card>
      )
    }

    let opts = {
      keys,
      data: scale === 'TIME' ? months : plot,
      height,
      width,
      ...dimensions,
      itemOnClick: args => {
        const { payload } = args
        const { key } = payload
        this.setState({
          activeLabel: activeLabel !== key ? key : undefined,
          activeItem: activeLabel !== key ? payload : {}
        })
      },
      activeLabel,
      kpiType,
      animate,
      scale,
      demo,
      metric,
      group,
      handleOpen: this._handleOpen,
      fill: theme.palette.secondary.main
    }

    const stroke = type === 'dark' ? '#FFFFFF' : palette.secondary.main
    const textFill = type === 'dark' ? '#FFFFFF' : 'rgba(0, 0, 0, 0.87)'

    if (fullScreen) {
      opts = {
        ...opts,
        height: 650,
        width: document.body.clientWidth / 1.2
      }
    }

    if (bubble) {
      opts = { ...opts, height: 80 }
    }

    return (
      <Card
        className={classNames(
          demo && classes.gridBoxFull,
          !fullScreen && classes.gridBox,
          fullScreenApp && classes.fullScreenApp
        )}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {!demo && (
            <Fragment>
              {chartTitle}
              <FullScreenToggle
                toggle={this._toggleFullscreen}
                fullScreen={fullScreen}
              />
            </Fragment>
          )}
        </div>

        <Dialog open={fullScreen} fullScreen>
          <DialogTitle style={{ textAlign: 'center' }}>
            <div className="fullscreen-title">
              <Typography variant="subtitle1">{chartTitle}</Typography>
              <FullScreenExit toggle={this._toggleFullscreen} />
            </div>
          </DialogTitle>
          <DialogContent>
            <div
              style={{
                display: 'grid',
                height: '100%',
                gridGap: '10px',
                gridTemplateColumns: activeLabel ? '80% 20%' : '100%'
              }}>
              <ScatterComponent
                opts={opts}
                stroke={stroke}
                textFill={textFill}
                bubble={bubble}
                fullScreen={fullScreen}
              />

              {activeLabel && (
                <ActiveItem classes={classes} label={activeLabel} value="" />
              )}
            </div>
          </DialogContent>
        </Dialog>

        <ScatterComponent
          opts={opts}
          stroke={stroke}
          textFill={textFill}
          bubble={bubble}
          fullScreen={fullScreen}
        />
      </Card>
    )
  }
}

export default withStyles(styles, { withTheme: true })(ScatterReChart)
