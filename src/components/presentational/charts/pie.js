import React, { Fragment, PureComponent } from 'react'
import classNames from 'classnames'
import humanize from 'humanize'
import {
  PieChart,
  Pie,
  Tooltip,
  Legend,
  Cell,
  Sector,
  ResponsiveContainer
} from 'recharts'
import {
  Card,
  Dialog,
  DialogTitle,
  DialogContent,
  Typography
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import {
  chartHeight,
  chartWidth,
  ActiveItem,
  legendStyles
} from './chart-utils'
import { FullScreenToggle, FullScreenExit } from './fullscreen'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const COLORS = require('../../../mock/colors-pie.json')

const RenderCustomizedLabel = props => {
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    value,
    stroke,
    name
  } = props
  const RADIAN = Math.PI / 180
  const radius = 25 + innerRadius + (outerRadius - innerRadius)
  const x = cx + radius * Math.cos(-midAngle * RADIAN)
  const y = cy + radius * Math.sin(-midAngle * RADIAN)

  return (
    <g>
      <text
        x={x}
        y={y}
        fill={stroke}
        textAnchor={x > cx ? 'start' : 'end'}
        dominantBaseline="central">
        {name}
      </text>
      <text
        x={x}
        y={y}
        dy={18}
        fill={stroke}
        textAnchor={x > cx ? 'start' : 'end'}
        dominantBaseline="central">
        {humanize.numberFormat(Math.round(value * 100) / 100)}
      </text>
    </g>
  )
}

const renderActiveShape = props => {
  const {
    cx,
    cy,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload
  } = props

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill="#4790d8"
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill="#1a75cf"
      />
    </g>
  )
}

class PieReChart extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      fullScreen: false,
      activeItem: {},
      activeIndex: undefined
    }
  }

  _toggleFullscreen = () => {
    const { fullScreen } = this.state
    this.setState({
      fullScreen: !fullScreen
    })
  }

  render() {
    const { fullScreen, activeIndex, activeItem } = this.state
    const {
      plot = [],
      keys = [],
      animate = true,
      classes,
      chartTitle = strings('charts.title-pie-chart'),
      demo = false,
      fullScreenApp = false,
      theme,
      dimensions = {}
    } = this.props
    const { palette } = theme
    const { type } = palette
    const stroke = type === 'dark' ? '#FFFFFF' : '#000000'
    const { height = chartHeight, width = chartWidth } = dimensions

    const { name, value } = activeItem

    let pieStyles = {}

    if (demo) {
      pieStyles = { alignItems: 'baseline' }
    }

    const opts = {
      onClick: (args, idx) =>
        this.setState({
          activeIndex: idx !== activeIndex ? idx : undefined,
          activeItem: idx !== activeIndex ? args : {}
        }),
      activeIndex,
      // innerRadius: 60,
      activeShape: renderActiveShape,
      label: demo ? false : <RenderCustomizedLabel stroke={stroke} />
    }

    if (plot.length === 0) {
      return (
        <Card
          className={classNames(
            !fullScreen && classes.gridEmpty,
            fullScreenApp && classes.fullScreenApp
          )}>
          <Typography variant="h3">
            {strings('messages.no-data-found-message')}
          </Typography>
        </Card>
      )
    }

    return (
      <Card
        style={pieStyles}
        className={classNames(
          demo && classes.gridBoxFull,
          !fullScreen && classes.gridBox,
          fullScreenApp && classes.fullScreenApp
        )}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {!demo && (
            <Fragment>
              {chartTitle}
              <FullScreenToggle
                toggle={this._toggleFullscreen}
                fullScreen={fullScreen}
              />
            </Fragment>
          )}
        </div>

        <Dialog open={fullScreen} fullScreen>
          <DialogTitle style={{ textAlign: 'center' }}>
            <div className="fullscreen-title">
              <Typography variant="subtitle1">{chartTitle}</Typography>
              <FullScreenExit toggle={this._toggleFullscreen} />
            </div>
          </DialogTitle>
          <DialogContent>
            {fullScreen && (
              <div
                style={{
                  display: 'grid',
                  height: '100%',
                  gridColumnGap: '10px',
                  gridTemplateColumns: name ? '80% 20%' : '100%'
                }}>
                <ResponsiveContainer>
                  <PieChart width={width} height={height}>
                    {keys.length > 0 ? (
                      keys.map(key => (
                        <Pie
                          {...opts}
                          key={key}
                          dataKey={key}
                          nameKey="key"
                          data={plot}
                          fill={theme.palette.secondary.main}>
                          {plot.map((entry, index) => (
                            <Cell
                              key={entry.key || entry.x}
                              fill={COLORS.colors[index % COLORS.colors.length]}
                            />
                          ))}
                        </Pie>
                      ))
                    ) : (
                      <Pie
                        {...opts}
                        dataKey="value"
                        data={plot}
                        fill="#8884d8"
                        isAnimationActive>
                        {plot.map((entry, index) => (
                          <Cell
                            key={entry.key || entry.x}
                            fill={COLORS.colors[index % COLORS.colors.length]}
                          />
                        ))}
                      </Pie>
                    )}

                    <Legend />

                    <Tooltip />
                  </PieChart>
                </ResponsiveContainer>
                {name && (
                  <ActiveItem classes={classes} label={name} value={value} />
                )}
              </div>
            )}
          </DialogContent>
        </Dialog>

        <div
          style={{
            display: 'flex',
            justifyContent: 'center'
          }}>
          {!fullScreen && (
            <ResponsiveContainer width={demo ? width : '55%'} height={height}>
              <PieChart>
                {keys.length > 0 ? (
                  keys.map(key => (
                    <Pie
                      {...opts}
                      key={key}
                      dataKey={key}
                      nameKey="key"
                      data={plot}
                      isAnimationActive={false}
                      fill={theme.palette.secondary.main}>
                      {plot.map((entry, index) => (
                        <Cell
                          key={entry.key || entry.x}
                          fill={COLORS.colors[index % COLORS.colors.length]}
                        />
                      ))}
                    </Pie>
                  ))
                ) : (
                  <Pie
                    {...opts}
                    dataKey="value"
                    data={plot}
                    fill="#8884d8"
                    isAnimationActive={animate}>
                    {plot.map((entry, index) => (
                      <Cell
                        key={entry.key || entry.x}
                        fill={COLORS.colors[index % COLORS.colors.length]}
                      />
                    ))}
                  </Pie>
                )}

                {!demo && (
                  <Legend
                    iconSize={10}
                    layout="vertical"
                    verticalAlign="middle"
                    wrapperStyle={legendStyles(theme.palette.type)}
                  />
                )}
                <Tooltip />
              </PieChart>
            </ResponsiveContainer>
          )}
        </div>
      </Card>
    )
  }
}

export default withStyles(styles, { withTheme: true })(PieReChart)
