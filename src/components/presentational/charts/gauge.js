import React, { PureComponent } from 'react'
import ReactSpeedometer from 'react-d3-speedometer'
import { Card, Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import { CustomLegend } from './chart-utils'
import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

class GaugeChart extends PureComponent {
  render() {
    const {
      plot = [],
      classes,
      chartTitle,
      value,
      minValue,
      maxValue,
      func,
      metric,
      group
    } = this.props

    if (plot.length === 0) {
      return (
        <Card className={classes.gridEmpty}>
          <Typography variant="h3">
            {strings('messages.no-data-found-message')}
          </Typography>
        </Card>
      )
    }

    return (
      <Card className={classes.gridBox}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {chartTitle}
        </div>

        <div
          style={{
            height: '300px',
            textAlign: 'center'
          }}>
          <ReactSpeedometer
            value={value}
            minValue={minValue}
            maxValue={maxValue}
            height={300}
            width={500}
            segments={3}
            needleTransitionDuration={4000}
            needleTransition="easeElastic"
            currentValueText="Current Value: #{value}"
            currentValuePlaceholderStyle="#{value}"
          />
        </div>
        <CustomLegend metric={metric} group={group} func={func} />
      </Card>
    )
  }
}

export default withStyles(styles, { withTheme: true })(GaugeChart)
