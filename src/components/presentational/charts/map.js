import React, { Component, Fragment } from 'react'
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker,
  Graticule
} from 'react-simple-maps'
import classNames from 'classnames'
import {
  Button,
  Card,
  Dialog,
  DialogTitle,
  DialogContent,
  Typography
} from '@material-ui/core'

import { Motion, spring } from 'react-motion'
import { withStyles } from '@material-ui/core/styles'

import { FullScreenToggle, FullScreenExit } from './fullscreen'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const data = require('./world-50m.json')

const cities = [
  { name: 'Zurich', coordinates: [8.5417, 47.3769] },
  { name: 'Singapore', coordinates: [103.8198, 1.3521] },
  { name: 'San Francisco', coordinates: [-122.4194, 37.7749] },
  { name: 'Sydney', coordinates: [151.2093, -33.8688] },
  { name: 'Lagos', coordinates: [3.3792, 6.5244] },
  { name: 'Buenos Aires', coordinates: [-58.3816, -34.6037] },
  { name: 'Shanghai', coordinates: [121.4737, 31.2304] }
]

const MapComponent = ({ zoom, center, handleCityClick, theme }) => (
  <Fragment>
    <Motion
      defaultStyle={{
        zoom: 1,
        x: 0,
        y: 20
      }}
      style={{
        zoom: spring(zoom, { stiffness: 210, damping: 20 }),
        x: spring(center[0], { stiffness: 210, damping: 20 }),
        y: spring(center[1], { stiffness: 210, damping: 20 })
      }}>
      {({ zoom: zoomValue, x, y }) => (
        <ComposableMap
          projectionConfig={{ scale: 205 }}
          width={980}
          height={551}
          style={{
            width: '100%',
            height: 'auto'
          }}>
          <ZoomableGroup center={[x, y]} zoom={zoomValue}>
            <Geographies geography={data}>
              {(geographies, projection) =>
                geographies.map(
                  geography =>
                    geography.id !== '010' && (
                      <Geography
                        key={geography.id}
                        geography={geography}
                        projection={projection}
                        style={{
                          default: {
                            fill: '#ECEFF1',
                            stroke: '#607D8B',
                            strokeWidth: 0.75,
                            outline: 'none'
                          },
                          hover: {
                            fill: '#CFD8DC',
                            stroke: '#607D8B',
                            strokeWidth: 0.75,
                            outline: 'none'
                          },
                          pressed: {
                            fill: '#FF5722',
                            stroke: '#607D8B',
                            strokeWidth: 0.75,
                            outline: 'none'
                          }
                        }}
                      />
                    )
                )
              }
            </Geographies>
            <Graticule />
            <Markers>
              {cities.map(city => (
                <Marker key={city.name} marker={city} onClick={handleCityClick}>
                  <circle
                    cx={0}
                    cy={0}
                    r={6}
                    fill={theme.palette.secondary.main}
                    stroke="#DF3702"
                  />
                  <text
                    textAnchor="middle"
                    y={-10}
                    style={{
                      fill: theme.palette.default
                    }}>
                    {city.name}
                  </text>
                </Marker>
              ))}
            </Markers>
          </ZoomableGroup>
        </ComposableMap>
      )}
    </Motion>
  </Fragment>
)

class AnimatedMap extends Component {
  constructor(props) {
    super(props)
    this.state = {
      center: [0, 0],
      zoom: 1,
      fullScreen: false
    }
  }

  handleZoomIn = () => {
    const { zoom } = this.state
    this.setState({
      zoom: zoom * 2
    })
  }

  handleZoomOut = () => {
    const { zoom } = this.state
    this.setState({
      zoom: zoom / 2
    })
  }

  handleCityClick = city => {
    this.setState({
      zoom: 2,
      center: city.coordinates
    })
  }

  _handleReset = () => {
    this.setState({
      center: [0, 0],
      zoom: 1
    })
  }

  _toggleFullscreen = () => {
    const { fullScreen } = this.state
    this.setState({
      fullScreen: !fullScreen
    })
  }

  render() {
    const { fullScreen, zoom, center } = this.state
    const {
      classes,
      theme,
      chartTitle,
      demo = false,
      dimensions = {},
      fullScreenApp = false
    } = this.props
    const { width = 450 } = dimensions
    let mapStyles = {}

    if (demo) {
      mapStyles = { display: 'flex', width, alignItems: 'baseline' }
    }

    return (
      <Fragment>
        <Dialog open={fullScreen} fullScreen>
          <DialogTitle style={{ textAlign: 'center' }}>
            <div className="fullscreen-title">
              <Typography variant="subtitle">{chartTitle}</Typography>
              <Button
                variant="contained"
                color="secondary"
                onClick={this._handleReset}>
                {strings('buttons.label-reset')}
              </Button>
              <FullScreenExit toggle={this._toggleFullscreen} />
            </div>
          </DialogTitle>
          <DialogContent>
            {fullScreen && (
              <MapComponent
                theme={theme}
                zoom={zoom}
                center={center}
                handleCityClick={this.handleCityClick}
              />
            )}
          </DialogContent>
        </Dialog>

        <Card
          className={classNames(
            !fullScreen && classes.gridBox,
            demo && classes.gridBoxFull,
            fullScreenApp && classes.fullScreenApp
          )}>
          <div style={{ display: 'flex', alignItems: 'center', width }}>
            {!demo && (
              <Fragment>
                {chartTitle}
                <FullScreenToggle
                  toggle={this._toggleFullscreen}
                  fullScreen={fullScreen}
                />
              </Fragment>
            )}
          </div>
          <div style={mapStyles}>
            <MapComponent
              theme={theme}
              zoom={zoom}
              center={center}
              handleZoomIn={this.handleZoomIn}
              handleZoomOut={this.handleZoomOut}
              handleReset={this.handleReset}
              handleCityClick={this.handleCityClick}
            />
          </div>
        </Card>
      </Fragment>
    )
  }
}

export default withStyles(styles, { withTheme: true })(AnimatedMap)
