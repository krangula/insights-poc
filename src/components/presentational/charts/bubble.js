// import React, { Fragment, PureComponent } from 'react'
// import classNames from 'classnames'
// import humanize from 'humanize'
// import {
//   PieChart,
//   Pie,
//   Tooltip,
//   Legend,
//   Cell,
//   Sector,
//   ResponsiveContainer
// } from 'recharts'
// import {
//   Card,
//   Dialog,
//   DialogTitle,
//   DialogContent,
//   Typography
// } from '@material-ui/core'
// import { withStyles } from '@material-ui/core/styles'
//
// import ReactBubbleChart from 'react-bubble-chart'
// import { CustomLegend, ActiveItem } from './chart-utils'
// import { FullScreenToggle, FullScreenExit } from './fullscreen'
//
// import styles from '../../../styles'
// import { strings } from '../../../../locales/i18n'
//
// const COLORS = require('../../../mock/colors-pie.json')
//
// const data = [
//   {
//     _id: 1,
//     value: 50,
//     selected: false
//   },
//   {
//     _id: 1,
//     value: 150,
//     selected: false
//   },
//   {
//     _id: 1,
//     value: 30,
//     selected: false
//   }
// ]
//
// const colorLegend = [
//   // reds from dark to light
//   { color: '#67000d', text: 'Negative', textColor: '#ffffff' },
//   '#a50f15',
//   '#cb181d',
//   '#ef3b2c',
//   '#fb6a4a',
//   '#fc9272',
//   '#fcbba1',
//   '#fee0d2',
//   // neutral grey
//   { color: '#f0f0f0', text: 'Neutral' },
//   // blues from light to dark
//   '#deebf7',
//   '#c6dbef',
//   '#9ecae1',
//   '#6baed6',
//   '#4292c6',
//   '#2171b5',
//   '#08519c',
//   { color: '#08306b', text: 'Positive', textColor: '#ffffff' }
// ]
//
// const BubbleComponent = ({ height, width }) => (
//   <ReactBubbleChart
//     className="my-cool-chart"
//     colorLegend={colorLegend}
//     data={data}
//     selectedColor="#737373"
//     selectedTextColor="#d9d9d9"
//     fixedDomain={{ min: -1, max: 1 }}
//     legend
//     legendSpacing={0}
//     tooltip
//   />
// )
//
// class BubbleChartD3 extends PureComponent {
//   constructor(props) {
//     super(props)
//     this.state = {
//       fullScreen: false,
//       activeItem: {},
//       activeIndex: undefined
//     }
//   }
//
//   _toggleFullscreen = () => {
//     const { fullScreen } = this.state
//     this.setState({
//       fullScreen: !fullScreen
//     })
//   }
//
//   render() {
//     const { fullScreen, activeIndex, activeItem } = this.state
//     const {
//       plot = [],
//       keys = [],
//       kpiType,
//       animate = true,
//       classes,
//       chartTitle = strings('charts.title-pie-chart'),
//       demo = false,
//       fullScreenApp = false,
//       theme,
//       metric,
//       group,
//       dimensions = {}
//     } = this.props
//     const { palette } = theme
//     const { type } = palette
//     const stroke = type === 'dark' ? '#FFFFFF' : '#000000'
//     const { height = 480, width = 680 } = dimensions
//
//     const { name, value } = activeItem
//
//     let pieStyles = {}
//
//     if (demo) {
//       pieStyles = { alignItems: 'baseline' }
//     }
//
//     const opts = {
//       onClick: (args, idx) =>
//         this.setState({
//           activeIndex: idx !== activeIndex ? idx : undefined,
//           activeItem: idx !== activeIndex ? args : {}
//         }),
//       activeIndex
//     }
//
//     if (plot.length === 0) {
//       return (
//         <Card
//           className={classNames(
//             !fullScreen && classes.gridEmpty,
//             fullScreenApp && classes.fullScreenApp
//           )}>
//           <Typography variant="h3">
//             {strings('messages.no-data-found-message')}
//           </Typography>
//         </Card>
//       )
//     }
//
//     return (
//       <Card
//         style={pieStyles}
//         className={classNames(
//           demo && classes.gridBoxFull,
//           !fullScreen && classes.gridBox,
//           fullScreenApp && classes.fullScreenApp
//         )}>
//         <div style={{ display: 'flex', alignItems: 'center' }}>
//           {!demo && (
//             <Fragment>
//               {chartTitle}
//               <FullScreenToggle
//                 toggle={this._toggleFullscreen}
//                 fullScreen={fullScreen}
//               />
//             </Fragment>
//           )}
//         </div>
//
//         <Dialog open={fullScreen} fullScreen>
//           <DialogTitle style={{ textAlign: 'center' }}>
//             <div className="fullscreen-title">
//               <Typography variant="subtitle1">{chartTitle}</Typography>
//               <FullScreenExit toggle={this._toggleFullscreen} />
//             </div>
//           </DialogTitle>
//           <DialogContent>
//             {fullScreen && (
//               <div
//                 style={{
//                   display: 'grid',
//                   height: '100%',
//                   gridColumnGap: '10px',
//                   gridTemplateColumns: name ? '80% 20%' : '100%'
//                 }}>
//                 <BubbleComponent height={height} width={width} />
//                 {name && (
//                   <ActiveItem classes={classes} label={name} value={value} />
//                 )}
//               </div>
//             )}
//           </DialogContent>
//         </Dialog>
//
//         <div
//           style={{
//             display: 'flex',
//             justifyContent: 'center'
//           }}>
//           {!fullScreen && <BubbleComponent height={height} width={width} />}
//         </div>
//       </Card>
//     )
//   }
// }
//
// export default withStyles(styles, { withTheme: true })(BubbleChartD3)
