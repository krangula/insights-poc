import React from 'react'
import moment from 'moment'
import humanizeNum from 'humanize-num'
import {
  Button,
  Typography,
  Slide,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
import { strings } from '../../../../locales/i18n'

export const chartHeight = 450
export const chartWidth = 550

export const legendStyles = (type = '') => ({
  bottom: 10,
  right: 10,
  lineHeight: '24px',
  background: '#FFFFFF',
  paddingLeft: 5,
  boxShadow:
    '0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12)',
  backgroundColor: type === 'dark' && '#888'
})

export const yAxisLabel = ({ keys, metric, textFill }) => ({
  value: keys.length > 0 ? `${metric} (${keys[0]})` : metric,
  angle: -90,
  fontWeight: 'bold',
  position: 'insideLeft',
  fill: textFill
})

export const xAxisLabel = ({ group, textFill }) => ({
  value: group,
  fontWeight: 'bold',
  position: 'bottom',
  fill: textFill
})

const TypographyLabel = ({ label, value, themeType }) => (
  <div className={`custom-chart-label ${themeType}`}>
    <Typography
      variant="subtitle1"
      color="secondary"
      style={{ marginRight: 5 }}>
      {label}
    </Typography>
    <Typography variant="subtitle1">{value}</Typography>
  </div>
)

export const CustomLegend = ({ group, metric, func, keys, themeType = '' }) => (
  <div
    style={{
      textAlign: 'center',
      marginTop: 20
    }}>
    {(group || metric) && (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center'
        }}>
        {metric && (
          <TypographyLabel
            label={`${(func || keys[0]).charAt(0).toUpperCase() +
              (func || keys[0]).slice(1)}`}
            value={metric}
            themeType={themeType}
          />
        )}
        {group && (
          <TypographyLabel label="By" value={group} themeType={themeType} />
        )}
      </div>
    )}
  </div>
)

// <Typography variant="h6">{`${kpiType} (${func || keys[0]})`}</Typography>

export const Transition = props => <Slide direction="up" {...props} />

export const CustomTick = props => {
  const { x, y, payload, textFill } = props
  const { value } = payload

  return (
    <g transform={`translate(${x},${y})`}>
      <text x={0} y={0} dy={16} textAnchor="middle" fill={textFill}>
        {moment(value).isValid() ? moment(value).format('MM/DD/YYYY') : value}
      </text>
    </g>
  )
}

export const CustomizedLabel = props => {
  const { x, y, textFill, value } = props
  return (
    <text x={x} y={y} fill={textFill} fontSize={10} textAnchor="middle">
      {humanizeNum(value)}
    </text>
  )
}

export const ActiveItem = ({ classes, label, value }) => (
  <div
    style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
    <Table
      className={classes.table}
      style={{
        position: 'relative',
        zIndex: 1000,
        background: '#fafafa',
        border: '1px solid rgba(224, 224, 224, 1)'
      }}>
      <TableHead>
        <TableRow>
          <TableCell>{strings('charts.title-name')}</TableCell>
          <TableCell align="right">{strings('charts.title-value')}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow key={label}>
          <TableCell component="th" scope="row">
            {label}
          </TableCell>
          <TableCell align="right">{value}</TableCell>
        </TableRow>
      </TableBody>
    </Table>
    <Button style={{ marginTop: 10 }} variant="contained" disabled>
      Export Data
    </Button>
  </div>
)
