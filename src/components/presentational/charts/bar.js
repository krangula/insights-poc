import React, { PureComponent, Fragment } from 'react'
import humanizeNum from 'humanize-num'
import {
  BarChart,
  Brush,
  Bar,
  Legend,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Cell,
  ResponsiveContainer
} from 'recharts'
import classNames from 'classnames'
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
  Card
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import { FullScreenToggle, FullScreenExit } from './fullscreen'
import {
  chartHeight,
  chartWidth,
  xAxisLabel,
  yAxisLabel,
  CustomTick,
  ActiveItem
} from './chart-utils'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const ChartComponent = ({ opts, stroke, textFill }) => {
  const {
    handleOpen,
    scale,
    zoom,
    animate = true,
    fill,
    activeIndex,
    demo,
    metric,
    group,
    data = [],
    keys = [],
    height,
    width,
    fullScreen
  } = opts

  let lineOpts = {
    dataKey: keys.length > 0 ? keys[0] : 'value',
    nameKey: keys.length > 0 && 'key',
    barSize: 50,
    isAnimationActive: animate
  }

  if (scale === 'TIME') {
    lineOpts = {
      ...lineOpts,
      onClick: item => {
        if (handleOpen) handleOpen(item)
      }
    }
  }

  return (
    <ResponsiveContainer
      width={demo || fullScreen ? width : '99%'}
      height={height}>
      <BarChart {...opts}>
        {!demo && (
          <YAxis
            dataKey={keys.length > 0 ? keys[0] : 'value'}
            nameKey={keys.length > 0 && 'key'}
            type="number"
            tickFormatter={tick => humanizeNum(tick)}
            label={yAxisLabel({ keys, metric, textFill })}
          />
        )}
        <XAxis
          height={50}
          dataKey={keys.length > 0 ? 'key' : 'name'}
          tick={<CustomTick keys={keys} textFill={textFill} />}
          label={xAxisLabel({ group, textFill })}
        />
        <Tooltip />
        {!demo && <Legend />}
        <CartesianGrid />
        {zoom && (
          <Brush
            style={{ top: 50 }}
            dataKey="time"
            height={30}
            stroke={stroke}
            startIndex={0}
            endIndex={Math.round(data.length / 8)}
            isAnimationActive={animate}
          />
        )}
        <Bar {...lineOpts}>
          {data.map((entry, index) => (
            <Cell
              key={entry.key || entry.x}
              fill={activeIndex === index ? '#1a75cf' : fill}
            />
          ))}
        </Bar>
      </BarChart>
    </ResponsiveContainer>
  )
}

class BarReChart extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      zoom: [],
      zoomTitle: '',
      showZoom: false,
      fullScreen: false,
      activeIndex: undefined,
      activeItem: {}
    }
  }

  _handleOpen = data => {
    const { plot, payload } = data
    const { x } = payload

    this.setState(
      {
        showZoom: true,
        zoomTitle: x
      },
      () => {
        setTimeout(() => {
          this.setState({
            zoom: plot
          })
        }, 1000)
      }
    )
  }

  _handleClose = () => {
    this.setState({
      showZoom: false,
      zoom: []
    })
  }

  _toggleFullscreen = () => {
    const { fullScreen } = this.state
    this.setState({
      fullScreen: !fullScreen
    })
  }

  render() {
    const { fullScreen, activeIndex, activeItem } = this.state
    const {
      keys = [],
      plot = [],
      months = [],
      scale,
      kpiType,
      classes,
      dimensions = {},
      animate = true,
      chartTitle,
      demo = false,
      fullScreenApp = false,
      theme,
      metric,
      group
    } = this.props
    const { activeLabel, activePayload = [] } = activeItem
    const { value } = activePayload[0] || {}
    const { palette } = theme
    const { type } = palette
    const { height = chartHeight, width = chartWidth } = dimensions

    if (plot.length === 0) {
      return (
        <Card
          className={classNames(
            !fullScreen && classes.gridEmpty,
            fullScreenApp && classes.fullScreenApp
          )}>
          <Typography variant="h3">
            {strings('messages.no-data-found-message')}
          </Typography>
        </Card>
      )
    }

    let opts = {
      keys,
      data: scale === 'TIME' ? months : plot,
      height,
      width,
      ...dimensions,
      onClick: args => {
        const { activeTooltipIndex } = args
        this.setState({
          activeIndex:
            activeIndex !== activeTooltipIndex ? activeTooltipIndex : undefined,
          activeItem: activeIndex !== activeTooltipIndex ? args : {}
        })
      },
      activeIndex,
      kpiType,
      handleOpen: this._handleOpen,
      animate,
      scale,
      fill: theme.palette.secondary.main,
      metric,
      group,
      demo
    }

    const stroke = type === 'dark' ? '#FFFFFF' : palette.secondary.main
    const textFill = type === 'dark' ? '#FFFFFF' : 'rgba(0, 0, 0, 0.87)'

    if (fullScreen) {
      opts = {
        ...opts,
        height: 650,
        width: document.body.clientWidth / 1.2
      }
    }

    return (
      <Card
        className={classNames(
          demo && classes.gridBoxFull,
          !fullScreen && classes.gridBox,
          fullScreenApp && classes.fullScreenApp
        )}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          {!demo && (
            <Fragment>
              {chartTitle}
              <FullScreenToggle
                toggle={this._toggleFullscreen}
                fullScreen={fullScreen}
              />
            </Fragment>
          )}
        </div>

        <Dialog open={fullScreen} fullScreen>
          <DialogTitle style={{ textAlign: 'center' }}>
            <div className="fullscreen-title">
              <Typography variant="subtitle1">{chartTitle}</Typography>
              <FullScreenExit toggle={this._toggleFullscreen} />
            </div>
          </DialogTitle>
          <DialogContent>
            <div
              style={{
                display: 'grid',
                height: '100%',
                gridGap: '10px',
                gridTemplateColumns: activeLabel ? '80% 20%' : '100%'
              }}>
              <ChartComponent
                opts={opts}
                stroke={stroke}
                textFill={textFill}
                fullScreen={fullScreen}
              />

              {activeLabel && (
                <ActiveItem
                  classes={classes}
                  label={activeLabel}
                  value={value}
                />
              )}
            </div>
          </DialogContent>
        </Dialog>

        <ChartComponent
          opts={opts}
          stroke={stroke}
          textFill={textFill}
          fullScreen={fullScreen}
        />
      </Card>
    )
  }
}

export default withStyles(styles, { withTheme: true })(BarReChart)

// label={<CustomizedLabel textFill={textFill} />}
