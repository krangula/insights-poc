import React, { Fragment } from 'react'
import { IconButton } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Icon from '../icon'

import styles from '../../../styles'

const FullScreenToggle = ({ fullScreen, toggle }) => (
  <Fragment>
    {fullScreen ? (
      <FullScreenExit toggle={toggle} />
    ) : (
      <FullScreen toggle={toggle} />
    )}
  </Fragment>
)

const FullScreenRegular = ({ toggle }) => (
  <IconButton color="secondary" style={{ marginLeft: 5 }} onClick={toggle}>
    <Icon name="fullscreen" />
  </IconButton>
)

const FullScreenExitRegular = ({ toggle }) => (
  <IconButton color="secondary" style={{ marginLeft: 5 }} onClick={toggle}>
    <Icon name="fullscreen_exit" />
  </IconButton>
)

const FullScreen = withStyles(styles, { withTheme: true })(FullScreenRegular)
const FullScreenExit = withStyles(styles, { withTheme: true })(
  FullScreenExitRegular
)

export { FullScreenToggle, FullScreen, FullScreenExit }
