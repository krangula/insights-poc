import React from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { IconButton, Typography, Drawer, Divider } from '@material-ui/core'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'
import Icon from '../icon'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const linkStyle = {
  textDecoration: 'none',
  textAlign: 'center',
  marginTop: 10,
  marginBottom: 10
}

const Button = ({ children, active, label, classes }) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    {children}
    <Typography
      variant="button"
      color={active ? 'secondary' : 'inherit'}
      classes={{
        colorInherit: classes.inheritIconWhite
      }}>
      {label}
    </Typography>
  </div>
)

const NavLinks = props => {
  const { classes, location, dashboard } = props
  const { pathname } = location
  const { fullScreen } = dashboard
  return (
    <Drawer
      variant="permanent"
      classes={{
        paper: classNames(classes.drawerPaper)
      }}
      style={{ display: fullScreen ? 'none' : 'flex' }}
      open>
      <div className={classes.toolbar}>
        <Typography variant="h5" />
      </div>
      <Divider />

      <Link
        to="/business"
        onClick={e => e.preventDefault()}
        replace
        style={linkStyle}>
        <IconButton
          color={pathname === '/business' ? 'secondary' : 'inherit'}
          style={{ backgroundColor: 'transparent' }}
          classes={{
            colorInherit: classes.inheritIconWhite
          }}>
          <Button
            classes={classes}
            active={pathname === '/business'}
            label={strings('buttons.label-analyze')}>
            <Icon name="view_comfy" classProp="drawerIcon" />
          </Button>
        </IconButton>
      </Link>
      <Link to="/configure" replace style={linkStyle}>
        <IconButton
          color={pathname === '/configure' ? 'secondary' : 'inherit'}
          style={{ backgroundColor: 'transparent' }}
          classes={{
            colorInherit: classes.inheritIconWhite
          }}>
          <Button
            classes={classes}
            active={pathname === '/configure'}
            label={strings('buttons.label-configure')}>
            <Icon name="dashboard" classProp="drawerIcon" />
          </Button>
        </IconButton>
      </Link>
      <Link to="/workbench" replace style={linkStyle}>
        <IconButton
          color={pathname === '/workbench' ? 'secondary' : 'inherit'}
          style={{ backgroundColor: 'transparent' }}
          classes={{
            colorInherit: classes.inheritIconWhite
          }}>
          <Button
            classes={classes}
            active={pathname === '/workbench'}
            label="workbench">
            <Icon name="work" classProp="drawerIcon" />
          </Button>
        </IconButton>
      </Link>
    </Drawer>
  )
}

const mapStateToProps = ({ dashboard }) => ({ dashboard })
const mapDispatchToProps = {}
const MemoizedNavLinks = React.memo(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(NavLinks)
)
export default withStyles(styles, { withTheme: true })(
  withRouter(React.memo(MemoizedNavLinks))
)

// <Link to="/benchmark/bots" replace style={linkStyle}>
//   <IconButton
//     color={
//       pathname.indexOf('benchmark') !== -1 || pathname === '/'
//         ? 'secondary'
//         : 'default'
//     }
//     style={{ backgroundColor: 'transparent' }}>
//     <Button
//       active={pathname.indexOf('benchmark') !== -1 || pathname === '/'}
//       label="BenchMark">
//       <Icon name="av_timer" classProp="drawerIcon" />
//     </Button>
//   </IconButton>
// </Link>
