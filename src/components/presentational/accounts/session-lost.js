import React from 'react'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  Button
} from '@material-ui/core'
import { strings } from '../../../../locales/i18n'

const SessionLost = React.memo(({ loginValid }) => {
  if (!loginValid) {
    return (
      <Dialog open>
        <DialogTitle id="alert-dialog-title">
          {strings('dialog.title-session-lost')}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {strings('dialog.content-session-invalid')}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            color="secondary"
            onClick={() => {
              localStorage.removeItem('insight-token')
              window.location.reload()
            }}>
            {strings('buttons.label-reload')}
          </Button>
        </DialogActions>
      </Dialog>
    )
  }

  return null
})

export default SessionLost
