import React, { PureComponent } from 'react'
import { IconButton, Menu, MenuItem } from '@material-ui/core'
import AccountCircle from '@material-ui/icons/AccountCircle'
import styled from 'styled-components'
import { strings } from '../../../../locales/i18n'

const StyledPopover = styled.div`
  position: absolute;
  right: 10px;
`

class AccountPopover extends PureComponent {
  state = {
    anchorEl: null
  }

  _handleMenu = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  _handleClose = () => {
    this.setState({ anchorEl: null })
  }

  _handleLogout = () => {
    localStorage.removeItem('authToken')
    localStorage.removeItem('lastUsername')
    window.location.reload()
  }

  render() {
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)

    return (
      <StyledPopover>
        <IconButton
          aria-owns={open ? 'menu-appbar' : null}
          aria-haspopup="true"
          onClick={this._handleMenu}
          color="inherit">
          <AccountCircle />
        </IconButton>
        <Menu
          id="menu-appbar"
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          open={open}
          onClose={this._handleClose}>
          <MenuItem onClick={this._handleClose}>
            {strings('menu.item-profile')}
          </MenuItem>
          <MenuItem onClick={this._handleClose}>
            {strings('menu.item-my-account')}
          </MenuItem>
          <MenuItem onClick={this._handleLogout}>
            {strings('menu.item-logout')}
          </MenuItem>
        </Menu>
      </StyledPopover>
    )
  }
}

export default AccountPopover
