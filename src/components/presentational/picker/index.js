import React, { PureComponent } from 'react'
import moment from 'moment'
import { DateRangePicker } from 'react-date-range'
import { withStyles } from '@material-ui/core/styles'
import ButtonComponent from '../dashboards/button-component'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-end',
    alignItems: 'baseline'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  }
})

const dateFormat = 'MM-DD-YYYY'

class DateRange extends PureComponent {
  static getDerivedStateFromProps(props, state) {
    const { fromDate, toDate } = props
    const { _startDate, _endDate } = state
    const formattedStartDate = moment(_startDate).format('MM-DD-YYYY')
    const formattedEndDate = moment(_endDate).format('MM-DD-YYYY')
    const formattedFromDate = moment(fromDate).format('MM-DD-YYYY')
    const formattedToDate = moment(toDate).format('MM-DD-YYYY')

    return {
      _startDate:
        formattedFromDate !== formattedStartDate
          ? formattedFromDate
          : formattedStartDate,
      _endDate:
        formattedToDate !== formattedEndDate
          ? formattedToDate
          : formattedEndDate
    }
  }

  constructor(props) {
    super(props)

    this._setRange = this._setRange.bind(this)
    this._togglePicker = this._togglePicker.bind(this)

    const { fromDate, toDate } = props
    this.state = {
      key: 'selection',
      _startDate: moment(fromDate).format(dateFormat),
      _endDate: moment(toDate).format(dateFormat),
      startDate: new Date(fromDate),
      endDate: new Date(toDate),
      error: false,
      showPicker: false
    }
    this.datePickerDiv = React.createRef()
  }

  componentDidMount() {
    this.dismiss = window.addEventListener('click', event => {
      const parent =
        event.target.closest('#date-range-picker') ||
        event.target.closest('#date-range-picker-compare')
      if (!parent) {
        this.setState({
          showPicker: false
        })
      }
    })
  }

  componentWillUnmount() {
    if (this.dismiss) window.removeEventListener('click')
  }

  _setRange(range) {
    const { selection } = range
    const { startDate, endDate } = selection
    const { setFn } = this.props

    this.setState(
      {
        startDate,
        endDate,
        _startDate: moment(startDate).format(dateFormat),
        _endDate: moment(endDate).format(dateFormat)
      },
      () => {
        if (setFn) {
          setFn({
            from: moment(startDate).format(dateFormat),
            to: moment(endDate).format(dateFormat)
          })
        }
      }
    )
  }

  _togglePicker() {
    this.setState({
      showPicker: true
    })
  }

  render() {
    const { startDate, endDate, showPicker, key } = this.state
    const { classes, compare = false, theme, variant = 'outlined' } = this.props
    const { palette } = theme
    const { secondary } = palette
    const { main } = secondary
    const id = compare ? `date-range-picker-compare` : 'date-range-picker'

    return (
      <div id={id} style={{ position: 'relative' }}>
        <div
          ref={this.datePickerDiv}
          style={{
            position: 'absolute',
            zIndex: 6,
            right: 0,
            outline: 'none',
            top: 40,
            display: showPicker ? 'block' : 'none'
          }}>
          <DateRangePicker
            rangeColors={[main]}
            ranges={[{ startDate, endDate, key }]}
            onChange={this._setRange}
          />
        </div>
        <form className={classes.container} noValidate>
          <ButtonComponent
            style={{ margin: 0 }}
            icon="date_range"
            handler={this._togglePicker}
            label={`${moment(startDate).format(dateFormat)} - ${moment(
              endDate
            ).format(dateFormat)}`}
            variant={showPicker ? 'contained' : variant}
          />
        </form>
      </div>
    )
  }
}

// const MemoizedDateRange = React.memo(DateRange)
export default withStyles(styles, { withTheme: true })(DateRange)
