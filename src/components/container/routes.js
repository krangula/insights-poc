import React from 'react'
import { Switch, Route, Redirect /* Link */ } from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import BenchMark from './benchmark'
import BusinessInfo from './business'
import Dashboard from './dashboard'
import WorkBench from './workbench'
import Login from './login'

const AppRoutes = ({ session, location, activeSession }) => (
  <TransitionGroup>
    <CSSTransition key={location.pathname} classNames="my-node" timeout={320}>
      <Switch location={location}>
        <Route
          path="/"
          exact
          render={() => {
            if (session) {
              return <Redirect to="/configure" />
            }
            return <Login activeSession={activeSession} />
          }}
        />
        <Route
          path="/business"
          exact
          render={() => {
            if (!session) {
              return <Redirect to="/" />
            }

            return <BusinessInfo />
          }}
        />
        <Route
          path="/benchmark/:type(bots|runners|creators)"
          render={() => {
            if (!session) {
              return <Redirect to="/" />
            }

            return <BenchMark />
          }}
        />
        <Route
          path="/configure"
          render={() => {
            if (!session) {
              return <Redirect to="/" />
            }

            return <Dashboard />
          }}
        />
        <Route
          path="/workbench"
          render={() => {
            if (!session) {
              return <Redirect to="/" />
            }

            return <WorkBench />
          }}
        />
      </Switch>
    </CSSTransition>
  </TransitionGroup>
)

export default AppRoutes
