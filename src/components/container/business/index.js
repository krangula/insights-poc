import React, { Component } from 'react'
import { Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

class BusinessInfo extends Component {
  state = {
    loading: true
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.compareContainer}>
        <Typography variant="headline">
          {strings('business.list-header-goals')}
        </Typography>
        <ol>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-logic-support')}
          </li>
          <li>
            {strings('business.list-support-library')}
            <ul>
              <li style={{ color: '#8bc34a' }}>
                {strings('business.list-bar-chart')}
              </li>
              <li>{strings('business.list-scatter-chart')}</li>
              <li style={{ color: '#8bc34a' }}>
                {strings('business.list-pie-chart')}
              </li>
              <li style={{ color: '#8bc34a' }}>
                {strings('business.list-line-chart')}
              </li>
              <li>{strings('business.list-heat-maps')}</li>
              <li>{strings('business.list-map-components')}</li>
            </ul>
          </li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-ability-to-add-charts')}
          </li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-fullscreen-components')}
          </li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-full-screen-dashboard')}
          </li>
          <li>{strings('business.list-group-charts')}</li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-date-range-support')}
          </li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-auto-complete-to-search')}
          </li>
          <li>{strings('business.list-auto-dashboard-generation')}</li>
          <li>{strings('business.list-data-cache')}</li>
          <li>{strings('business.list-export-dashboard')}</li>
          <li style={{ color: '#8bc34a' }}>
            {strings('business.list-dashboard-screenshot')}
          </li>
          <li>{strings('business.list-dashboard-pdf')}</li>
        </ol>
      </div>
    )
  }
}

export default withStyles(styles, { withTheme: true })(BusinessInfo)
