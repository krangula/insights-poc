import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'
import {
  Grow,
  TextField,
  Button,
  InputAdornment,
  IconButton,
  Card,
  CardContent,
  Typography,
  CircularProgress
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import Lock from '@material-ui/icons/Lock'

import * as loginActions from 'reducers/reducer-login'
import { authenticate } from 'services/authenticate'
import { strings } from '../../../locales/i18n'

const styles = theme => ({
  container: {
    height: '100vh',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center'
  },
  textField: {
    width: '100%',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  dense: {
    marginTop: 16
  },
  menu: {
    width: 200
  },
  button: {
    margin: theme.spacing.unit,
    height: 56
  },
  cardRoot: {
    width: 400,
    height: 375
  }
})

class Login extends Component {
  constructor(props) {
    super(props)
    this._handleChange = this._handleChange.bind(this)
    this._handleSubmit = this._handleSubmit.bind(this)
    this._handleClickShowPassword = this._handleClickShowPassword.bind(this)
    this.state = {
      loading: false,
      error: false,
      showPassword: false,
      username: '',
      password: ''
    }
  }

  componentDidMount() {
    document.addEventListener('keypress', event => {
      const { keyCode } = event
      if (keyCode === 13) this._handleSubmit()
    })
  }

  _handleChange = prop => event => {
    this.setState({ [prop]: event.target.value })
  }

  _handleClickShowPassword() {
    const { showPassword } = this.state
    this.setState({
      showPassword: !showPassword
    })
  }

  _handleSubmit() {
    const { username, password } = this.state
    const { doLoginSuccess, activeSession, enqueueSnackbar } = this.props

    if (username === '' || password === '') {
      this.setState({
        error: true
      })
      return
    }

    this.setState({
      error: false,
      loading: true
    })
    // authenticate({ user: 'biadmin', pass: 'W0nderland' })
    authenticate({ user: username, pass: password })
      .then(response => {
        const { status } = response
        if (status === 200) {
          response.json().then(data => {
            doLoginSuccess(data, username)
            activeSession()
            localStorage.setItem('authToken', data.token)
            localStorage.setItem('lastUsername', username)
          })
        } else {
          this.setState({
            loading: false
          })
          enqueueSnackbar(
            strings('snackbar.message-invalid-user-credentials'),
            {
              variant: strings('snackbar.message-variant-error')
            }
          )
        }
      })
      .catch(() => {
        this.setState({
          loading: false
        })
        enqueueSnackbar(strings('snackbar.message-unknown-error'), {
          variant: strings('snackbar.message-variant-error')
        })
      })
  }

  render() {
    const { username, password, error, showPassword, loading } = this.state
    const { classes, theme } = this.props
    const { palette } = theme
    const { type } = palette

    return (
      <Grow in>
        <Card className={classes.cardRoot}>
          <CardContent>
            <form className={classes.container} noValidate autoComplete="off">
              {type === 'dark' ? (
                <div className="app-logo-white" />
              ) : (
                <div className="app-logo" />
              )}
              <Typography style={{ textAlign: 'center' }} variant="h6">
                {strings('title-product')}
              </Typography>
              <TextField
                error={error && username === ''}
                id="outlined-email-input"
                label={strings('inputs.label-username')}
                className={classes.textField}
                type="text"
                name="username"
                autoComplete="off"
                margin="normal"
                variant="outlined"
                value={username}
                onChange={this._handleChange('username')}
              />

              <TextField
                error={error && password === ''}
                id="outlined-password-input"
                label={strings('inputs.label-password')}
                className={classes.textField}
                type="password"
                autoComplete="off"
                margin="normal"
                variant="outlined"
                value={password}
                onChange={this._handleChange('password')}
                endadornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={this._handleClickShowPassword}>
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              <Button
                fullWidth
                variant="contained"
                color="secondary"
                className={classes.button}
                onClick={this._handleSubmit}>
                {loading ? (
                  <CircularProgress />
                ) : (
                  <Fragment>
                    <Lock />
                    <Typography variant="button">
                      {strings('buttons.label-login')}
                    </Typography>
                  </Fragment>
                )}
              </Button>
            </form>
          </CardContent>
        </Card>
      </Grow>
    )
  }
}

const mapStateToProps = ({ login }) => ({ login })
const mapDispatchToProps = {
  doLoginSuccess: loginActions.doLoginSuccess
}

const ConnectedLogin = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)

export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedLogin)
)
