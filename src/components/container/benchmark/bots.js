import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  BarChart,
  Scatter,
  ScatterChart
} from 'recharts'
import {
  Typography,
  Radio,
  RadioGroup,
  FormControl,
  FormLabel,
  FormControlLabel,
  Button
} from '@material-ui/core'
import CheckCircle from '@material-ui/icons/CheckCircle'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'

import ServiceLoader from '../../presentational/service-loader'
import * as Actions from '../../../reducers/reducer-bots'
import { mockService } from '../../../services/bots'
import styles from '../../../styles'
import { strings } from '../../../../locales/i18n'

const BotPerformance = ({ name, value, label, toggle, type }) => (
  <Button
    style={{
      minWidth: 150,
      margin: 10
    }}
    variant="contained"
    color="primary"
    onClick={() => toggle && toggle(name)}>
    {type === name && <CheckCircle color="secondary" />}
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
      }}>
      <Typography variant="h5" style={{ color: '#FFFFFF' }}>
        {value}
      </Typography>
      <Typography variant="subtitle1" style={{ color: '#FFFFFF' }}>
        {label}
      </Typography>
    </div>
  </Button>
)

class Bots extends PureComponent {
  state = {
    rpa: 'dev',
    benchmark: 'rpa',
    loading: false
  }

  handleEnvironmentChange = event => {
    const {
      enqueueSnackbar,
      setEnvironment,
      requestBots,
      receivedBots
    } = this.props
    const { target } = event
    const { value } = target

    setEnvironment(value)

    requestBots()

    mockService().then(() => {
      receivedBots()
      enqueueSnackbar(strings('snackbar.message-data-updated'), {
        variant: strings('snackbar.message-variant-success')
      })
    })
  }

  handleBenchmarkChange = event => {
    const {
      enqueueSnackbar,
      setBenchmark,
      requestBots,
      receivedBots
    } = this.props
    const { target } = event
    const { value } = target
    setBenchmark(value)

    requestBots()

    mockService().then(() => {
      receivedBots()
      enqueueSnackbar(strings('snackbar.message-data-updated'), {
        variant: strings('snackbar.message-variant-success')
      })
    })
  }

  handleChart = val => {
    const { toggle, requestBots, receivedBots, enqueueSnackbar } = this.props
    requestBots()
    mockService().then(() => {
      receivedBots()
      toggle(val)
      enqueueSnackbar(strings('snackbar.message-updated-chart-data'), {
        variant: strings('snackbar.message-variant-success')
      })
    })
  }

  render() {
    const { classes, bots } = this.props
    const { filters, requesting, toggles, botType, creators, sample } = bots
    const { rpa, benchmark } = filters

    return (
      <Fragment>
        {requesting && <ServiceLoader />}
        <div style={{ display: 'flex' }}>
          {toggles.map(item => (
            <BotPerformance
              key={item.name}
              toggle={this.handleChart}
              type={botType}
              {...item}
            />
          ))}
        </div>
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend" className={classes.formLabel}>
            {strings('forms.label-rpa-environments')}
          </FormLabel>
          <RadioGroup
            aria-label="Environments"
            name="environments"
            className={classes.group}
            value={rpa}
            onChange={this.handleEnvironmentChange}>
            <FormControlLabel
              value="dev"
              control={<Radio />}
              label={strings('forms.label-development')}
            />
            <FormControlLabel
              value="uat"
              control={<Radio />}
              label={strings('forms.label-uat')}
            />
            <FormControlLabel
              value="prod"
              control={<Radio />}
              label={strings('forms.label-production')}
            />
          </RadioGroup>
        </FormControl>

        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel component="legend" className={classes.formLabel}>
            {strings('forms.label-benchmark')}
          </FormLabel>
          <RadioGroup
            aria-label="Benchmark"
            name="benchmark"
            className={classes.group}
            value={benchmark}
            onChange={this.handleBenchmarkChange}>
            <FormControlLabel
              value="peers"
              control={<Radio />}
              label={strings('forms.label-industry-peers')}
            />
            <FormControlLabel
              value="rpa"
              control={<Radio />}
              label={strings('forms.label-rpa-benchmarks')}
            />
          </RadioGroup>
        </FormControl>

        <div
          style={{
            transition: 'opacity 0.6s ease',
            opacity: requesting ? 0.55 : 1
          }}>
          {botType === 'bots' && (
            <ResponsiveContainer width="100%" height={350}>
              <ComposedChart
                data={sample[filters.rpa]}
                margin={{ top: 20, right: 20, bottom: 20, left: 20 }}>
                <CartesianGrid stroke="#f5f5f5" />
                <XAxis
                  dataKey="time"
                  domain={['auto', 'auto']}
                  name="Time"
                  scale="time"
                  tickFormatter={unixTime => moment.unix(unixTime).format('ll')}
                  type="number"
                  padding={{ left: 20, right: 20 }}
                />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="value" barSize={20} fill="#ff7300" name="Bot" />
                <Line
                  type="monotone"
                  dataKey="value"
                  stroke="#ff7300"
                  name="Bot Curve"
                />
                {benchmark === 'peers' && (
                  <Bar
                    dataKey="peers"
                    barSize={20}
                    fill="#413ea0"
                    name="Peers"
                  />
                )}
                {benchmark === 'peers' && (
                  <Line
                    type="monotone"
                    dataKey="peers"
                    stroke="#413ea0"
                    name="Peer Curve"
                  />
                )}

                {benchmark === 'rpa' && (
                  <Bar
                    dataKey="bench"
                    barSize={20}
                    fill="#413ea0"
                    name="BenchMark"
                  />
                )}
                {benchmark === 'rpa' && (
                  <Line
                    type="monotone"
                    dataKey="bench"
                    stroke="#413ea0"
                    name="BenchMark Curve"
                  />
                )}
              </ComposedChart>
            </ResponsiveContainer>
          )}

          {botType === 'bot_runners' && (
            <ResponsiveContainer width="100%" height={350}>
              <BarChart
                data={sample[filters.rpa]}
                margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis
                  dataKey="time"
                  domain={['auto', 'auto']}
                  name="Time"
                  scale="time"
                  tickFormatter={unixTime => moment.unix(unixTime).format('ll')}
                  type="number"
                  padding={{ left: 20, right: 20 }}
                />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="value" stackId="a" fill="#8884d8" name="Bot" />
                <Bar dataKey="peers" stackId="a" fill="#82ca9d" name="Peers" />
                <Bar dataKey="bench" fill="#ffc658" name="BenchMark" />
              </BarChart>
            </ResponsiveContainer>
          )}

          {botType === 'bot_creators' && (
            <ResponsiveContainer width="100%" height={350}>
              <ScatterChart>
                <XAxis
                  dataKey="time"
                  domain={['auto', 'auto']}
                  name="Time"
                  tickFormatter={unixTime =>
                    moment(unixTime).format('HH:mm Do')
                  }
                  type="number"
                />
                <YAxis dataKey="value" name="Value" />

                <Scatter
                  data={creators}
                  line={{ stroke: '#666666' }}
                  lineJointType="monotoneX"
                  lineType="joint"
                  name="Values"
                />
              </ScatterChart>
            </ResponsiveContainer>
          )}
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ bots }) => ({
  bots
})

const mapDispatchToProps = {
  requestBots: Actions.requestBots,
  receivedBots: Actions.receivedBots,
  setEnvironment: Actions.setEnvironment,
  setBenchmark: Actions.setBenchmark,
  toggle: Actions.toggleBotDataType
}

const ConnectedBots = connect(
  mapStateToProps,
  mapDispatchToProps
)(Bots)
export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedBots)
)
