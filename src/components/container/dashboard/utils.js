import { getDashboardData, searchDashboardData } from 'services/dashboard'

export const processWidgets = ({
  dashboard,
  search,
  setDashboardData,
  updateStart,
  updateFinish
}) => {
  const { widgets, taskId } = dashboard
  const { dateFrom, dateTo } = dashboard
  const requests = []
  const customQueries = []

  updateStart()

  const getData = search ? searchDashboardData : getDashboardData

  for (let i = 0; i < widgets.length; i++) {
    const { query } = widgets[i]
    let _query = typeof query === 'string' ? JSON.parse(query) : query
    _query = typeof _query === 'string' ? JSON.parse(_query) : _query
    const { query: innerQuery } = _query
    const { bool: innerBool } = innerQuery
    const { must } = innerBool

    for (let d = 0; d < must.length - 1; d++) {
      const idx = must.findIndex(item => item.range)
      const { range } = must[idx]
      let { dateLogged } = range
      dateLogged = {
        ...dateLogged,
        gte: new Date(dateFrom).valueOf(),
        lte: new Date(dateTo).valueOf()
      }

      _query.query.bool.must[idx].range.dateLogged = dateLogged
    }

    requests.push(getData(_query, search, taskId))
    customQueries.push(_query)
  }

  Promise.all(requests)
    .then(responses => {
      if (responses.length === 0) {
        updateFinish()
        setDashboardData()
        return
      }

      const widgetPromises = []
      for (let r = 0; r < responses.length; r++) {
        const widgetResponse = responses[r]
        if (widgetResponse.status === 200) {
          widgetPromises.push(widgetResponse.json())
        }
      }

      Promise.all(widgetPromises).then(widgetData => {
        for (let w = 0; w < widgetData.length; w++) {
          const { id, meta: _meta } = widgets[w]
          const query = customQueries[w]
          const meta = _meta !== '' ? JSON.parse(_meta) : {}
          const { type } = meta

          setDashboardData({
            id,
            type,
            meta,
            query,
            dashboard: widgetData[w]
          })
        }
      })

      updateFinish()
    })
    .catch(() => setDashboardData())
}
