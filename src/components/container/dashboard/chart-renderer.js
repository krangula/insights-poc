import React, { Fragment } from 'react'
import humanize from 'humanize'
import { withStyles } from '@material-ui/core/styles'
import { Card, CardContent, Typography } from '@material-ui/core'
import HeatMap from 'react-heatmap-grid'

import Title from 'presentational/dashboards/title'
import PieChart from 'presentational/charts/pie'
import BarChart from 'presentational/charts/bar'
import GaugeChart from 'presentational/charts/gauge'
import LineChart from 'presentational/charts/line'
import ScatterReChart from 'presentational/charts/scatter'
import DataMap from 'presentational/charts/map'
import { CustomLegend } from 'presentational/charts/chart-utils'
import { strings } from '../../../../locales/i18n'

import styles from '../../../styles'

// //////////////////////////////////////////////////////////////////////////////////////////////
const data03 = [[5600, 1680, 330, 0, 0, 0], [0, 0, 0, 1710, 840, 1080]]
// //////////////////////////////////////////////////////////////////////////////////////////////

const ChartRenderer = props => {
  const {
    type,
    plot,
    keys,
    months,
    heatX,
    heatY,
    chartTitle,
    name: kpiType,
    scale,
    classes,
    theme,
    fullScreen,
    formValues = {}
  } = props

  const { metric, group } = formValues

  if (type === 'DONUT' || type === 'PIE') {
    return (
      <PieChart
        keys={keys}
        plot={plot}
        kpiType={kpiType}
        metric={metric}
        group={group}
        chartTitle={<Title title={chartTitle} variant="h6" />}
      />
    )
  }

  if (type === 'GAUGE') {
    const { fromField0, toField2 } = formValues
    const minimumValue = Number(fromField0)
    const maximumValue = Number(toField2)
    return (
      <Fragment>
        {plot.map(
          ({
            chartTitle: gaugeTitle = '',
            value: gaugeValue = 0,
            func = ''
          }) => (
            <GaugeChart
              key={minimumValue}
              value={gaugeValue && Number(gaugeValue.toFixed(2))}
              minValue={minimumValue}
              maxValue={maximumValue}
              plot={plot}
              func={func}
              metric={metric}
              group={group}
              chartTitle={<Title title={gaugeTitle} variant="h6" />}
            />
          )
        )}
      </Fragment>
    )
  }

  if (type === 'BAR') {
    return (
      <BarChart
        keys={keys}
        plot={plot}
        months={months}
        scale={scale}
        kpiType={kpiType}
        title={chartTitle}
        chartTitle={<Title title={chartTitle} variant="h6" />}
        fullScreenApp={fullScreen}
        metric={metric}
        group={group}
      />
    )
  }

  if (type === 'LINE') {
    return (
      <LineChart
        keys={keys}
        plot={plot}
        months={months}
        scale={scale}
        kpiType={kpiType}
        title={chartTitle}
        chartTitle={<Title title={chartTitle} variant="h6" />}
        fullScreenApp={fullScreen}
        metric={metric}
        group={group}
      />
    )
  }

  if (type === 'BUBBLE' || type === 'SCATTER') {
    return (
      <ScatterReChart
        keys={keys}
        plot={plot}
        months={months}
        scale={scale}
        kpiType={kpiType}
        title={chartTitle}
        chartTitle={<Title title={chartTitle} variant="h6" />}
        fullScreenApp={fullScreen}
        metric={metric}
        group={group}
        bubble={type === 'BUBBLE'}
      />
    )
  }

  if (type === 'HEAT_MAP') {
    return (
      <HeatMap
        xLabels={heatX}
        yLabels={heatY}
        data={data03}
        background="#ff7300"
        chartTitle={<Title title={chartTitle} variant="h6" />}
      />
    )
  }

  if (type === 'KPI') {
    return (
      <Fragment>
        {plot.map(
          ({ chartTitle: kpiTitle = '', value: kpiValue = 0, func = '' }) => (
            <Card className={classes.gridKpiBox} key={kpiValue}>
              <CardContent>
                <Typography variant="h4" style={{ textAlign: 'center' }}>
                  {kpiTitle}
                </Typography>
                <Typography
                  variant="h2"
                  color="secondary"
                  style={{ textAlign: 'center' }}>
                  {humanize.numberFormat(kpiValue)}
                </Typography>
                <CustomLegend
                  metric={metric}
                  group={group}
                  func={func}
                  themeType={theme.palette.type}
                />
              </CardContent>
            </Card>
          )
        )}
      </Fragment>
    )
  }

  if (type === 'DATA_MAP') {
    return (
      <DataMap
        chartTitle={
          <Title
            title={strings('charts.title-simple-global-map')}
            variant="h6"
          />
        }
        fullScreenApp={fullScreen}
      />
    )
  }

  if (type === 'WORLD_MAP') {
    return <div className={classes.gridBox} id="basic" />
  }

  return null
}

export default withStyles(styles, { withTheme: true })(ChartRenderer)
