import React from 'react'
import { connect } from 'react-redux'
import { FormControl } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Select from 'react-select'

import styles from 'appStyles'
import { strings } from '../../../../locales/i18n'

const DashboardSelect = ({
  classes,
  theme,
  dashboard,
  compare,
  handleChange
}) => {
  const { updatingDashboard, dashboards, selected, compareSelected } = dashboard
  const selectedItem = compare ? compareSelected : selected
  return (
    <FormControl className={classes.searchBarSpacing} fullWidth>
      <Select
        placeholder={strings('inputs.placeholder-select-dashboard')}
        onChange={handleChange}
        options={dashboards}
        value={updatingDashboard ? null : selectedItem}
        styles={{
          control: itemStyles => ({ ...itemStyles, backgroundColor: 'white' }),
          singleValue: (itemStyles, { data }) => {
            const { type } = data
            return {
              ...itemStyles,
              color: type === 'AUTO' ? theme.palette.secondary.main : '#000000'
            }
          },
          option: (itemStyles, { data }) => {
            const { type } = data
            return {
              ...itemStyles,
              color: type === 'AUTO' ? theme.palette.secondary.main : '#000000'
            }
          }
        }}
      />
    </FormControl>
  )
}

const mapStateToProps = ({ dashboard }) => ({ dashboard })
const mapDispatchToProps = {}

const ConnectedSelect = connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardSelect)

export default withStyles(styles, { withTheme: true })(ConnectedSelect)
