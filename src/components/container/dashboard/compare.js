import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import SwipeableViews from 'react-swipeable-views'
import moment from 'moment'
import {
  Grow,
  Fade,
  Typography,
  CircularProgress,
  Tab,
  Tabs
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'

import ServiceLoader from 'presentational/service-loader'
import DataMap from 'presentational/charts/map'
import Title from 'presentational/dashboards/title'
import DateRange from 'presentational/picker'
import ButtonComponent from 'presentational/dashboards/button-component'
import * as dashboardActions from 'reducers/reducer-dashboard'
import { getDashboardQueries } from 'services/dashboard'
import { processWidgets } from './utils'
import { strings } from '../../../../locales/i18n'
import ChartRenderer from './chart-renderer'
import DashboardSelect from './select-dashboard'
import styles from '../../../styles'

const growDelay = 500

const TabContainer = ({ children, dir }) => (
  <Typography style={{ paddingTop: 10 }} component="div" dir={dir}>
    {children}
  </Typography>
)

class Bots extends PureComponent {
  constructor(props) {
    super(props)

    this.timer = growDelay

    this.state = {
      tabValue: 0
    }
  }

  componentDidUpdate(prevProps) {
    const { dashboard: prevDashboard } = prevProps
    const { dashboard, enqueueSnackbar } = this.props
    const {
      compareSelected: prevSelected,
      comparedDashboard: prevCompared
    } = prevDashboard
    const { comparedDashboard, compareSelected, selected } = dashboard

    if (
      comparedDashboard.id &&
      (compareSelected.value !== prevSelected.value ||
        comparedDashboard.dateFrom !== prevCompared.dateFrom ||
        comparedDashboard.dateTo !== prevCompared.dateTo)
    ) {
      this._getDashboardWidgets(comparedDashboard)
    }

    if (
      prevSelected.value !== compareSelected.value &&
      compareSelected.value === selected.value
    ) {
      enqueueSnackbar('Scroll Synced.', {
        variant: 'info'
      })
    }
  }

  _getDashboardWidgets = dashboard => {
    const {
      setCompareToDashboard,
      updateDashboardStart,
      updateDashboardFinish
    } = this.props

    processWidgets({
      dashboard,
      setDashboardData: setCompareToDashboard,
      updateStart: updateDashboardStart,
      updateFinish: updateDashboardFinish
    })
  }

  handleChange = (args = {}) => {
    const {
      requestCompareDashboard,
      receiveCompareDashboard,
      setComparedDashboard,
      dashboard
    } = this.props
    const { compareSelected } = dashboard
    const { value } = args
    const { value: compareValue } = compareSelected

    if (compareValue === value) return

    requestCompareDashboard()

    this.timer = growDelay

    getDashboardQueries(value).then(response => {
      const { status } = response
      if (status === 200) {
        receiveCompareDashboard()

        response.json().then((dashboardData = {}) => {
          const { startDate, endDate } = dashboardData

          setComparedDashboard(dashboardData, args, {
            dateFrom: moment(startDate).format('YYYY-MM-DDTHH:MM'),
            dateTo: moment(endDate).format('YYYY-MM-DDTHH:MM')
          })
        })
      } else {
        receiveCompareDashboard()
      }
    })
  }

  _tabChange = (event, value) => {
    this.setState({
      tabValue: value
    })
  }

  render() {
    const { tabValue } = this.state
    const { classes, theme, bots, dashboard, setDateRangeCompare } = this.props
    const {
      updatingDashboard,
      requestingCompare,
      preparingCompare,
      selected,
      compareSelected = {},
      selectedDashboard,
      compareToDashboard = {},
      fullScreen,
      compare
    } = dashboard
    const { fromDateCompare, toDateCompare } = bots
    const { value = '', label } = compareSelected
    const { value: activeValue = '' } = selected
    const { visuals: widgets = [] } = compareToDashboard
    const { direction } = theme

    const showDashboardMessage =
      (compare && preparingCompare) || requestingCompare

    return (
      <div
        className={classNames(
          fullScreen && classes.dashboardContainerFullScreen,
          !fullScreen && !compare && classes.dashboardContainer,
          fullScreen && compare && classes.dashboardFullScreenCompare
        )}>
        {updatingDashboard && <ServiceLoader />}
        <div className={classes.searchBar}>
          <DashboardSelect handleChange={this.handleChange} compare />
        </div>

        <div className={classes.tabContainer}>
          <Tabs
            value={tabValue}
            onChange={this._tabChange}
            indicatorColor="secondary"
            textColor="primary">
            <Tab
              disabled={compareSelected === ''}
              label={strings('tabs.label-dashboard')}
            />
            <Tab
              disabled={compareSelected === ''}
              label={strings('tabs.label-profile')}
            />
          </Tabs>
          {compareSelected !== '' && (
            <Fade in>
              <DateRange
                setFn={setDateRangeCompare}
                fromDate={fromDateCompare}
                toDate={toDateCompare}
                disabled={value === '' || showDashboardMessage}
                compare={compare}
              />
            </Fade>
          )}
        </div>

        <SwipeableViews
          axis={direction === 'rtl' ? 'x-reverse' : 'x'}
          index={tabValue}
          onChangeIndex={this.handleChangeIndex}>
          <TabContainer dir={direction}>
            {showDashboardMessage && (
              <Grow in={showDashboardMessage} timeout={500}>
                <div
                  className={classNames(classes.gridEmpty, classes.gridBase)}>
                  <CircularProgress color="secondary" />
                  <Typography variant="h6">
                    {preparingCompare
                      ? strings('messages.preparing-dashboards-message')
                      : strings('messages.requesting-dashboard-data-message')}
                  </Typography>
                </div>
              </Grow>
            )}
            {compareSelected !== '' && !requestingCompare && !preparingCompare && (
              <div
                className="widget-container"
                id="compare-dashboard"
                onScroll={() => {
                  if (value === activeValue) {
                    const active = document.getElementById('active-dashboard')
                    const compared = document.getElementById(
                      'compare-dashboard'
                    )
                    active.scrollTop = compared.scrollTop
                  }
                }}>
                {widgets.length > 0 ? (
                  <div style={{ marginTop: 0 }} className={classes.dashboard}>
                    {widgets.map((mapProps, idx) => {
                      const { chartTitle } = mapProps
                      if (idx === 0) this.timer = growDelay

                      this.timer = this.timer + 100

                      return (
                        <div key={chartTitle} style={{ position: 'relative' }}>
                          <ChartRenderer
                            {...mapProps}
                            fullScreen={fullScreen}
                          />
                        </div>
                      )
                    })}
                    <DataMap
                      chartTitle={
                        <Title
                          title={strings('charts.title-simple-global-map')}
                          variant="h6"
                        />
                      }
                      fullScreenApp={fullScreen}
                    />
                  </div>
                ) : (
                  <Grow in={widgets.length === 0} timeout={1000}>
                    <div
                      className={classNames(
                        classes.gridEmpty,
                        classes.gridBase
                      )}>
                      <Typography variant="h6">{`No data found for ${label}`}</Typography>
                      <ButtonComponent
                        label={strings('buttons.label-retry')}
                        handler={() =>
                          this._getDashboardWidgets(selectedDashboard)
                        }
                        icon="refresh"
                      />
                    </div>
                  </Grow>
                )}
              </div>
            )}
            {compareSelected === '' && !requestingCompare && !preparingCompare && (
              <Grow in timeout={1000}>
                <div
                  className={classNames(classes.gridEmpty, classes.gridBase)}>
                  <Typography variant="h6">
                    {strings('messages.select-dashboard-to-compare')}
                  </Typography>
                </div>
              </Grow>
            )}
          </TabContainer>

          <TabContainer dir={direction}>
            <Grow in timeout={1000}>
              <div className={classNames(classes.gridEmpty, classes.gridBase)}>
                <Typography variant="h6">
                  {strings('dashboard.dashboard-coming-soon')}
                </Typography>
              </div>
            </Grow>
          </TabContainer>
        </SwipeableViews>
      </div>
    )
  }
}

const mapStateToProps = ({ bots, dashboard }) => ({
  bots,
  dashboard
})

const mapDispatchToProps = {
  setDateRangeCompare: dashboardActions.setDateRangeCompare,
  setCompareDashboard: dashboardActions.setCompareDashboard,
  setComparedDashboard: dashboardActions.setComparedDashboard,
  requestCompareDashboard: dashboardActions.requestCompareDashboard,
  receiveCompareDashboard: dashboardActions.receiveCompareDashboard,
  setCompareToDashboard: dashboardActions.setCompareToDashboard,
  updateDashboardStart: dashboardActions.updateDashboardStart,
  updateDashboardFinish: dashboardActions.updateDashboardFinish
}

const ConnectedBots = connect(
  mapStateToProps,
  mapDispatchToProps
)(Bots)
export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedBots)
)
