import React, { PureComponent } from 'react'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'
import classNames from 'classnames'
import {
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  CircularProgress
} from '@material-ui/core'

import * as previewDataActions from 'reducers/reducer-dataprofile'
import styles from 'appStyles'
import { getPreviewData } from 'services/data-profile'
import ButtonComponent from 'presentational/dashboards/button-component'

const previewDataStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.type !== 'dark' && '#e8e8e8'
    },
    '&:nth-of-type(even)': {
      backgroundColor: theme.palette.type !== 'dark' && '#d8d8d8'
    }
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 10
  }
})

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#666666',
    color: theme.palette.common.white,
    borderRight: '1px solid rgba(224, 224, 224, 1)',
    borderLeft: '1px solid rgba(224, 224, 224, 1)',
    textAlign: 'center',
    padding: '5px 10px'
  },
  body: {
    borderRight: '1px solid rgba(224, 224, 224, 1)',
    borderLeft: '1px solid rgba(224, 224, 224, 1)',
    padding: '5px 10px'
  }
}))(TableCell)

class RawData extends PureComponent {
  state = {
    loadingPreviewData: true
  }

  componentDidMount() {
    this._getPreviewData()
  }

  _getPreviewData = () => {
    const { setPreviewData, dashboardId } = this.props
    getPreviewData(dashboardId)
      .then(response => {
        const { status } = response
        if (status === 200) {
          response.json().then(data => {
            const { data: previewData } = data
            setPreviewData(previewData)
            this.setState({ loadingPreviewData: false })
          })
        } else {
          this.setState({ loadingPreviewData: false })
        }
      })
      .catch(() => {
        this.setState({ loadingPreviewData: false })
      })
  }

  render() {
    const { classes, dataProfile, dashboardTitle } = this.props
    const { previewData } = dataProfile
    const { loadingPreviewData } = this.state

    if (loadingPreviewData) {
      return (
        <div className={classNames(classes.loader)}>
          <CircularProgress color="secondary" />
          <Typography variant="h6">Preparing preview data table...</Typography>
        </div>
      )
    }

    return (
      <div
        style={{
          margin: '90px 25px'
        }}>
        <div className={classes.titleContainer}>
          <Typography variant="h6">{`Task Name: ${dashboardTitle}`}</Typography>
        </div>
        {previewData.length > 0 ? (
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                {previewData.length > 0 &&
                  previewData[0].variables.map(row => (
                    <CustomTableCell>{row.name}</CustomTableCell>
                  ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {previewData.map(bodyRow => (
                <TableRow className={classes.row}>
                  {bodyRow.variables.map(row => (
                    <CustomTableCell>{row.value}</CustomTableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        ) : (
          <div className={classNames(classes.gridEmpty, classes.gridBase)}>
            <Typography variant="h6">No data found</Typography>
            <ButtonComponent
              label="Retry"
              handler={() => this._getPreviewData()}
              icon="refresh"
            />
          </div>
        )}
      </div>
    )
  }
}

const mapDispatchToProps = {
  setPreviewData: previewDataActions.setPreviewData
}

const mapStateToProps = ({ dataProfile, dashboard }) => ({
  dataProfile,
  dashboard
})

const ConnectedRawData = connect(
  mapStateToProps,
  mapDispatchToProps
)(RawData)

export default withStyles(
  theme => ({ ...styles(theme), ...previewDataStyles(theme) }),
  { withTheme: true }
)(withSnackbar(ConnectedRawData))
