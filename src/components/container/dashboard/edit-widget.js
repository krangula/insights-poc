import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import { IconButton } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import Icon from 'presentational/icon'
import styles from 'appStyles'
import ConstructWidget from './library/construct-widget'

class EditWidget extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }

  _handleClose = () => {
    this.setState({
      open: false
    })
  }

  _handleOpen = () => {
    this.setState({
      open: true
    })
  }

  render() {
    const { open } = this.state
    const { details } = this.props
    const { formValues, id, tempId } = details
    return (
      <Fragment>
        <IconButton color="secondary" onClick={this._handleOpen}>
          <Icon name="edit" />
        </IconButton>
        <ConstructWidget
          edit
          widgetId={id}
          tempId={tempId}
          open={open}
          chart={details}
          initialValues={formValues}
          handleClose={this._handleClose}
        />
      </Fragment>
    )
  }
}

const mapStateToProps = ({ dashboard }) => ({ dashboard })
const mapDispatchToProps = {}

const ConnectedEdit = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditWidget)

export default withStyles(styles, { withTheme: true })(ConnectedEdit)
