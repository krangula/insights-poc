import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  Zoom,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Stepper,
  Step,
  StepLabel,
  Typography
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'
import * as dashboardActions from 'reducers/reducer-dashboard'
import { getDashboardData, getWidgetQuery } from 'services/dashboard'
import ChartForm from './chart-form'
import GaugeChartForm from './chart-form-gauge'

import { strings } from '../../../../../locales/i18n'
import styles from './styles'

// const queries = require('../../../../queries.json')

const getUniqueTempId = tempIds => {
  let tempId
  let isUnique = false

  while (!isUnique) {
    tempId = Math.floor(100000000 + Math.random() * 900000000)
    isUnique = !tempIds.includes(tempId)
  }

  return tempId
}

class AddChartConfirmation extends Component {
  constructor(props) {
    super(props)
    const { edit = false } = props
    this.state = {
      title: '',
      formInvalid: true,
      activeStep: 0,
      query: {},
      newQuery: {},
      metric: {},
      group: {},
      formValues: {},
      steps: edit
        ? [
            strings('charts.construct_your_query'),
            strings('charts.confirm_chart')
          ]
        : [
            strings('charts.selected_chart'),
            strings('charts.construct_your_query'),
            strings('charts.confirm_chart')
          ],
      tempIds: []
    }
  }

  shouldComponentUpdate(nextProps) {
    const { open: next } = nextProps
    const { open: prev } = this.state
    return next || next !== prev
  }

  componentDidUpdate(prevProps) {
    const me = this
    const { open: prevOpen } = prevProps
    const { chart = {}, open, initialValues = {}, dashboard } = this.props
    const { selectedDashboard } = dashboard
    const { taskId } = selectedDashboard
    const { type } = chart

    if (!prevOpen && open) {
      getWidgetQuery(type.toLowerCase()).then(response =>
        response.json().then(data => {
          me.setState({
            query: data || {}
          })
        })
      )

      // construct the metricField and groupField
      const { metric, group } = initialValues
      if (metric || group) {
        me.setState({
          metricField: `data.${taskId}.var.${metric}`,
          groupField: `data.${taskId}.var.${group}.keyword`
        })
      }
    }
  }

  _showForm = () => {
    this.setState({
      chartForm: true
    })
  }

  _handleBack = () => {
    const { activeStep } = this.state
    this.setState({
      activeStep: activeStep - 1
    })
  }

  _handleNext = () => {
    const { activeStep, steps } = this.state
    const nextStep = activeStep + 1
    this.setState(
      {
        activeStep: nextStep
      },
      () => {
        if (nextStep === steps.length - 1) this._contructQuery()
      }
    )
  }

  _handleClose = () => {
    const { handleClose } = this.props
    this.setState(
      {
        query: {},
        newQuery: {},
        formValues: {},
        activeStep: 0,
        title: '',
        tempIds: []
      },
      () => handleClose()
    )
  }

  _updateQuery = async value => {
    const { chart } = this.props
    const { query, metricField, groupField } = this.state
    const { type } = chart
    const newQuery = {
      ...query
    }
    const { aggs } = newQuery
    const { visualization } = aggs || {}

    if (visualization) {
      if (type === 'KPI' || type === 'GAUGE') {
        // const { chart: chartForm, gaugeChart } = form
        // const { values } = type === 'KPI' ? chartForm : gaugeChart
        const kpiKeys = Object.keys(aggs)
        const kpiKey = kpiKeys[0]
        // const kpiField = aggs[kpiKey][Object.keys(aggs[kpiKey])[0]]
        delete aggs[kpiKey]

        aggs[kpiKey] = {
          [value]: {
            field: metricField
          }
        }

        newQuery.aggs = aggs
      } else {
        const { aggs: innerAggs, terms } = visualization
        const keys = Object.keys(innerAggs)
        if (keys.length > 0) {
          for (let i = 0; i < keys.length; i++) {
            const key = keys[i]
            // const innerKeys = Object.keys(innerAggs[key])
            // const aggField = innerAggs[key][innerKeys[0]]

            delete innerAggs[key]

            innerAggs[key] = {
              [value]: {
                field: metricField
              }
            }
          }

          newQuery.aggs.visualization = {
            ...visualization,
            aggs: innerAggs,
            terms: {
              ...terms,
              field: groupField
            }
          }
        }
      }
    }

    return newQuery
  }

  _contructQuery = () => {
    const { form, chart, initialValues } = this.props
    const { type } = chart
    const { chart: baseChart, gaugeChart } = form
    const _chartForm = type === 'GAUGE' ? gaugeChart : baseChart
    const { values } = _chartForm
    const { func, title, metric } = values
    const queryMetric = func === '' ? metric : func

    if (JSON.stringify(values) === JSON.stringify(initialValues)) return

    this._updateQuery(queryMetric).then(newQuery => {
      this.setState({
        title,
        newQuery,
        formValues: values
      })
    })
  }

  handleForm = formInvalid => {
    this.setState({
      formInvalid
    })
  }

  _handleAddWidget = () => {
    const { query: _query, title, formValues, tempIds } = this.state
    const {
      setKibanaDashboard,
      chart = {},
      bots,
      edit,
      widgetId,
      tempId,
      enqueueSnackbar
    } = this.props
    const { type } = chart

    const { query: innerQuery } = _query
    const { bool } = innerQuery
    const { must } = bool

    const { fromDate, toDate } = bots

    for (let d = 0; d < must.length; d++) {
      const idx = must.findIndex(item => item.range)
      const { range } = must[idx]
      let { dateLogged } = range
      dateLogged = {
        ...dateLogged,
        gte: new Date(fromDate).valueOf(),
        lte: new Date(toDate).valueOf()
      }

      _query.query.bool.must[idx].range.dateLogged = dateLogged
    }

    getDashboardData(_query).then(response => {
      const { status } = response
      if (status === 200) {
        response.json().then(result => {
          let newTempId

          if (!widgetId && !tempId) {
            newTempId = getUniqueTempId(tempIds)
            tempIds.push(newTempId)
            this.setState({
              tempIds
            })
          }

          setKibanaDashboard({
            type,
            title,
            formValues,
            query: _query,
            dashboard: result,
            id: widgetId,
            tempId: !widgetId && (tempId || newTempId),
            edit
          })

          if (edit) {
            enqueueSnackbar(strings('widget.widget_updated'), {
              variant: 'success'
            })
          }

          this._handleClose()
        })
      }
    })
  }

  handleGroupSelect = (val, args = {}) => {
    const { dashboard } = this.props
    const { selectedDashboard } = dashboard
    const { taskId } = selectedDashboard
    const { label } = args[val] || {}

    this.setState({
      groupField: `data.${taskId}.var.${label}.keyword`
    })
  }

  handleMetricSelect = (val, args) => {
    const { dashboard } = this.props
    const { selectedDashboard } = dashboard
    const { taskId } = selectedDashboard
    const { label } = args[val] || {}

    this.setState({
      metricField: `data.${taskId}.var.${label}`
    })
  }

  _switchWidget = () => {
    const { toggleWigetSwitch } = this.props
    toggleWigetSwitch(true)

    this._handleClose()
  }

  render() {
    const { activeStep, newQuery, formInvalid, steps, formValues } = this.state
    const {
      open,
      edit,
      chart = {},
      classes,
      dashboard,
      initialValues
    } = this.props
    const { tasks, mappings, metrics } = dashboard
    const { type } = chart
    let content

    if (!open) return null

    const chartForm = (
      <ChartForm
        type={type}
        handleForm={this.handleForm}
        tasks={tasks}
        mappings={mappings}
        metrics={metrics}
        confirm={!edit && activeStep === 2}
        initialValues={edit ? initialValues : formValues}
        handleMetricSelect={this.handleMetricSelect}
        handleGroupSelect={this.handleGroupSelect}
      />
    )

    const gaugeForm = (
      <GaugeChartForm
        type={type}
        handleForm={this.handleForm}
        tasks={tasks}
        mappings={mappings}
        metrics={metrics}
        confirm={!edit && activeStep === 2}
        initialValues={edit ? initialValues : formValues}
        handleMetricSelect={this.handleMetricSelect}
      />
    )

    const form = type === 'GAUGE' ? gaugeForm : chartForm

    const hasQuery = Object.keys(newQuery).length > 0
    const queryBody = (
      <div>
        {hasQuery ? (
          <Zoom in>
            <Typography variant="h4" style={{ textAlign: 'center' }}>
              {strings('widget.ready_to_build')}
            </Typography>
          </Zoom>
        ) : (
          <Typography variant="h4" style={{ textAlign: 'center' }}>
            {strings('widget.default-content')}
          </Typography>
        )}
      </div>
    )

    switch (activeStep) {
      case 0:
        content = edit ? form : chart.component
        break
      case 1:
        content = edit ? queryBody : form
        break
      case 2:
        content = queryBody
        break
      default:
        content = <p>{strings('widget.default-content')}</p>
        break
    }

    return (
      <Dialog
        open={open}
        fullWidth
        maxWidth="sm"
        keepMounted
        scroll="body"
        disableBackdropClick
        onClose={this._handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        classes={{ paper: classes.dialogAllowOverflow }}>
        <DialogTitle id="alert-dialog-title">
          {edit ? strings('charts.edit_chart') : `Add ${chart.title}`}
        </DialogTitle>
        <DialogContent classes={{ root: classes.dialogAllowOverflow }}>
          <Stepper
            activeStep={activeStep}
            alternativeLabel
            classes={{ root: classes.stepperDialog }}>
            {steps.map(label => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          {content}
        </DialogContent>
        <DialogActions>
          {!edit && (
            <Button
              variant="contained"
              color="secondary"
              onClick={this._switchWidget}>
              {strings('buttons.label-switch-widget')}
            </Button>
          )}
          <Button
            onClick={this._handleClose}
            variant="contained"
            color="secondary">
            {strings('buttons.label-cancel')}
          </Button>
          {activeStep !== 0 && (
            <Button
              onClick={this._handleBack}
              variant="contained"
              color="secondary">
              {strings('buttons.label-back')}
            </Button>
          )}

          {(edit && activeStep === 1) || activeStep === 2 ? (
            <Button
              onClick={this._handleAddWidget}
              disabled={!hasQuery}
              variant="contained"
              color="secondary"
              autoFocus>
              {strings('buttons.label-finish')}
            </Button>
          ) : (
            <Button
              disabled={
                edit
                  ? activeStep === 0 && formInvalid
                  : activeStep === 1 && formInvalid
              }
              onClick={this._handleNext}
              variant="contained"
              color="secondary"
              autoFocus>
              {strings('buttons.label-next')}
            </Button>
          )}
        </DialogActions>
      </Dialog>
    )
  }
}

const mapStateToProps = ({ bots, dashboard, form }) => ({
  bots,
  form,
  dashboard
})

const mapDispatchToProps = {
  setDashboardTasks: dashboardActions.setDashboardTasks,
  setKibanaDashboard: dashboardActions.setKibanaDashboard,
  toggleWigetSwitch: dashboardActions.toggleWigetSwitch
}

const ConnectedWidgets = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddChartConfirmation)

export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedWidgets)
)

// <pre>{JSON.stringify(newQuery, null, 4)}</pre>
