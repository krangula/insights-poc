import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, formValueSelector } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import FormControl from '@material-ui/core/FormControl'
import { TextField } from 'redux-form-material-ui'
import IntegrationReactSelect from 'presentational/select'

import { strings } from '../../../../../locales/i18n'
import styles from './styles'

const options = [
  { value: 'min', label: 'Min' },
  { value: 'max', label: 'Max' },
  { value: 'avg', label: 'Avg' },
  { value: 'sum', label: 'Sum' }
]

// validation functions
const required = value =>
  value == null || value === '' ? strings('forms.label-required') : undefined

class ChartForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      metricType: ''
    }
  }

  componentDidUpdate() {
    const { handleForm, invalid } = this.props

    if (handleForm) {
      handleForm(invalid)
    }
  }

  _handleMetric = (val, args) => {
    const { handleMetricSelect } = this.props
    const { type } = args[val] || {}
    this.setState(
      {
        metricType: type
      },
      () => handleMetricSelect(val, args)
    )
  }

  // saveRef = ref => (this.ref = ref)

  render() {
    const { metricType } = this.state
    const {
      type,
      handleSubmit,
      classes,
      mappings = [],
      metric = '',
      metrics,
      handleGroupSelect
    } = this.props

    const item = metrics.find(({ label }) => label === metric) || {}
    const _metric = metricType || item.type

    return (
      <form onSubmit={handleSubmit}>
        <FormControl fullWidth classes={{ root: classes.marginVertical }}>
          <Field
            name="title"
            component={TextField}
            label={strings('charts.title_chart')}
            validate={required}
            autoComplete="off"
            withRef
          />
        </FormControl>
        <FormControl fullWidth classes={{ root: classes.marginVertical }}>
          <Field
            name="metric"
            component={IntegrationReactSelect}
            placeholder={strings('charts.chart_metric')}
            openOnFocus
            dataSource={metrics}
            disabled={metrics.length === 0}
            validate={required}
            handleSelect={this._handleMetric}
            reduxForm
          />
        </FormControl>
        {['long', 'integer', 'double', 'float'].includes(_metric) && (
          <FormControl fullWidth classes={{ root: classes.marginVertical }}>
            <Field
              name="func"
              component={IntegrationReactSelect}
              placeholder={strings('charts.chart_aggregate')}
              openOnFocus
              dataSource={options}
              validate={required}
              reduxForm
            />
          </FormControl>
        )}
        {type !== 'KPI' && type !== 'GAUGE' && (
          <FormControl fullWidth classes={{ root: classes.marginVertical }}>
            <Field
              name="group"
              component={IntegrationReactSelect}
              placeholder={strings('charts.chart_group')}
              openOnFocus
              dataSource={mappings}
              disabled={mappings.length === 0}
              handleSelect={handleGroupSelect}
              validate={required}
              reduxForm
            />
          </FormControl>
        )}
      </form>
    )
  }
}

// Decorate with redux-form
let ChartConnected = reduxForm({
  form: 'chart',
  initialValues: {
    title: '',
    func: '',
    metric: ''
  }
})(ChartForm)

const selector = formValueSelector('chart')

ChartConnected = connect(state => {
  const metric = selector(state, 'metric')

  return {
    metric
  }
})(ChartConnected)

export default withStyles(styles, { withTheme: true })(ChartConnected)
