import { createStyles } from '@material-ui/core/styles'

const styles = theme =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      margin: '0 auto'
    },
    rootFullScreenWidth: {
      width: '95vw'
    },
    rootWidth: {
      width: '95vw'
    },
    marginVertical: {
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit
    },
    image: {
      backgroundColor: '#fff',
      position: 'relative',
      height: 200,
      [theme.breakpoints.down('xs')]: {
        width: '100% !important', // Overrides inline-style
        height: 100
      },
      '&:hover, &$focusVisible': {
        zIndex: 1,
        '& $imageBackdrop': {
          opacity: 0.15
        },
        '& $imageMarked': {
          opacity: 0
        },
        '& $imageTitle': {
          border: '4px solid currentColor'
        }
      }
    },
    iconSpacing: {
      marginLeft: theme.spacing.unit
    },
    focusVisible: {},
    imageButton: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: '1.1rem',
      color: theme.palette.common.black
    },
    imageSrc: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundSize: 'cover',
      backgroundPosition: 'center 40%'
    },
    imageBackdrop: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: theme.palette.common.black,
      opacity: theme.palette.type === 'dark' ? 0.1 : 0.4,
      transition: theme.transitions.create('opacity')
    },
    imageTitle: {
      position: 'relative',
      padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme
        .spacing.unit + 6}px`
    },
    imageMarked: {
      height: 3,
      width: 18,
      backgroundColor: theme.palette.common.white,
      position: 'absolute',
      bottom: -2,
      left: 'calc(50% - 9px)',
      transition: theme.transitions.create('opacity')
    },
    dialogAllowOverflow: {
      overflow: 'visible'
    },
    readyIcon: {
      fontSize: 80,
      marginBottom: 15,
      color: 'green'
    },
    flexColumnCenter: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center'
    },
    fabIcon: {
      right: 20,
      bottom: 20,
      zIndex: 50,
      position: 'fixed'
    },
    noPadding: {
      padding: 0
    }
  })

export default styles
