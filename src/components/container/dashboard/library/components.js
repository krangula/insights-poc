import React from 'react'
import { Treemap } from 'recharts'
import { Typography } from '@material-ui/core'

import HeatMap from 'react-heatmap-grid'
import ReactSpeedometer from 'react-d3-speedometer'
import BarChart from 'presentational/charts/bar'
import PieChart from 'presentational/charts/pie'
import Scatter from 'presentational/charts/scatter'
import LineChart from 'presentational/charts/line'
import WorldMap from 'presentational/charts/map'
import { strings } from '../../../../../locales/i18n'

const mock = require('../../../../mock/library.json')

// mock heat map data
const xLabels = new Array(24).fill(0).map((_, i) => `${i}`)
const yLabels = ['Sun', 'Mon', 'Tue']
const data = new Array(yLabels.length)
  .fill(0)
  .map(() =>
    new Array(xLabels.length).fill(0).map(() => Math.floor(Math.random() * 100))
  )

const kpi = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: 300,
  height: 200
}

const dimensions = {
  width: 300,
  height: 200
}

const components = [
  {
    type: 'GAUGE',
    url: '/static/images/grid-list/breakfast.jpg',
    title: strings('charts.title-gauge-chart'),
    component: (
      <div
        style={{
          height: 300
        }}>
        <ReactSpeedometer
          fluidWidth
          value={333}
          segments={3}
          needleTransitionDuration={4000}
          needleTransition="easeElastic"
          currentValueText="Current Value: #{value}"
          currentValuePlaceholderStyle="#{value}"
        />
      </div>
    ),
    preview: (
      <ReactSpeedometer
        value={333}
        segments={3}
        needleTransitionDuration={4000}
        needleTransition="easeElastic"
        currentValueText="Current Value: #{value}"
        currentValuePlaceholderStyle="#{value}"
      />
    )
  },
  {
    type: 'KPI',
    url: '/static/images/grid-list/breakfast.jpg',
    title: strings('charts.title-kpi'),
    component: (
      <div
        style={{
          ...kpi,
          width: '100%'
        }}>
        <Typography color="secondary" variant="h2">
          {strings('charts.title-add-kpi')}
        </Typography>
      </div>
    ),
    preview: (
      <div style={kpi}>
        <Typography color="secondary" variant="h2">
          {strings('charts.title-dollar')}
        </Typography>
      </div>
    )
  },
  {
    type: 'BAR',
    url: '/static/images/grid-list/breakfast.jpg',
    title: strings('charts.title-bar-chart'),
    preview: (
      <BarChart demo animate={false} plot={mock.bar} dimensions={dimensions} />
    ),
    component: <BarChart demo plot={mock.bar} dimensions={dimensions} />
  },
  {
    type: 'PIE',
    url: '/static/images/grid-list/burgers.jpg',
    title: strings('charts.title-pie-chart'),
    preview: (
      <PieChart
        demo
        animate={false}
        plot={mock.bar}
        dimensions={dimensions}
        position={{ cx: 150, cy: 100 }}
      />
    ),
    component: (
      <PieChart demo animate={false} plot={mock.bar} dimensions={dimensions} />
    )
  },
  {
    type: 'LINE',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-line-chart'),
    preview: <LineChart demo plot={mock.bar} dimensions={dimensions} />,
    component: <LineChart demo plot={mock.bar} dimensions={dimensions} />
  },
  {
    type: 'SCATTER',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-scatter-chart'),
    preview: <Scatter demo plot={mock.bar} dimensions={dimensions} />,
    component: <Scatter demo plot={mock.bar} dimensions={dimensions} />
  },
  {
    type: 'BUBBLE',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-bubble-chart'),
    preview: <Scatter demo bubble plot={mock.bar} dimensions={dimensions} />,
    component: <Scatter demo bubble plot={mock.bar} dimensions={dimensions} />
  },
  {
    type: 'PIVOT',
    title: strings('charts.title-pivot-chart'),
    component: (
      <div
        className="pivot-preview"
        style={{
          height: 300
        }}
      />
    ),
    preview: <div className="pivot-preview" style={kpi} />
  },
  {
    type: 'HEAT_MAP',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-heat-map'),
    preview: (
      <div
        style={{
          width: '300px',
          height: '200px',
          display: 'flex',
          overflow: 'hidden',
          alignItems: 'center'
        }}>
        <HeatMap
          xLabels={xLabels}
          yLabels={yLabels}
          data={data}
          background="#ff7300"
        />
      </div>
    ),
    component: (
      <HeatMap
        xLabels={xLabels}
        yLabels={yLabels}
        data={data}
        background="#ff7300"
      />
    )
  },
  {
    type: 'TREE_MAP',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-tree-map'),
    preview: (
      <Treemap
        {...dimensions}
        isAnimationActive={false}
        width={300}
        height={200}
        data={mock.tree}
        dataKey="size"
        ratio={4 / 3}
        stroke="#fff"
        fill="#ff7300"
      />
    ),
    component: (
      <Treemap
        {...dimensions}
        data={mock.tree}
        dataKey="size"
        ratio={4 / 3}
        stroke="#fff"
        fill="#ff7300"
      />
    )
  },
  {
    type: 'MAP',
    url: '/static/images/grid-list/camera.jpg',
    title: strings('charts.title-world-map'),
    preview: <WorldMap demo plot={mock.bar} dimensions={dimensions} />,
    component: <WorldMap demo plot={mock.bar} dimensions={{ width: 'auto' }} />
  }
]

export default components
