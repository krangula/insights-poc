import React, { Component } from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import {
  Zoom,
  ButtonBase,
  Typography,
  Grow,
  Fab,
  Dialog,
  DialogTitle,
  DialogContent
} from '@material-ui/core'
import Icon from 'presentational/icon'
import * as dashboardActions from 'reducers/reducer-dashboard'
import { getTaskMappings } from 'services/dashboard'
import { strings } from '../../../../../locales/i18n'

import components from './components'
import ConstructWidget from './construct-widget'
import styles from './styles'

let delay = 0

class LibraryBases extends Component {
  constructor(props) {
    super(props)

    this.state = {
      chartLibrary: [],
      showLibrary: false,
      showDetails: false,
      chartDetails: {}
    }
  }

  componentDidMount() {
    this.setState(
      {
        chartLibrary: components
      },
      () => this._getMappings()
    )
  }

  componentDidUpdate(prevProps) {
    const { dashboard } = this.props
    const { selectedDashboard } = dashboard
    const { dashboard: next } = prevProps
    const { selectedDashboard: nextDashboard } = next

    if (selectedDashboard.id !== nextDashboard.id) this._getMappings()
  }

  _toggleLibrary = () => {
    const { showLibrary } = this.state
    this.setState({
      showLibrary: !showLibrary
    })
  }

  _confirmChart = chart => {
    const { toggleWigetSwitch } = this.props
    this.setState(
      {
        showLibrary: false,
        showDetails: true,
        details: chart
      },
      () => toggleWigetSwitch()
    )
  }

  _cancelConfirm = () => {
    this.setState({
      showLibrary: false,
      showDetails: false,
      details: {}
    })
  }

  _cancelConfirmSwitch = () => {
    const { toggleWigetSwitch } = this.props
    this.setState(
      {
        showLibrary: false,
        showDetails: false,
        details: {}
      },
      () => toggleWigetSwitch()
    )
  }

  _getMappings() {
    const { dashboard, setTaskMappings } = this.props
    const { selectedDashboard } = dashboard
    const { taskId } = selectedDashboard

    if (taskId) {
      getTaskMappings(taskId).then(response => {
        const { status } = response
        if (status === 200) {
          response.json().then(mappings => {
            setTaskMappings(mappings)
          })
        }
      })
    }
  }

  render() {
    const { classes, disabled, show, widgetSwitch } = this.props
    const { chartLibrary, showLibrary, showDetails, details } = this.state

    return (
      <div>
        <Zoom in={show}>
          <Fab
            disabled={disabled}
            color="secondary"
            className={classes.fabIcon}
            aria-label={strings('buttons.label-add-visualizations')}
            onClick={this._toggleLibrary}>
            <Icon name="insert_chart" />
          </Fab>
        </Zoom>

        <ConstructWidget
          open={showDetails}
          chart={details}
          handleClose={this._cancelConfirm}
        />

        <Dialog
          open={showLibrary || widgetSwitch}
          onClose={this._cancelConfirmSwitch}
          hideBackdrop>
          <DialogTitle>
            {strings('buttons.label-add-visualizations')}
          </DialogTitle>
          <DialogContent classes={{ root: classes.noPadding }}>
            <div className={classNames(classes.root)} style={{ minWidth: 620 }}>
              {chartLibrary.map((chart, index) => {
                delay = index === 0 ? 0 : delay + 150
                return (
                  <Grow
                    in
                    style={{ transformOrigin: '0 0 0' }}
                    timeout={delay}
                    key={chart.title}>
                    <ButtonBase
                      onClick={() => this._confirmChart(chart)}
                      focusRipple
                      className={classes.image}
                      focusVisibleClassName={classes.focusVisible}
                      style={{
                        width: chart.width
                      }}>
                      {chart.preview}
                      <span className={classes.imageBackdrop} />
                      <span className={classes.imageButton}>
                        <Typography
                          component="span"
                          variant="subtitle1"
                          color="inherit"
                          className={classes.imageTitle}>
                          {chart.title}
                          <span className={classes.imageMarked} />
                        </Typography>
                      </span>
                    </ButtonBase>
                  </Grow>
                )
              })}
            </div>
          </DialogContent>
        </Dialog>
      </div>
    )
  }
}

const mapStateToProps = ({ dashboard }) => ({
  dashboard
})

const mapDispatchToProps = {
  setTaskMappings: dashboardActions.setTaskMappings,
  toggleWigetSwitch: dashboardActions.toggleWigetSwitch
}

const ConnectedLibrary = connect(
  mapStateToProps,
  mapDispatchToProps
)(LibraryBases)

export default withStyles(styles, { withTheme: true })(ConnectedLibrary)

// <Button
//   disabled={disabled}
//   variant={showLibrary ? 'contained' : 'outlined'}
//   color="secondary"
//   onClick={this._toggleLibrary}>
//   {showLibrary ? (
//     <Icon name="insert_chart" />
//   ) : (
//     <Icon name="insert_chart_outlined" />
//   )}
//   <Typography variant="button">
//     {showLibrary
//       ? strings('buttons.label-hide-visualizations')
//       : strings('buttons.label-add-visualizations')}
//   </Typography>
// </Button>
