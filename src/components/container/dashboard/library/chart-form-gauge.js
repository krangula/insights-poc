import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Field, FieldArray, reduxForm, formValueSelector } from 'redux-form'
import { withStyles } from '@material-ui/core/styles'
import FormControl from '@material-ui/core/FormControl'
import { TextField } from 'redux-form-material-ui'
import IntegrationReactSelect from 'presentational/select'

import { strings } from '../../../../../locales/i18n'
import styles from './styles'

const options = [
  { value: 'min', label: 'Min' },
  { value: 'max', label: 'Max' },
  { value: 'avg', label: 'Avg' },
  { value: 'sum', label: 'Sum' }
]

const fields = [{}, {}, {}]

const rangeFields = () => (
  <Fragment>
    {fields.map((value, index) => (
      <div key="field">
        <FormControl>
          <Field
            label={strings('inputs.label-from')}
            name={`fromField${index}`}
            component={TextField}
            type="number"
            style={{ paddingRight: 100 }}
          />
        </FormControl>
        <FormControl>
          <Field
            label={strings('inputs.label-to')}
            type="number"
            name={`toField${index}`}
            component={TextField}
          />
        </FormControl>
      </div>
    ))}
  </Fragment>
)

const validate = values => {
  const errors = {}
  Object.keys(values).forEach(key => {
    if (key.startsWith('toField')) {
      const index = Number(key.split('toField')[1])
      const toValue = Number(values[key])

      const fromValue = values[`fromField${index}`]
      if (Number(fromValue) >= Number(toValue))
        errors[key] = strings('inputs.label_range_error')
    } else if (key.startsWith('fromField')) {
      const index = Number(key.split('fromField')[1])
      const fromValue = Number(values[key])
      if (index === 0) {
        if (Number.isNaN(fromValue)) {
          errors[key] = strings('inputs.label_invalid_input')
        }
      } else {
        const toFieldVal = values[`toField${index - 1}`]
        if (
          Number.isNaN(fromValue) ||
          Number(fromValue) !== Number(toFieldVal) + 1
        )
          errors[key] = strings('inputs.label_invalid_number')
      }
    }
  })
  return errors
}

// validation functions
const required = value =>
  value == null || value === '' ? strings('forms.label-required') : undefined

class GaugeChartForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      metricType: ''
    }
  }

  componentDidUpdate() {
    const { handleForm, invalid } = this.props

    if (handleForm) {
      handleForm(invalid)
    }
  }

  _handleMetric = (val, args) => {
    const { handleMetricSelect } = this.props
    const { type } = args[val] || {}
    this.setState(
      {
        metricType: type
      },
      () => handleMetricSelect(val, args)
    )
  }

  render() {
    const { metricType } = this.state
    const { handleSubmit, classes, metric, metrics } = this.props

    const item = metrics.find(({ label }) => label === metric) || {}
    const _metric = metricType || item.type

    return (
      <form onSubmit={handleSubmit}>
        <FormControl fullWidth classes={{ root: classes.marginVertical }}>
          <Field
            name="title"
            component={TextField}
            label={strings('charts.title_chart')}
            validate={required}
            autoComplete="off"
            withRef
          />
        </FormControl>
        <FormControl fullWidth classes={{ root: classes.marginVertical }}>
          <Field
            name="metric"
            component={IntegrationReactSelect}
            placeholder={strings('charts.chart_metric')}
            openOnFocus
            dataSource={metrics}
            disabled={metrics.length === 0}
            validate={required}
            handleSelect={this._handleMetric}
            reduxForm
          />
        </FormControl>
        {['long', 'integer', 'double', 'float'].includes(_metric) && (
          <FormControl fullWidth classes={{ root: classes.marginVertical }}>
            <Field
              name="func"
              component={IntegrationReactSelect}
              placeholder={strings('charts.chart_aggregate')}
              openOnFocus
              dataSource={options}
              validate={required}
              reduxForm
            />
          </FormControl>
        )}
        <FieldArray name="rangesArray" component={rangeFields} />
      </form>
    )
  }
}

// Decorate with redux-form
let GaugeChartConnected = reduxForm({
  form: 'gaugeChart',
  initialValues: {
    title: '',
    func: '',
    metric: ''
  },
  validate
})(GaugeChartForm)

const selector = formValueSelector('gaugeChart')

GaugeChartConnected = connect(state => {
  const metric = selector(state, 'metric')

  return {
    metric
  }
})(GaugeChartConnected)

export default withStyles(styles, { withTheme: true })(GaugeChartConnected)
