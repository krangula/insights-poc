import React from 'react'
import { SortableContainer, SortableElement } from 'react-sortable-hoc'

import Title from 'presentational/dashboards/title'
import DataMap from 'presentational/charts/map'
import ChartRenderer from './chart-renderer'
import Delete from './delete-widget'
import Edit from './edit-widget'

import { strings } from '../../../../locales/i18n'

export const SortableMap = SortableElement(({ fullScreen, index }) => (
  <li
    index={index}
    style={{
      position: 'relative',
      height: '100%',
      listStyleType: 'none'
    }}>
    <DataMap
      chartTitle={
        <Title title={strings('charts.title-simple-global-map')} variant="h6" />
      }
      fullScreenApp={fullScreen}
    />
  </li>
))

const SortableItem = SortableElement(
  ({ hideActions, mapProps, removeWidget, fullScreen, index }) => {
    const { id } = mapProps
    return (
      <li index={index} className="sortable-list">
        <div
          className="card-animate"
          style={{
            animationDelay: `${index === 0 ? 0 : 200 * (index / 2)}ms`
          }}>
          <div
            style={{
              position: 'absolute',
              right: 0,
              zIndex: 5
            }}>
            {!hideActions && <Edit details={mapProps} />}
            {id && !hideActions && (
              <Delete widget={id} removeWidget={removeWidget} />
            )}
          </div>
          <ChartRenderer {...mapProps} fullScreen={fullScreen} />
        </div>
      </li>
    )
  }
)

const SortableList = SortableContainer(
  ({ widgets, fullScreen, removeWidget, classes, hideActions }) => (
    <ul style={{ padding: 0, margin: 0 }} className={classes.dashboard}>
      {widgets.map((mapProps = {}, index) => (
        <SortableItem
          key={`item-${mapProps.id || mapProps.tempId}`}
          index={index}
          mapProps={mapProps}
          widgets={widgets}
          fullScreen={fullScreen}
          removeWidget={removeWidget}
          hideActions={hideActions}
        />
      ))}
    </ul>
  )
)

export default SortableList

// <SortableMap
//   index={widgets.length + 1}
//   key="item-map"
//   fullScreen={fullScreen}
// />
