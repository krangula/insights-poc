import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'
import { withStyles } from '@material-ui/core/styles'
import { Paper, InputBase, Zoom } from '@material-ui/core'
import { Search, Close } from '@material-ui/icons'

import * as Actions from 'reducers/reducer-dashboard'

import { strings } from '../../../../locales/i18n'

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    height: 34,
    width: 300,
    marginRight: 10,
    border: '1px solid rgba(255, 188, 3, 0.5)',
    backgroundColor: '#FFFFFF'
  },
  input: {
    flex: 1,
    color: theme.palette.type === 'dark' ? '#888888' : 'inherit',
    backgroundColor: '#FFFFFF'
  },
  iconButton: {
    padding: 5
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4
  }
})

class CustomizedInputBase extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      search: ''
    }
  }

  _doSearch = e => {
    const { charCode } = e
    const { search } = this.state
    const { setSmartSearchPrimary, enqueueSnackbar } = this.props

    if (charCode === 13) {
      enqueueSnackbar(strings('messages.searching'), {
        variant: 'info'
      })
      setSmartSearchPrimary(search)
    }
  }

  _handleClear = () => {
    const { setSmartSearchPrimary } = this.props
    this.setState(
      {
        search: ''
      },
      () => setSmartSearchPrimary()
    )
  }

  _handleChange = e => {
    const { value } = e.target
    const { setSmartSearchPrimary } = this.props

    if (value === '') {
      setSmartSearchPrimary()
    }

    this.setState({
      search: e.target.value
    })
  }

  render() {
    const { search } = this.state
    const { classes } = this.props

    return (
      <Zoom in>
        <Paper className={classes.root} elevation={0}>
          <Search className={classes.iconButton} />
          <InputBase
            className={classes.input}
            placeholder={strings('inputs.smart_filter')}
            value={search}
            onChange={this._handleChange}
            onKeyPress={this._doSearch}
          />
          <Zoom in={search !== ''}>
            <Close
              className={classes.iconButton}
              style={{ cursor: 'pointer' }}
              onClick={this._handleClear}
            />
          </Zoom>
        </Paper>
      </Zoom>
    )
  }
}

const mapStateToProps = ({ dashboard }) => ({ dashboard })
const mapDispatchToProps = {
  setSmartSearchPrimary: Actions.setSmartSearchPrimary
}

const ConnectedSearch = connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomizedInputBase)

export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedSearch)
)
