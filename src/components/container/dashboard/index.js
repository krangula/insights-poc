import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { CircularProgress } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import cx from 'classnames'

import SessionLost from 'presentational/accounts/session-lost'
import ApplicationError from 'presentational/app/app-error'
import * as botActions from 'reducers/reducer-bots'
import * as loginActions from 'reducers/reducer-login'
import * as mainActions from 'reducers/reducer-global'
import styles from 'appStyles'
import Compare from './compare'
import Dashboard from './dashboard'

class Benchmark extends Component {
  state = {
    value: 'bots',
    loading: true
  }

  componentDidMount() {
    this.setState({ loading: false })
  }

  handleChange = (event, value) => {
    const { history, match } = this.props
    const { value: val } = this.state

    if (val === value) return

    this.setState(
      {
        value
      },
      () => {
        history.push(`${match.url}/${value}`)
      }
    )
  }

  render() {
    const { loading } = this.state
    const { classes, login, main, dashboard, requestErrorReset } = this.props
    const { authData, loginValid } = login
    const { requestError, errorPayload } = main
    const { compare, fullScreen } = dashboard
    const { status, statusText } = errorPayload

    if (loading || !authData.token) {
      return (
        <div className={classes.loader}>
          <CircularProgress />
        </div>
      )
    }

    return (
      <Fragment>
        <SessionLost loginValid={loginValid} />
        <ApplicationError
          error={requestError}
          status={status}
          message={statusText}
          requestErrorReset={requestErrorReset}
        />
        <div
          className={cx(
            classes.mainContainer,
            compare && classes.mainContainerCompare,
            fullScreen && classes.mainContainerFullScreen,
            compare && fullScreen && classes.mainContainerFullScreenCompare
          )}>
          <Dashboard />
          {compare && <Compare />}
        </div>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ login, main, dashboard }) => ({
  login,
  main,
  dashboard
})
const mapDispatchToProps = {
  doLoginSuccess: loginActions.doLoginSuccess,
  setDateRange: botActions.setDateRange,
  requestErrorReset: mainActions.requestErrorReset
}

const ConnectedBenchmark = connect(
  mapStateToProps,
  mapDispatchToProps
)(Benchmark)

export default withStyles(styles, { withTheme: true })(ConnectedBenchmark)
