import React, { PureComponent, createRef } from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import moment from 'moment'
import arrayMove from 'array-move'
import SwipeableViews from 'react-swipeable-views'
import {
  Grow,
  Fade,
  Typography,
  CircularProgress,
  Button,
  Tab,
  Tabs
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'
import DateRange from 'presentational/picker'
import ServiceLoader from 'presentational/service-loader'
import ButtonComponent from 'presentational/dashboards/button-component'
import DashboardButtons from 'presentational/dashboards/dashboard-buttons'
import SimpleAlert from 'presentational/simple-alert'
import Icon from 'presentational/icon'
import * as dashboardActions from 'reducers/reducer-dashboard'
import {
  createDashboard,
  updateDashboard,
  deleteDashboard,
  getDashboards,
  getDashboardQueries
} from 'services/dashboard'
import styles from 'appStyles'
import { processWidgets } from './utils'
import { strings } from '../../../../locales/i18n'
import DashboardSelect from './select-dashboard'
import Library from './library'
import DataProfile from './data-profile'
import SortableList from './sortable'
import SmartSearch from './smart-search'

const growDelay = 500

const TabContainer = ({ children, dir }) => (
  <Typography component="div" dir={dir}>
    {children}
  </Typography>
)

class Bots extends PureComponent {
  static getDerivedStateFromProps(props) {
    const { dashboard } = props
    const { compare } = dashboard

    if (compare) {
      return {
        scrolled: false
      }
    }

    return null
  }

  constructor(props) {
    super(props)

    this.timer = growDelay
    this.dashboard = createRef()

    this.state = {
      tabValue: 0,
      loading: false,
      requestingDashboard: false,
      dashboard: '',
      scrolled: false
    }
  }

  componentDidMount() {
    this._getDashboards()
  }

  componentDidUpdate(prevProps) {
    const { dashboard: prevDashboard } = prevProps
    const { dashboard, setGeneratedDashboard } = this.props
    const {
      selectedDashboard: prevSelected,
      smartSearchPrimary: prevSearch
    } = prevDashboard
    const {
      selectedDashboard,
      generatedDashboard,
      smartSearchPrimary: search
    } = dashboard

    if (
      selectedDashboard.id &&
      (prevSelected.dateFrom !== selectedDashboard.dateFrom ||
        prevSelected.dateTo !== selectedDashboard.dateTo ||
        selectedDashboard.id !== prevSelected.id ||
        search !== prevSearch)
    ) {
      this._getDashboardWidgets(selectedDashboard)
    }

    if (generatedDashboard !== '') {
      this._handleChange(generatedDashboard)
      setGeneratedDashboard()
    }
  }

  _getDashboards = () => {
    const { dashboard, setDashboardList } = this.props
    const { demoMode } = dashboard
    getDashboards().then(response => {
      if (demoMode) {
        setDashboardList(response)
        return
      }
      const { status } = response
      if (status === 200) {
        response.json().then(data => {
          const { dashboards = [] } = data
          setDashboardList(dashboards)
        })
      }
    })
  }

  _getDashboardWidgets = currentDashboard => {
    const {
      dashboard,
      setKibanaDashboard,
      updateDashboardStart,
      updateDashboardFinish
    } = this.props
    const { smartSearchPrimary } = dashboard

    processWidgets({
      dashboard: currentDashboard,
      search: smartSearchPrimary,
      setDashboardData: setKibanaDashboard,
      updateStart: updateDashboardStart,
      updateFinish: updateDashboardFinish
    })
  }

  handleBeforeChange = (args = {}) => {
    const { dashboard } = this.props
    const { activeDashboard, selectedDashboard, selected } = dashboard
    const { visuals = [], queries = [] } = activeDashboard
    const { widgets = [], type } = selectedDashboard
    let hasNewWidgets = false
    let hasNewQueries = false

    if (selected !== '' && type !== 'AUTO') {
      for (let i = 0; i < widgets.length; i++) {
        const { meta = {} } = queries[i]
        const { meta: original } = widgets[i]

        if (JSON.stringify(meta) !== original) {
          hasNewQueries = true
          break
        }
      }

      hasNewWidgets = visuals.find(visual => !visual.id)

      if (hasNewWidgets || hasNewQueries) {
        this.setState({
          dirty: true,
          nextDashboard: args
        })
        return
      }
    }

    this._handleChange(args)
  }

  _handleChange = (args = {}) => {
    const {
      requestDashboard,
      receiveDashboard,
      setSelectedDashboard,
      setKibanaDashboard,
      dashboard
    } = this.props
    const { selected } = dashboard
    const { value, update } = args
    const { value: primaryValue } = selected

    if (primaryValue === value) return

    const dashboardValue = value || primaryValue

    this.timer = growDelay

    this.setState({
      scrolled: false
    })

    requestDashboard({ update })

    getDashboardQueries(dashboardValue)
      .then(response => {
        const { status } = response
        if (status === 200) {
          receiveDashboard({ update })

          response.json().then((dashboardData = {}) => {
            const { startDate, endDate } = dashboardData
            const _dashboard = update ? selected : args
            setSelectedDashboard(dashboardData, _dashboard, {
              dateFrom: moment(startDate).format('YYYY-MM-DDTHH:MM'),
              dateTo: moment(endDate).format('YYYY-MM-DDTHH:MM')
            })

            if (update) this._getDashboardWidgets(dashboardData)
          })
        } else {
          receiveDashboard()
          setKibanaDashboard()
        }
      })
      .catch(() => {
        setKibanaDashboard()
        receiveDashboard()
      })
  }

  saveDashboard = (title, description) => {
    const {
      bots,
      dashboard,
      enqueueSnackbar,
      updateDashboardStart,
      updateDashboardFinish,
      setGeneratedDashboard
    } = this.props
    const { fromDate, toDate } = bots
    const { selectedDashboard, activeDashboard, dashboards } = dashboard
    const { queries } = activeDashboard
    const { taskId } = selectedDashboard

    if (title) {
      if (dashboards.findIndex(({ label }) => label === title) !== -1) {
        enqueueSnackbar(strings('dashboard.dashboard-duplicate'), {
          variant: 'error'
        })
        return
      }
    }

    updateDashboardStart()

    const service = title ? createDashboard : updateDashboard

    setTimeout(() => {
      queries.map((item, idx) => {
        const { meta, query, id } = item
        queries[idx] = {
          id,
          meta: JSON.stringify(meta),
          query: JSON.stringify(query)
        }
      })

      let opts = {
        startDate: new Date(fromDate).toISOString(),
        endDate: new Date(toDate).toISOString(),
        widgets: queries
      }

      if (title) {
        opts = {
          ...opts,
          taskId,
          title,
          description
        }
      } else {
        opts = {
          ...selectedDashboard,
          ...opts
        }
      }

      service(opts)
        .then(response => {
          const { status } = response
          updateDashboardFinish()
          if (status === 200 || status === 204) {
            const message = title
              ? `${strings('dashboard.dashboard-created')} ${title}`
              : strings('dashboard.dashboard-updated')
            enqueueSnackbar(message, {
              variant: 'success'
            })

            if (title) {
              response.json().then(data => {
                const { id, title: label } = data
                setGeneratedDashboard({
                  label,
                  value: id
                })
              })
            } else {
              // setSelectedDashboard(opts)
              this._handleChange({ update: true })
            }

            return
          }

          enqueueSnackbar(strings('dashboard.dashboard-not-saved'), {
            variant: 'error'
          })
        })
        .catch(() =>
          enqueueSnackbar(strings('dashboard.dashboard-not-saved'), {
            variant: 'error'
          })
        )
    }, 1500)
  }

  deleteDashboard = () => {
    const {
      dashboard,
      enqueueSnackbar,
      resetDashboard,
      removeDashboard,
      updateDashboardStart,
      updateDashboardFinish
    } = this.props
    const { selected } = dashboard
    const { value } = selected

    updateDashboardStart()

    setTimeout(() => {
      deleteDashboard(value).then(response => {
        updateDashboardFinish()
        const { status } = response
        if (status === 200) {
          enqueueSnackbar(strings('dashboard.dashboard-deleted'), {
            variant: 'success'
          })

          resetDashboard()
          removeDashboard(value)
        }
      })
    }, 1500)
  }

  _tabChange = (event, value) => {
    this.setState({
      tabValue: value
    })
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { dashboard, reorderWidgets } = this.props
    const { activeDashboard } = dashboard
    const { visuals = [], queries = [] } = activeDashboard
    const visualsPayload = arrayMove(visuals, oldIndex, newIndex)
    const queriesPayload = arrayMove(queries, oldIndex, newIndex)
    reorderWidgets({ visuals: visualsPayload, queries: queriesPayload })
  }

  render() {
    const { tabValue, nextDashboard, dirty, scrolled } = this.state
    const {
      classes,
      theme,
      bots,
      dashboard,
      removeWidget,
      setDateRange,
      toggleFullScreen,
      toggleCompare
    } = this.props
    const {
      updatingDashboard,
      requestingDashboard,
      preparingDashboards,
      selected = {},
      compareSelected = {},
      selectedDashboard,
      activeDashboard = {},
      fullScreen,
      widgetSwitch,
      compare
    } = dashboard

    const { fromDate, toDate } = bots
    const { value = '', label } = selected
    const { value: compareValue = '' } = compareSelected
    const { visuals: widgets = [] } = activeDashboard
    const { type } = selectedDashboard

    const showDashboardMessage = preparingDashboards || requestingDashboard
    const showLibrary = selected !== '' && tabValue === 0
    return (
      <div
        className={classNames(
          fullScreen && classes.dashboardContainerFullScreen,
          !fullScreen && !compare && classes.dashboardContainer,
          fullScreen && compare && classes.dashboardFullScreenCompare
        )}>
        <Library
          fullScreen={fullScreen}
          disabled={type === 'AUTO'}
          show={showLibrary && !compare}
          widgetSwitch={widgetSwitch}
        />

        <SimpleAlert
          isOpen={dirty}
          title={strings('dashboard.dashboard-unsaved')}
          message={strings('dashboard.dashboard-unsaved-message')}
          acceptHandler={() =>
            this.setState({ dirty: false }, () =>
              this._handleChange(nextDashboard)
            )
          }
          cancelHandler={() => this.setState({ dirty: false })}
        />
        {updatingDashboard && <ServiceLoader />}

        <div className={classes.searchBar}>
          {!scrolled && (
            <DashboardSelect handleChange={this.handleBeforeChange} />
          )}
          {compare && (
            <ButtonComponent
              label={strings('buttons.label-compare')}
              classes={classes}
              handler={toggleCompare}
              disabled={selected === ''}
              variant={compare && 'contained'}
              style={{ margin: 0, marginLeft: 10 }}
            />
          )}
        </div>

        <div className={classes.tabContainer}>
          {!scrolled && (
            <Fade in>
              <Tabs
                value={tabValue}
                onChange={this._tabChange}
                indicatorColor="secondary"
                textColor="primary">
                <Tab
                  disabled={selected === ''}
                  label={strings('tabs.label-dashboard')}
                />
                <Tab
                  disabled={selected === ''}
                  label={strings('tabs.label-profile')}
                />
              </Tabs>
            </Fade>
          )}

          {selected !== '' && (
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              {!compare && tabValue === 0 && (
                <div className={scrolled && classes.fixedSearch}>
                  <SmartSearch />
                </div>
              )}
              {!scrolled && !compare && tabValue === 0 && (
                <DashboardButtons
                  disabled={Object.keys(activeDashboard).length === 0}
                  fullScreen={fullScreen}
                  toggleCompare={toggleCompare}
                  saveDashboardAs={this.saveDashboard}
                  saveDashboard={this.saveDashboard}
                  deleteDashboard={this.deleteDashboard}
                  dashboardType={type}
                />
              )}
              {tabValue === 0 && (
                <div className={scrolled && classes.fixedPicker}>
                  <Fade in>
                    <DateRange
                      setFn={setDateRange}
                      fromDate={fromDate}
                      toDate={toDate}
                      disabled={value === '' || showDashboardMessage}
                      compare={compare}
                      variant={scrolled && 'contained'}
                    />
                  </Fade>
                </div>
              )}

              {!scrolled && !compare && (
                <Button
                  color="secondary"
                  variant={fullScreen ? 'contained' : 'outlined'}
                  onClick={toggleFullScreen}
                  style={{ marginLeft: 10 }}>
                  <Icon name={fullScreen ? 'fullscreen_exit' : 'fullscreen'} />
                </Button>
              )}
            </div>
          )}
        </div>

        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={tabValue}
          onChangeIndex={this.handleChangeIndex}>
          <TabContainer dir={theme.direction}>
            <div id="dashboard-container">
              {showDashboardMessage && (
                <Grow in={showDashboardMessage} timeout={500}>
                  <div
                    className={classNames(classes.gridEmpty, classes.gridBase)}>
                    <CircularProgress color="secondary" />
                    <Typography variant="h6">
                      {preparingDashboards
                        ? strings('messages.preparing-dashboards-message')
                        : strings('messages.requesting-dashboard-data-message')}
                    </Typography>
                  </div>
                </Grow>
              )}
              {selected !== '' && !requestingDashboard && !preparingDashboards && (
                <div
                  id="active-dashboard"
                  className={classNames({
                    'widget-container': true,
                    scrolled
                  })}
                  onScroll={e => {
                    if (compare && value === compareValue) {
                      const active = document.getElementById('active-dashboard')
                      const compared = document.getElementById(
                        'compare-dashboard'
                      )

                      compared.scrollTop = active.scrollTop
                      return
                    }

                    const { target } = e
                    const { scrollTop } = target
                    if (scrollTop >= 20 && !scrolled) {
                      this.setState({ scrolled: true })
                    } else if (scrollTop < 20 && scrolled) {
                      this.setState({ scrolled: false })
                    }
                  }}>
                  {widgets.length > 0 ? (
                    <div id="dashboard">
                      <SortableList
                        axis="xy"
                        disabled={type === 'AUTO'}
                        helperClass="sortableHelper"
                        classes={classes}
                        widgets={widgets}
                        fullScreen={fullScreen}
                        removeWidget={removeWidget}
                        hideActions={compare}
                        shouldCancelStart={event =>
                          typeof event.target.className === 'object' ||
                          event.target.className === 'material-icons' ||
                          compare
                        }
                        onSortEnd={this.onSortEnd}
                      />
                    </div>
                  ) : (
                    <Grow in={widgets.length === 0} timeout={1000}>
                      <div
                        className={classNames(
                          classes.gridEmpty,
                          classes.gridBase
                        )}>
                        <Typography variant="h6">{`No data found for ${label}`}</Typography>
                        <ButtonComponent
                          label={strings('buttons.label-retry')}
                          handler={() =>
                            this._getDashboardWidgets(selectedDashboard)
                          }
                          icon="refresh"
                        />
                      </div>
                    </Grow>
                  )}
                </div>
              )}
              {selected === '' && !requestingDashboard && !preparingDashboards && (
                <Grow in timeout={1000}>
                  <div
                    className={classNames(classes.gridEmpty, classes.gridBase)}>
                    <Typography variant="h6">
                      {strings('messages.select-dashboard-to-begin')}
                    </Typography>
                  </div>
                </Grow>
              )}
            </div>
          </TabContainer>
          <TabContainer>{tabValue === 1 && <DataProfile />}</TabContainer>
        </SwipeableViews>

        <div style={{ position: 'fixed' }} />
      </div>
    )
  }
}

const mapStateToProps = ({ bots, dashboard }) => ({
  bots,
  dashboard
})

const mapDispatchToProps = {
  setDateRange: dashboardActions.setDateRange,
  setDashboard: dashboardActions.setDashboard,
  removeWidget: dashboardActions.removeWidget,
  setDashboardList: dashboardActions.setDashboardList,
  setDashboardData: dashboardActions.setDashboardData,
  requestDashboard: dashboardActions.requestDashboard,
  receiveDashboard: dashboardActions.receiveDashboard,
  toggleFullScreen: dashboardActions.toggleFullScreen,
  setKibanaDashboard: dashboardActions.setKibanaDashboard,
  setSelectedDashboard: dashboardActions.setSelectedDashboard,
  resetDashboard: dashboardActions.resetDashboard,
  removeDashboard: dashboardActions.removeDashboard,
  updateDashboardStart: dashboardActions.updateDashboardStart,
  updateDashboardFinish: dashboardActions.updateDashboardFinish,
  toggleCompare: dashboardActions.toggleCompare,
  setGeneratedDashboard: dashboardActions.setGeneratedDashboard,
  reorderWidgets: dashboardActions.reorderWidgets
}

const ConnectedBots = connect(
  mapStateToProps,
  mapDispatchToProps
)(Bots)
export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedBots)
)
