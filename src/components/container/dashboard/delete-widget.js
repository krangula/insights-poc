import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  IconButton,
  Button,
  Typography
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withSnackbar } from 'notistack'

import Icon from 'presentational/icon'
import styles from 'appStyles'
import { strings } from '../../../../locales/i18n'

class DeleteChart extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }

  _handleClose = () => {
    this.setState({
      open: false
    })
  }

  _handleOpen = () => {
    this.setState({
      open: true
    })
  }

  _handleDelete = () => {
    const { widget, removeWidget, enqueueSnackbar } = this.props
    this.setState(
      {
        open: false
      },
      () => {
        removeWidget(widget)
        enqueueSnackbar(strings('snackbar.message-api-call-to-delete-chart'), {
          variant: 'info'
        })
      }
    )
  }

  render() {
    const { open } = this.state
    const { classes, theme, dashboard } = this.props
    const { selectedDashboard } = dashboard
    const { type } = selectedDashboard

    if (type === 'AUTO') return null

    return (
      <Fragment>
        <IconButton
          onClick={this._handleOpen}
          style={{ color: theme.palette.error.main }}>
          <Icon name="delete" />
        </IconButton>
        <Dialog
          open={open}
          onClose={this._handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogTitle id="alert-dialog-title">
            {strings('dialog.title-delete-widget')}
          </DialogTitle>
          <DialogContent
            classes={{
              root: classes.noPadding
            }}>
            <DialogContentText id="alert-dialog-description">
              {strings('dialog.content-delete-widget-confirmation')}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="secondary"
              onClick={this._handleClose}>
              <Typography variant="button">
                {strings('buttons.label-cancel')}
              </Typography>
            </Button>
            <Button
              variant="contained"
              color="secondary"
              style={{ color: theme.palette.error.main }}
              onClick={this._handleDelete}
              className={classes.button}
              autoFocus>
              <Typography variant="button">
                {strings('buttons.label-delete')}
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    )
  }
}

const mapStateToProps = ({ dashboard }) => ({
  dashboard
})

const mapDispatchToProps = {}

const ConnectedDelete = connect(
  mapStateToProps,
  mapDispatchToProps
)(DeleteChart)

export default withStyles(styles, { withTheme: true })(
  withSnackbar(ConnectedDelete)
)
