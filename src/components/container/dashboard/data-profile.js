import React, { PureComponent, Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Select from 'react-select'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'
import classNames from 'classnames'
import moment from 'moment'
import humanize from 'humanize'
import {
  Typography,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Popper,
  Fade,
  Card,
  Button,
  TextField,
  Checkbox,
  ClickAwayListener,
  CircularProgress
} from '@material-ui/core'
import * as dataProfileActions from 'reducers/reducer-dataprofile'
import styles from 'appStyles'
import { getDataProfileData, getRankValuesCount } from 'services/data-profile'
import ButtonComponent from 'presentational/dashboards/button-component'

const dataProfileStyles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.type !== 'dark' && '#e8e8e8'
    },
    '&:nth-of-type(even)': {
      backgroundColor: theme.palette.type !== 'dark' && '#d8d8d8'
    }
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 10
  },
  rankValue: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
})

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: '#666666',
    color: theme.palette.common.white,
    borderRight: '1px solid rgba(224, 224, 224, 1)',
    borderLeft: '1px solid rgba(224, 224, 224, 1)',
    textAlign: 'center',
    padding: '5px 10px'
  },
  body: {
    borderRight: '1px solid rgba(224, 224, 224, 1)',
    borderLeft: '1px solid rgba(224, 224, 224, 1)',
    padding: '5px 10px'
  }
}))(TableCell)

const rankRange = [{ value: '5', label: '5' }, { value: '10', label: '10' }]

const options = [
  { value: 'desc', label: 'Top' },
  { value: 'asc', label: 'Bottom' }
]

const numericOptions = [
  { value: 'NUMERIC', label: 'Numeric' },
  { value: 'STRING', label: 'String' },
  { value: 'CNTRY_CODE', label: 'Country Code' },
  { value: 'State Code', label: 'State Code' },
  { value: 'Zip Code', label: 'Zip Code' }
]

const stringOptions = [
  { value: 'STRING', label: 'String' },
  { value: 'CNTRY_CODE', label: 'Country Code' },
  { value: 'State Code', label: 'State Code' },
  { value: 'Zip Code', label: 'Zip Code' }
]

const timestampOptions = [{ value: 'TIMESTAMP', label: 'Timestamp' }]
const countrycodeOptions = [{ value: 'CNTRY_CODE', label: 'Country Code' }]
const stateCodeOptions = [{ value: 'State Code', label: 'State Code' }]
const zipCodeOptions = [{ value: 'Zip Code', label: 'Zip Code' }]

class DataProfile extends PureComponent {
  state = {
    anchorEl: null,
    variableName: null,
    direction: options[0],
    range: rankRange[0],
    editMode: false,
    arrowRef: null,
    datatypeOptions: [],
    loadingDataProfile: true
  }

  componentDidMount() {
    this._getDataProfile()
  }

  componentDidUpdate(prevProps) {
    const { dashboard: prevDashboard } = prevProps
    const { dashboard } = this.props
    const { selectedDashboard: prevSelected } = prevDashboard
    const { selectedDashboard } = dashboard

    if (selectedDashboard.id !== prevSelected.id) {
      this._getDataProfile()
    }
  }

  _getDataProfile = () => {
    const { setDataprofile, dashboard, setInitialDataProfile } = this.props
    const { selectedDashboard } = dashboard
    const { id } = selectedDashboard
    getDataProfileData(id)
      .then(response => {
        const { status } = response
        if (status === 200) {
          response.json().then(data => {
            const dataProfile = data
            const { dataProfiles } = dataProfile
            setInitialDataProfile(dataProfiles)
            setDataprofile(dataProfiles)
            this.setState({ loadingDataProfile: false })
          })
        } else {
          this.setState({ loadingDataProfile: false })
        }
      })
      .catch(() => {
        this.setState({ loadingDataProfile: false })
      })
  }

  _getRankValuesCount = () => {
    const { dashboard, setDistinctValues } = this.props
    const { direction, range, variableName } = this.state
    const { selectedDashboard } = dashboard
    const { id } = selectedDashboard
    getRankValuesCount(
      id,
      direction.value,
      variableName,
      Number(range.value)
    ).then(response => {
      const { status } = response
      if (status === 200) {
        response.json().then(data => {
          const { distinctValues } = data
          distinctValues.sort((obj1, obj2) =>
            direction.value === 'asc'
              ? Number(obj1.count) - Number(obj2.count)
              : Number(obj2.count) - Number(obj1.count)
          )
          setDistinctValues(distinctValues)
        })
      }
    })
  }

  _handleRankClick = variableName => event => {
    const { currentTarget } = event
    this.setState(
      {
        anchorEl: currentTarget,
        variableName
      },
      () => {
        this._getRankValuesCount()
      }
    )
  }

  _onDataTypeOptionsChange = (row, event) => {
    const { setProfileVariablesDataType } = this.props
    const selected = event.value

    setProfileVariablesDataType({ row, selected })
  }

  _onInclusionChange = (row, event) => {
    const { setProfileVariablesEnabled } = this.props
    const selected = event.target.checked ? 'Y' : 'N'
    setProfileVariablesEnabled({ row, selected })
  }

  _datatypeOptions = row => {
    let datatypeOptions
    let selected
    const isChanged =
      row.defaultDatatype && row.defaultDatatype !== row.attributeType
    const datatype = isChanged ? row.defaultDatatype : row.attributeType

    switch (datatype) {
      case 'NUMERIC':
        datatypeOptions = numericOptions
        break
      case 'STRING':
        datatypeOptions = stringOptions
        break
      case 'TIMESTAMP':
        datatypeOptions = timestampOptions
        break
      case 'CNTRY_CODE':
        datatypeOptions = countrycodeOptions
        break
      case 'State Code':
        datatypeOptions = stateCodeOptions
        break
      case 'Zip Code':
        datatypeOptions = zipCodeOptions
        break
      default:
        datatypeOptions = numericOptions
    }
    datatypeOptions.forEach(option => {
      if (option.value === row.attributeType) {
        selected = option
      }
    })

    return (
      <CustomTableCell style={{ minWidth: 125 }}>
        <Select
          options={datatypeOptions}
          value={selected}
          onChange={event => this._onDataTypeOptionsChange(row, event)}
        />
      </CustomTableCell>
    )
  }

  _handleRankPopperClose = () => {
    this.setState({
      anchorEl: null,
      variableName: ''
    })
  }

  _handleDirectionChange = event => {
    this.setState(
      {
        direction: event
      },
      () => {
        this._getRankValuesCount()
      }
    )
  }

  _handleRangeChange = event => {
    this.setState(
      {
        range: event
      },
      () => {
        this._getRankValuesCount()
      }
    )
  }

  _handleInputChange = (row, event) => {
    const { setProfileVariables } = this.props

    setProfileVariables({ displayName: event.target.value, row })
  }

  _editDataProfile = () => {
    this.setState({
      editMode: true
    })
  }

  _cancelDataProfile = () => {
    const { setDataprofile, dataProfile } = this.props
    const { initialDataProfile } = dataProfile
    this.setState({
      editMode: false
    })
    setDataprofile(initialDataProfile)
  }

  _saveDataProfile = () => {
    const { setDataprofile, dataProfile } = this.props
    const { initialDataProfile } = dataProfile
    this.setState({
      editMode: false
    })
    setDataprofile(initialDataProfile)
  }

  render() {
    const { classes, theme, dataProfile, dashboard } = this.props
    const { dataProfileData, distinctValues } = dataProfile
    const { selectedDashboard } = dashboard
    const { title, id } = selectedDashboard
    const {
      anchorEl,
      variableName,
      direction,
      range,
      editMode,
      loadingDataProfile
    } = this.state

    if (loadingDataProfile) {
      return (
        <div className={classNames(classes.gridEmpty, classes.gridBase)}>
          <CircularProgress color="secondary" />
          <Typography variant="h6">Preparing data profile table...</Typography>
        </div>
      )
    }

    return (
      <div>
        <div
          style={{
            height: '100%',
            background: theme.palette.type !== 'dark' && '#f3f3f3'
          }}>
          <div className={classes.titleContainer}>
            <Typography variant="h6">{`Task Name: ${title}`}</Typography>
            {dataProfileData.length > 0 && (
              <Button
                id="rank-button"
                variant="outlined"
                color="secondary"
                target="_blank"
                href={`/#/rank-rawdata/${title}/${id}`}>
                <Typography variant="button">Preview Data</Typography>
              </Button>
            )}
          </div>
          {dataProfileData.length > 0 ? (
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <CustomTableCell>VARIABLE NAME</CustomTableCell>
                  <CustomTableCell>DISPLAY NAME</CustomTableCell>
                  <CustomTableCell>DATATYPE</CustomTableCell>
                  <CustomTableCell>INCLUSION</CustomTableCell>
                  <CustomTableCell>MINIMUM</CustomTableCell>
                  <CustomTableCell>MAXIMUM</CustomTableCell>
                  <CustomTableCell>AVERAGE</CustomTableCell>
                  <CustomTableCell>SUM</CustomTableCell>
                  <CustomTableCell>DISTINCT COUNT</CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataProfileData.map(
                  (row, index) =>
                    row.profileVariables &&
                    row.profileVariables.map(profileVariable => (
                      <TableRow
                        className={classes.row}
                        key={profileVariable.id}>
                        <CustomTableCell>
                          {profileVariable.variableName}
                        </CustomTableCell>
                        {editMode ? (
                          <CustomTableCell>
                            <TextField
                              id={`display-name-input-${index}`}
                              type="text"
                              name={`display-name-${index}`}
                              autoComplete="off"
                              onChange={event =>
                                this._handleInputChange(profileVariable, event)
                              }
                              value={profileVariable.displayName}
                            />
                          </CustomTableCell>
                        ) : (
                          <CustomTableCell>
                            {profileVariable.displayName}
                          </CustomTableCell>
                        )}
                        {editMode ? (
                          this._datatypeOptions(profileVariable)
                        ) : (
                          <CustomTableCell>
                            {profileVariable.attributeType}
                          </CustomTableCell>
                        )}
                        {editMode ? (
                          <CustomTableCell style={{ textAlign: 'center' }}>
                            <Checkbox
                              checked={profileVariable.enabled === 'Y'}
                              value="checkedA"
                              inputProps={{ 'aria-label': 'Checkbox A' }}
                              onChange={event =>
                                this._onInclusionChange(profileVariable, event)
                              }
                            />
                          </CustomTableCell>
                        ) : (
                          <CustomTableCell style={{ textAlign: 'center' }}>
                            {profileVariable.enabled === 'Y' && (
                              <i
                                style={{ color: '#006400' }}
                                className="material-icons">
                                done
                              </i>
                            )}
                          </CustomTableCell>
                        )}
                        <CustomTableCell style={{ textAlign: 'right' }}>
                          {profileVariable.attributeType === 'TIMESTAMP'
                            ? moment(profileVariable.minimumValue).format(
                                'MM/DD/YYYY hh:mm'
                              )
                            : humanize.numberFormat(
                                profileVariable.minimumValue
                              )}
                        </CustomTableCell>
                        <CustomTableCell style={{ textAlign: 'right' }}>
                          {profileVariable.attributeType === 'TIMESTAMP'
                            ? moment(profileVariable.maximumValue).format(
                                'MM/DD/YYYY hh:mm'
                              )
                            : humanize.numberFormat(
                                profileVariable.maximumValue
                              )}
                        </CustomTableCell>
                        <CustomTableCell style={{ textAlign: 'right' }}>
                          {humanize.numberFormat(
                            profileVariable.averageOfValues
                          )}
                        </CustomTableCell>
                        <CustomTableCell style={{ textAlign: 'right' }}>
                          {humanize.numberFormat(profileVariable.sumOfValue)}
                        </CustomTableCell>
                        <CustomTableCell>
                          <div className={classes.rankValue}>
                            <ButtonComponent
                              label="Rank"
                              variant={
                                variableName === profileVariable.variableName
                                  ? 'contained'
                                  : 'outlined'
                              }
                              aria-describedby={index}
                              style={{
                                visibility:
                                  profileVariable.attributeType !== 'NUMERIC'
                                    ? 'visible'
                                    : 'hidden'
                              }}
                              handler={this._handleRankClick(
                                profileVariable.variableName
                              )}
                            />
                            <span>{profileVariable.totalDistincts}</span>
                          </div>
                        </CustomTableCell>
                      </TableRow>
                    ))
                )}
              </TableBody>
            </Table>
          ) : (
            <div className={classNames(classes.gridEmpty, classes.gridBase)}>
              <Typography variant="h6">No data found</Typography>
              <ButtonComponent
                label="Retry"
                handler={() => this._getDataProfile()}
                icon="refresh"
              />
            </div>
          )}

          <Popper
            id="rank-tooltip"
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            placement="left"
            transition
            disablePortal={false}>
            {({ TransitionProps }) => (
              <Fade {...TransitionProps}>
                <ClickAwayListener onClickAway={this._handleRankPopperClose}>
                  <Card style={{ padding: 5 }}>
                    <div
                      style={{
                        display: 'flex'
                      }}>
                      <span style={{ width: '50%', marginRight: 5 }}>
                        <Select
                          options={options}
                          value={direction}
                          onChange={this._handleDirectionChange}
                        />
                      </span>
                      <span style={{ width: '50%' }}>
                        <Select
                          options={rankRange}
                          value={range}
                          onChange={this._handleRangeChange}
                        />
                      </span>
                    </div>
                    <Table style={{ maxWidth: 250 }}>
                      <TableHead>
                        <TableRow>
                          <TableCell>Rank</TableCell>
                          <TableCell>Value</TableCell>
                          <TableCell>Count</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {distinctValues.map((rankRow, index) => (
                          <TableRow key={rankRow.value}>
                            <TableCell>{index + 1}</TableCell>
                            <TableCell>{rankRow.value}</TableCell>
                            <TableCell>{rankRow.count}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </Card>
                </ClickAwayListener>
              </Fade>
            )}
          </Popper>
        </div>
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            margin: '10px 0px'
          }}>
          {!editMode && dataProfileData.length > 0 && (
            <ButtonComponent
              label="Edit"
              style={{ margin: 0 }}
              handler={this._editDataProfile}
            />
          )}
          {editMode && (
            <Fragment>
              <ButtonComponent
                label="Cancel"
                style={{ marginRight: 10 }}
                handler={this._cancelDataProfile}
              />
              <ButtonComponent
                label="Save"
                style={{ margin: 0 }}
                handler={this._saveDataProfile}
              />
            </Fragment>
          )}
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = {
  setDataprofile: dataProfileActions.setDataprofile,
  setProfileVariables: dataProfileActions.setProfileVariables,
  setProfileVariablesEnabled: dataProfileActions.setProfileVariablesEnabled,
  setProfileVariablesDataType: dataProfileActions.setProfileVariablesDataType,
  setDistinctValues: dataProfileActions.setDistinctValues,
  setInitialDataProfile: dataProfileActions.setInitialDataProfile
}

const mapStateToProps = ({ dataProfile, dashboard }) => ({
  dataProfile,
  dashboard
})

const ConnectedDataProfile = connect(
  mapStateToProps,
  mapDispatchToProps
)(DataProfile)

export default withStyles(
  theme => ({ ...styles(theme), ...dataProfileStyles(theme) }),
  {
    withTheme: true
  }
)(withSnackbar(ConnectedDataProfile))
