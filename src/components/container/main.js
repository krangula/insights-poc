import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import classNames from 'classnames'
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { CircularProgress, Typography, Zoom } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

import NavLinks from 'presentational/drawer'
import AppBar from 'presentational/app/app-bar'

import { authenticateToken } from 'services/authenticate'
import { doLoginSuccess } from 'reducers/reducer-login'
import styles from 'appStyles'
import RawData from './dashboard/rank-rawdata'
import AppRoutes from './routes'

import store from '../../store'
import { strings } from '../../../locales/i18n'

class MiniDrawer extends React.Component {
  constructor(props) {
    super(props)
    this.activeSession = this.activeSession.bind(this)

    this.state = {
      open: false,
      loading: true,
      session: false
    }
  }

  componentDidMount() {
    const storedToken = localStorage.getItem('authToken')
    let parsedToken

    try {
      parsedToken = JSON.parse(storedToken)
    } catch (e) {
      parsedToken = storedToken
    }

    setTimeout(() => {
      if (parsedToken) {
        authenticateToken(parsedToken)
          .then(validation => {
            const { status } = validation
            if (status === 200) {
              validation.json().then(validate => {
                const { token, user } = validate
                const { username } = user

                store.dispatch(doLoginSuccess({ token }, username))

                this.setState({
                  loading: false,
                  session: true
                })
              })
            } else {
              this.setState({
                loading: false
              })
            }
          })
          .catch(() =>
            this.setState({
              loading: false
            })
          )
      } else {
        this.setState({
          loading: false
        })
      }
    }, 1850)
  }

  activeSession() {
    this.setState({
      session: true
    })
  }

  render() {
    const { loading, session } = this.state
    const { classes, theme, toggleDark, themeType } = this.props
    const { palette } = theme
    const { type } = palette

    if (loading) {
      return (
        <Zoom in>
          <div className={classes.gridPlaceholder}>
            <CircularProgress color="secondary" />
            {type === 'dark' ? (
              <div className="app-logo-white" />
            ) : (
              <div className="app-logo" />
            )}
            <Typography variant="h6">
              {strings('messages.welcome-to-app-message')}
            </Typography>
          </div>
        </Zoom>
      )
    }

    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route
              exact
              path="/rank-rawdata/:dashboardTitle/:dashboardId"
              render={props => {
                if (!session) {
                  return <Redirect to="/" />
                }
                return (
                  <div
                    className={classNames(
                      classes.root,
                      !session && classes.loginContainer
                    )}>
                    {!loading && session && (
                      <AppBar themeType={themeType} toggleDark={toggleDark} />
                    )}
                    <main className={classNames(session && classes.content)}>
                      <RawData {...props.match.params} />
                    </main>
                  </div>
                )
              }}
            />
            <Route
              render={({ location }) => (
                <div
                  className={classNames(
                    classes.root,
                    !session && classes.loginContainer
                  )}>
                  {!loading && session && (
                    <Fragment>
                      <AppBar themeType={themeType} toggleDark={toggleDark} />
                      <NavLinks />
                    </Fragment>
                  )}
                  <main className={classNames(session && classes.content)}>
                    <AppRoutes
                      session={session}
                      location={location}
                      activeSession={this.activeSession}
                    />
                  </main>
                </div>
              )}
            />
          </Switch>
        </Router>
      </Provider>
    )
  }
}

MiniDrawer.propTypes = {
  classes: PropTypes.object.isRequired // eslint-disable-line react/forbid-prop-types
}

export default withStyles(styles, { withTheme: true })(MiniDrawer)
