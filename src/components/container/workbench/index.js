import React, { Component } from 'react'
import { Typography } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { withSnackbar } from 'notistack'

import ButtonComponent from 'presentational/dashboards/button-component'
import * as dashboardActions from 'reducers/reducer-dashboard'
import { getRunBot, getAnalyzeDetails } from 'services/dashboard'

import styles from 'appStyles'

class WorkBench extends Component {
  _getRunBot = () => {
    const { enqueueSnackbar } = this.props
    getRunBot().then(response => {
      const { status } = response
      if (status === 200) {
        enqueueSnackbar('Bot ran successfully', {
          variant: 'success'
        })
      }
    })
  }

  _getAnalyzeDetails = () => {
    const { enqueueSnackbar, setGeneratedDashboard, history } = this.props
    getAnalyzeDetails().then(response => {
      const { status } = response
      if (status === 200) {
        response.json().then(data => {
          const dashboard = data
          enqueueSnackbar(`${dashboard.title} generated successfully`, {
            variant: 'success'
          })
          history.push('/configure')

          const { id, title } = dashboard
          setGeneratedDashboard({
            label: title,
            value: id
          })
        })
      }
    })
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.mainContainer}>
        <Typography variant="h2">Workbench</Typography>
        <ButtonComponent
          label="Run a Bot"
          style={{ width: 125 }}
          handler={this._getRunBot}
        />
        <ButtonComponent
          label="Analyze"
          style={{ width: 125 }}
          handler={this._getAnalyzeDetails}
        />
      </div>
    )
  }
}

const mapDispatchToProps = {
  setGeneratedDashboard: dashboardActions.setGeneratedDashboard
}

const ConnectedWorkBench = connect(
  null,
  mapDispatchToProps
)(WorkBench)

export default withStyles(styles, { withTheme: true })(
  withSnackbar(withRouter(ConnectedWorkBench))
)
