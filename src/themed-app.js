import React, { Component } from 'react'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { SnackbarProvider } from 'notistack'
import Main from './components/container/main'

class ThemedApp extends Component {
  constructor(props) {
    super(props)

    this.state = {
      themeType: 'light'
    }
  }

  toggleDark = () => {
    const { themeType } = this.state
    this.setState({
      themeType: themeType === 'dark' ? 'light' : 'dark'
    })
  }

  render() {
    const { themeType } = this.state
    const typographyColor =
      themeType === 'dark' ? '#FFFFFF' : 'rgba(0, 0, 0, 0.87)'

    const theme = createMuiTheme({
      palette: {
        type: themeType,
        default: {
          main: '#FFFFFF'
        },
        primary: {
          main: '#666666'
        },
        secondary: {
          main: '#ffbc03',
          light: '#ffee4e',
          dark: '#c78c00'
        }
      },
      typography: {
        useNextVariants: true,
        fontFamily: '"Museo Sans", sans-serif',
        h4: {
          color: typographyColor
        },
        h6: {
          color: typographyColor
        },
        title: {
          color: typographyColor
        },
        button: {
          color: typographyColor
        }
      },
      overrides: {
        MuiDialogTitle: {
          root: {
            '& h6': {
              color: themeType === 'dark' && '#FFFFFF'
            }
          }
        }
      }
    })

    return (
      <MuiThemeProvider theme={theme}>
        <SnackbarProvider
          maxSnack={7}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right'
          }}
          autoHideDuration={2500}>
          <Main themeType={themeType} toggleDark={this.toggleDark} />
        </SnackbarProvider>
      </MuiThemeProvider>
    )
  }
}

export default ThemedApp
