/* eslint-env browser */

import React from 'react'
import ReactDOM from 'react-dom'
import ThemedApp from './themed-app'

import './styles/main.scss'
import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'
import 'common-frontend-components/src/fonts/museo-sans.scss'

const init = () => {
  const wrapper = document.getElementById('container')
  if (wrapper) {
    ReactDOM.render(<ThemedApp />, wrapper)
  }
}

init()
