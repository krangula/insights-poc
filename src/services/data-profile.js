import { returnBotEndpoint, fetchWithTimeout } from './config'

/**
 * Returns data profile object
 * @return {Promise} - service promise
 */
export function getDataProfileData(id) {
  const endpoint = returnBotEndpoint()
  return fetchWithTimeout({
    url: `/v2/botinsight/profile/${id}`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Returns Rank values count
 * @return {Promise} - service promise
 */
export function getRankValuesCount(id, sort, varName, count) {
  const endpoint = returnBotEndpoint()
  return fetchWithTimeout({
    url: `/v2/botinsight/profile/${id}/variableRank?sort=${sort}&varName=${varName}&count=${count}`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Returns Preview data object
 * @return {Promise} - service promise
 */
export function getPreviewData(id) {
  const endpoint = returnBotEndpoint()
  return fetchWithTimeout({
    url: `/v2/botinsight/task/latestRun/${id}`,
    type: 'GET',
    token: endpoint.token
  })
}
