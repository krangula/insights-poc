import store from '../store'
import { sessionLost } from '../reducers/reducer-login'
import { requestError } from '../reducers/reducer-global'

export const FETCH_TIMEOUT = 20000

export const getDemoStatus = () => {
  const { dashboard } = store.getState()
  const { demoMode } = dashboard

  return demoMode
}

export const returnBotEndpoint = () => {
  const { control, login } = store.getState()
  const { selectedControlRoom: room } = control
  const { authData } = login
  if (room.value && authData.token) {
    return {
      room: room.value.replace(/\/$/, ''),
      token: authData.token
    }
  }
  return null
}

export const returnLoginEndpoint = () => {
  const { control } = store.getState()
  const { selectedControlRoom: room } = control
  if (control && room) return room.value.replace(/\/$/, '')
  return null
}

export const networkStatus = () => {
  const { dashboard } = store.getState()
  const { appOnline } = dashboard
  return appOnline
}

export const fetchWithTimeout = ({ url, type, body, token }) => {
  let didTimeOut = false
  // const hasNetwork = networkStatus()
  //
  // if (!hasNetwork) {
  //   return new Promise((resolve, reject) => {
  //     setTimeout(() => {
  //       reject(new Error('network.no_internet'))
  //     }, 1500)
  //   })
  // }

  let opts = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'X-Authorization': `${token}`
    }
  }

  if (type !== 'GET') {
    opts = {
      ...opts,
      method: type,
      body
    }

    if (token) {
      opts = {
        ...opts,
        'X-Authorization': `${token}`
      }
    }
  }

  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      didTimeOut = true
      reject(new Error('network.error_timeout'))
    }, FETCH_TIMEOUT)

    fetch(url, { ...opts })
      .then(response => {
        clearTimeout(timeout)
        if (!didTimeOut) {
          const { status } = response
          if (status === 504 || status === 502 || status === 402) {
            store.dispatch(requestError(response))
          }

          if (status === 401) {
            response.json().then(reason => {
              let statusMessage = reason
              if (typeof reason === 'object') {
                const { message = '' } = reason
                statusMessage = message
              }

              store.dispatch(
                sessionLost({
                  status,
                  reason: statusMessage
                })
              )
            })
          }

          resolve(response)
        }
      })
      .catch(err => {
        store.dispatch(requestError(err))
        reject(err)
      })
      .finally(() => clearTimeout(timeout))
  })
}
