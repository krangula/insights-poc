import { fetchWithTimeout } from './config'

/**
 * Service to authenticate users' control room credentials
 * @param {!Object} - object containing username and password
 * @return {Promise} - Promise
 */
export function authenticate({ user, pass }) {
  return fetchWithTimeout({
    url: `/v1/authentication`,
    type: 'POST',
    body: JSON.stringify({
      username: user,
      password: pass
    })
  })
}

/**
 * Service to authenticate token information
 * @param {!Object} - object containing username and password
 * @return {Promise} - Promise
 */
export function authenticateToken(token) {
  return fetchWithTimeout({
    url: '/v1/authentication/token',
    type: 'POST',
    body: JSON.stringify({
      token
    }),
    token
  })
}

/**
 * Validate token received post validation
 * @param {!string} token - token string to validate
 * @return {Promise} - Promise
 */
export function validateToken(token) {
  return fetchWithTimeout({
    url: `/v1/authentication/token?token=${token}`,
    type: 'GET'
  })
}
