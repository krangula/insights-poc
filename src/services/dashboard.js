import moment from 'moment'
import { returnBotEndpoint, fetchWithTimeout, getDemoStatus } from './config'

const sample = require('../mock/sample-bots.json')
// const kibana = require('../widget-query.json')

/**
 * Returns all available dashboards
 * @return {Promise} - service promise
 */
export function getDashboards() {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    // url: `${endpoint.room}/analytics/rest/api/published_dashboard`,
    url: `/v2/botinsight/dashboards`,
    type: 'GET',
    token: endpoint.token
  })
}

// TODO: Remove the below getRunBot and getAnalyzeDetails methods after demo
export function getRunBot() {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/runBot`,
    type: 'POST',
    token: endpoint.token
  })
}

export function getAnalyzeDetails() {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/analyze/e377151c-77aa-4d28-a442-fc67eaeef4e7/Name-4f7e28f4-144e-4fce-950c-89408a2tbbbb`,
    type: 'POST',
    token: endpoint.token
  })
}

/**
 * Returns all mappings for a given task
 * @return {Promise} - service promise
 */
export function getTaskMappings(task) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    // url: `${endpoint.room}/analytics/rest/api/published_dashboard`,
    url: `/v2/botinsight/mappings/${task}`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Get list of tasks for a dashboard
 * @return {Promise} - service promise
 */
export function getDashboardTasks() {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/tasks`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Returns a widget query based on widget type
 * @return {Promise} - service promise
 */
export function getWidgetQuery(widget) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    // url: `${endpoint.room}/analytics/rest/api/published_dashboard`,
    url: `/v2/botinsight/getQuery/${widget}`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Create a new dashboard
 * @return {Promise} - service promise
 */
export function createDashboard(dashboard) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    // url: `${endpoint.room}/analytics/rest/api/published_dashboard`,
    url: `/v2/botinsight/dashboard`,
    type: 'POST',
    body: JSON.stringify(dashboard),
    token: endpoint.token
  })
}

/**
 * Update existing dashboard
 * @return {Promise} - service promise
 */
export function updateDashboard(dashboard) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/dashboard`,
    type: 'PUT',
    body: JSON.stringify(dashboard),
    token: endpoint.token
  })
}

/**
 * Returns a hopper widget query
 * @return {Promise} - service promise
 */
export function getDashboardQueries(id) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/dashboard/${id}`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Delete a dashboard
 * @return {Promise} - service promise
 */
export function deleteDashboard(id) {
  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/v2/botinsight/dashboard/${id}`,
    type: 'DELETE',
    token: endpoint.token
  })
}

/**
 * Returns a hopper dashboard data
 * @return {Promise} - service promise
 */
export function getDashboardData(query) {
  const endpoint = returnBotEndpoint()
  return fetchWithTimeout({
    url: '/v2/botinsight/search',
    type: 'POST',
    body: JSON.stringify(query),
    token: endpoint.token
  })
}

/**
 * Returns a kibana dashboard data
 * @return {Promise} - service promise
 */
export function searchDashboardData(query, search = '', task) {
  const endpoint = returnBotEndpoint()
  return fetchWithTimeout({
    url: `/v2/botinsight/search?fields=all&fullTextSearchFieldValue=${search}&fullTextSearch=true&taskId=${task}`,
    type: 'POST',
    body: JSON.stringify(query),
    token: endpoint.token
  })
}

/**
 * Returns a list of published dashboards for a control room
 * @return {Promise} - service promise
 */
export function getPublishedDashboards() {
  if (getDemoStatus()) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(sample.botList)
      }, 1500)
    })
  }

  const endpoint = returnBotEndpoint()

  return fetchWithTimeout({
    url: `/analytics/rest/api/all_published_dashboard`,
    type: 'GET',
    token: endpoint.token
  })
}

/**
 * Returns KPIs and activity data for a selected dashboard
 * @param {!number} dashboardId - requested dashboard id
 * @param {!Date} start - start date for data range
 * @param {!Date} end - end date for data range
 * @return {Promise} - service promise
 */
export function getZoomData(dashboardId, start, end) {
  if (getDemoStatus()) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(sample.botData)
      }, 1500)
    })
  }

  const endpoint = returnBotEndpoint()

  const _start = moment.utc(start).format('YYYY-MM-DDTHH:mm:ss')
  const _end = moment.utc(end).format('YYYY-MM-DDTHH:mm:ss')

  return fetchWithTimeout({
    // url: `${
    //   endpoint.room
    // }/analytics/rest/api/published_dashboard/${dashboardId}/${_start}/${_end}`,
    url: `/analytics/rest/api/published_dashboard/${dashboardId}/${_start}/${_end}`,
    type: 'GET',
    token: endpoint.token
  })
}
