import { createStyles } from '@material-ui/core/styles'

const drawerWidth = 120

export const primary = 'rgba(255, 90, 16, 1)'
export const primary2 = 'rgba(255, 131, 67, 1)'
export const primary3 = 'rgba(255, 163, 116, 1)'

// randomColor({hue: '#ffbc03',luminosity: 'random',count: 54});
const styles = theme =>
  createStyles({
    loader: {
      height: 'calc(100vh - 65px)',
      justifyContent: 'center',
      alignItems: 'center',
      display: 'flex',
      flexFlow: 'column nowrap'
    },
    appBar: {
      paddingLeft: 10,
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      position: 'fixed'
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    menuButton: {
      marginLeft: 12,
      marginRight: 36
    },
    hide: {
      display: 'none'
    },
    drawerPaper: {
      position: 'fixed',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      }),
      background: theme.palette.primary.main
    },
    drawerPaperClose: {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      width: theme.spacing.unit * 7,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing.unit * 9
      }
    },
    drawerPaperContainer: {
      height: 300,
      width: '100%',
      overflow: 'auto'
    },
    drawerIcon: {
      fontSize: 45,
      margin: '0 auto'
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar
    },
    loginContainer: {
      flexGrow: 1,
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center'
    },
    actionsContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end'
    },
    searchBar: {
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'flex-end',
      animation: 'fadeIn 0.5s ease'
    },
    searchBarSpacing: {
      margin: '0'
    },
    content: {
      height: '100%',
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing.unit * 3
      // overflow: 'hidden'
    },
    dashboard: {
      display: 'grid',
      gridTemplateColumns:
        'repeat(auto-fit, minmax(500px, 1fr))' /* Where the magic happens */,
      gridAutoRows: '550px',
      gridGap: '10px',
      margin: '20px 0',
      height: '100vh'
    },
    dashboardFullScreen: {
      display: 'grid',
      gridTemplateColumns:
        'repeat(auto-fit, minmax(500px, 1fr))' /* Where the magic happens */,
      gridAutoRows: '550px',
      gridGap: '10px',
      margin: '20px 8px',
      height: '100vh'
    },
    gridBox: {
      height: '100%',
      display: 'grid',
      overflow: 'scroll',
      alignItems: 'center',
      backgroundColor: theme.palette.type === 'dark' ? '#666666' : '#F5F5F5',
      position: 'relative',
      gridTemplateRows: '10% auto',
      gridTemplateColumns: '100%',
      justifyContent: 'center'
    },
    gridBoxFull: {
      gridTemplateRows: 'none',
      background: 'transparent',
      boxShadow: 'none',
      overflow: 'hidden'
    },
    gridKpiBox: {
      display: 'grid',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.palette.type === 'dark' ? '#666666' : '#F5F5F5',
      height: '100%'
    },
    gridPlaceholder: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: theme.palette.type === 'dark' ? '#666666' : '#f2f2f2',
      flexFlow: 'column nowrap',
      height: '100vh'
    },
    gridEmpty: {
      display: 'flex',
      height: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      flexFlow: 'column nowrap',
      gridTemplateRows: '10% auto',
      backgroundColor: theme.palette.type === 'dark' ? '#666666' : '#f2f2f2'
    },
    gridBase: {
      height: '50vh'
    },
    fullScreen: {
      display: 'flex',
      flexFlow: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'fixed',
      height: '100vh',
      width: '100%',
      zIndex: 1,
      top: 0,
      left: 0,
      paddingBottom: 64
    },
    fullScreenApp: {
      top: 0
    },
    formControl: {
      margin: theme.spacing.unit
    },
    formLabel: {
      fontWeight: 'bold',
      color: '#000000'
    },
    group: {
      display: 'flex',
      flexDirection: 'row',
      margin: `${theme.spacing.unit}px 0`
    },
    primaryText1: {
      color: primary
    },
    primaryText2: {
      color: primary2
    },
    primaryText3: {
      color: primary3
    },
    dashboardActions: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'flex-end',
      flexFlow: 'row wrap',
      marginTop: 5,
      marginBottom: 5,
      position: 'relative'
    },
    dashboardContainer: {
      position: 'relative',
      display: 'grid'
    },
    dashboardContainerFullScreen: {
      paddingLeft: 0
    },
    dashboardFullScreenCompare: {},
    stepperDialog: {
      paddingLeft: 0,
      paddingRight: 0,
      paddingTop: 0
    },
    mainContainer: {
      marginTop: 55,
      display: 'grid',
      paddingLeft: '120px',
      width: 'calc(100vw - 168px);'
    },
    mainContainerCompare: {
      gridTemplateColumns: ' 50% 50%',
      gridGap: '20px',
      paddingLeft: '110px'
    },
    mainContainerFullScreen: {
      marginTop: 0,
      paddingLeft: '0 !important',
      width: 'calc(100vw - 50px);'
    },
    mainContainerFullScreenCompare: {
      width: 'calc(100vw - 70px);'
    },
    tabsRoot: {
      boxShadow: 'none'
    },
    inheritIconWhite: {
      color: '#FFFFFF !important'
    },
    tabContainer: {
      display: 'flex',
      marginBottom: 10,
      alignItems: 'flex-end',
      justifyContent: 'space-between'
    },
    fixedPicker: {
      position: 'fixed',
      zIndex: '10',
      right: '25px',
      top: 95
    },
    fixedSearch: {
      position: 'fixed',
      zIndex: '10',
      right: '255px',
      top: 95
    }
  })

export default styles
