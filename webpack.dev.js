const merge = require('webpack-merge')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    proxy: [
      {
        context: ['/v1', '/analytics', '/v2'],
        target: 'http://ec2-34-213-193-30.us-west-2.compute.amazonaws.com'
      }
    ]
  }
})
