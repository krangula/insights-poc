##Bot Insights POC

The current implementation of Bot Insights relies on ZoomData and the use of iFrames to render
visualization information about bot performance. The aim of this project is to implement our own
solution using open source libraries and ultimately have complete ownership of all visualization and
customizations

##Getting Started

1. `npm i` or `npm install`
2. `npm run start`. This will launch the local development server on port 8080

##Technologies
##Core UI Libraries

1. React 16
2. Redux
3. React-Router
4. Babel
5. Material-UI

##Charting

1. Recharts
2. React Simple Maps

##Linting

1. Eslint
2. Prettier

##Testing
TBD

##Documentation
TBD
