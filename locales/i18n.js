// import { Localization } from 'expo-localization'
import i18n from 'i18n-js'

// Import all locales
// import ar from './ar.json'
import en from './en.json'
// import ja from './ja.json'
// import fr from './fr.json'
// import it from './it.json'
// import pt from './pt.json'
// import pl from './pl.json'
// import es from './es.json'
// import ko from './ko.json'
// import de from './de.json'
// import nl from './nl.json'
// import tr from './tr.json'
// import zhHans from './zh-Hans.json'
// import zhHant from './zh-Hant.json'

// Should the app fallback to English if user locale doesn't exists
i18n.fallbacks = true

// Define the supported translations
i18n.translations = {
  // ar,
  en
  // fr,
  // it,
  // pt,
  // pl,
  // ja,
  // es,
  // ko,
  // de,
  // nl,
  // tr,
  // 'zh-Hans': zhHans,
  // 'zh-Hant': zhHant
}

i18n.fallbacks = true
// i18n.locale = Localization.locale

// console.log('THE LOCALE', i18n.locale)

export const supportedLocales = [
  // 'ar',
  'en'
  // 'fr',
  // 'it',
  // 'pl',
  // 'pt',
  // 'pt-PT',
  // 'pt-BR',
  // 'ja',
  // 'es',
  // 'ko',
  // 'de',
  // 'zh-Hans',
  // 'zn-Hant'
]

const _locale = 'en'
// We do not like this
// Need to understand device lang
// if (Localization.locale.indexOf('pt') !== -1) {
//   _locale = 'pt'
// } else if (Localization.locale.indexOf('es') !== -1) {
//   _locale = 'es'
// } else if (Localization.locale.indexOf('fr') !== -1) {
//   _locale = 'fr'
// } else if (Localization.locale.indexOf('ja') !== -1) {
//   _locale = 'ja'
// } else if (Localization.locale.indexOf('ko') !== -1) {
//   _locale = 'ko'
// } else if (Localization.locale.indexOf('de') !== -1) {
//   _locale = 'de'
// } else if (Localization.locale.indexOf('ar') !== -1) {
//   _locale = 'ar'
// } else if (Localization.locale.indexOf('it') !== -1) {
//   _locale = 'it'
// } else if (Localization.locale.indexOf('pl') !== -1) {
//   _locale = 'pl'
// } else if (Localization.locale.indexOf('tr') !== -1) {
//   _locale = 'tr'
// } else if (Localization.locale.indexOf('nl') !== -1) {
//   _locale = 'nl'
// } else if (Localization.locale.indexOf('zh-Hans') !== -1) {
//   _locale = 'zh_CN'
// } else if (Localization.locale.indexOf('zh-Hant') !== -1) {
//   _locale = 'zh_TW'
// } else {
//   _locale = 'en'
// }

// Set the locale for the app
export const locale = _locale

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return i18n.t(name, params)
}

export default i18n
